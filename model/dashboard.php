<?php

include('header.php'); //includes the database connectivity files
require("sendgrid-php/new_register_mails.php"); //includes the email sending files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$response 		  = array();                       //response array stores response data
$restaurants_list_obj = new restaurants_list();   	   //new instance created for payment_request_class

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta
$email_obj     	 = new email_sender();                   //new instance created for email sender

if($type == "load_summary"){


    $user_hash   = $result -> user_hash;

    $response    = $restaurants_list_obj -> get_restaurant_details($user_hash,$connect_ref);
    //$response    += $restaurants_list_obj -> get_weekly_booking_details($user_hash,$connect_ref);

}else if($type == "load_dashboard_summary"){

    $user_hash   = $result -> user_hash;

    $response    = $restaurants_list_obj -> get_restaurant_details($user_hash,$connect_ref);
    $response    += $restaurants_list_obj -> get_weekly_booking_details($user_hash,$connect_ref);
    //$response    += $restaurants_list_obj -> get_latest_bookings($user_hash,$connect_ref);

}else if($type == "walkin_booking"){

    $user_hash   = $result -> user_hash;
    $guest_count   = $result -> walkin_guest_count;
    $restaurant_id   = $result -> restaurant_id;

    $walkin_booking_obj = new walkin_booking_class();   	   //new instance created for walkin_booking_class

    $response = $walkin_booking_obj -> book_a_walkin($user_hash, $guest_count, $restaurant_id, $email_obj, $connect_ref);

}

echo json_encode($response);



class restaurants_list{

    function get_restaurant_details($user_hash,$connect_ref){

        $user_count = 0;
        $status     = "active";
        $restaurant_array = array();

        $restaurant_group_id = 0;
        $sql = "SELECT `restaurant_group`,`user_role` FROM `user_details` WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id,$user_role);
            $stmt ->fetch();
            $stmt -> close();
            
        }

        $sql = "SELECT `restaurant_group_name` FROM `restaurant_group_mapping` WHERE `restaurant_group_id`=?";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_name);
            $stmt ->fetch();
            $stmt -> close();
            $restaurant_array['restaurant_group_name'] = $restaurant_group_name;
            $restaurant_array['restaurant_group_id'] = $restaurant_group_id;
        }


        if($user_role=="admin" || $user_role=="owner"){

            $sql = "SELECT `restaurant_id` FROM `restaurant_details` WHERE `status`=? AND `restaurant_group_id`=?";

            if($stmt = $connect_ref -> prepare($sql)){

                $stmt -> bind_param('ss',$status,$restaurant_group_id);
                $stmt -> execute();
                $stmt -> bind_result($restaurant_id);
                $restaurant_count = 0;
                while($stmt ->fetch()){

                    if($restaurant_id){

                        $restaurant_count++;
                        $restaurant_array['restaurant_id_'.$restaurant_count] = $restaurant_id;

                    }
                    
                }
                $restaurant_array['restaurant_count'] = $restaurant_count;
                $stmt -> close();
            
            }

        }else{

            $sql = "SELECT `restaurant_id` FROM `user_restaurant_mapping`	WHERE `status`=? AND `user_hash`=?";

            if($stmt = $connect_ref -> prepare($sql)){

                $stmt -> bind_param('ss',$status,$user_hash);
                $stmt -> execute();
                $stmt -> bind_result($restaurant_id);
                $restaurant_count = 0;
                while($stmt ->fetch()){

                    if($restaurant_id){

                        $restaurant_count++;
                        $restaurant_array['restaurant_id_'.$restaurant_count] = $restaurant_id;

                    }
                    
                }
                $restaurant_array['restaurant_count'] = $restaurant_count;
                $stmt -> close();
            
            }

        }

        

        for($temp_flag = 1; $temp_flag <= $restaurant_count; $temp_flag++){

            $restaurant_bookings_count = 0;
            $restaurant_id = $restaurant_array['restaurant_id_'.$temp_flag];
            $restaurant_name = $this -> get_restaurant_name($restaurant_id,$connect_ref);
            $restaurant_array['restaurant_'.$temp_flag] = $restaurant_name;

        }


        for($temp_flag = 1; $temp_flag <= $restaurant_count; $temp_flag++){

            $restaurant_bookings_count = 0;
            $restaurant_id = $restaurant_array['restaurant_id_'.$temp_flag];
            $table_allotment_status = $this -> get_restaurant_table_allotment_status($restaurant_id,$connect_ref);
            $restaurant_array['restaurant_table_'.$temp_flag] = $table_allotment_status;
            $restaurant_array['restaurant_id_'.$temp_flag] = $restaurant_id;

        }


        
        for($temp_flag = 1; $temp_flag <= $restaurant_count; $temp_flag++){

            $restaurant_bookings_count = 0;
            $restaurant_id = $restaurant_array['restaurant_id_'.$temp_flag];
            $restaurant_bookings_array = $this -> get_bookings_count($restaurant_id,$connect_ref);
            $restaurant_array['restaurant_booking_'.$temp_flag] = $restaurant_bookings_array["restaurant_count"];
            $restaurant_array['restaurant_guests_'.$temp_flag] = $restaurant_bookings_array["guests_count"];

        }

        $restaurant_array["user_role"] = $this -> get_user_role($user_hash,$connect_ref);

        return $restaurant_array;

    }


    function get_weekly_booking_details($user_hash,$connect_ref){

            $booking_history_array = array();

            $user_role = $this -> get_user_role($user_hash,$connect_ref);
            $restaurant_group_id = $this -> get_restaurant_group_id($user_hash,$connect_ref);

            $booking_history_array["latest_bookings"] = $this -> get_latest_bookings($user_hash,$user_role,$restaurant_group_id,$connect_ref);

            $today_date = date("Y/m/d");
           
            $ts = strtotime($today_date);
            $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
            $start_date = date('Y-m-d', $start);
            $end_date = date('Y-m-d', strtotime('next saturday', $start));
            $this_week_start = strtotime($start_date);
            $this_week_end = strtotime($end_date);
            $booking_history_array["this_week"] = $this -> get_booking_history_based_week($restaurant_group_id, $user_role, $user_hash, $this_week_start, $this_week_end, $connect_ref);

            $next_week = date('Y/m/d', strtotime("+1 week"));

            $ts = strtotime($next_week);
            $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
            $start_date = date('Y-m-d', $start);
            $end_date = date('Y-m-d', strtotime('next saturday', $start));
            $next_week_start = strtotime($start_date);
            $next_week_end = strtotime($end_date);
            $booking_history_array["next_week"] = $this -> get_booking_history_based_week($restaurant_group_id, $user_role, $user_hash, $next_week_start, $next_week_end, $connect_ref);

            $last_week = date('Y/m/d', strtotime("-1 week"));

            $ts = strtotime($last_week);
            $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
            $start_date = date('Y-m-d', $start);
            $end_date = date('Y-m-d', strtotime('next saturday', $start));
            $last_week_start = strtotime($start_date);
            $last_week_end = strtotime($end_date);
            $booking_history_array["last_week"] = $this -> get_booking_history_based_week($restaurant_group_id, $user_role, $user_hash, $last_week_start, $last_week_end, $connect_ref);

            return $booking_history_array;
    }


    function get_latest_bookings($user_hash,$user_role,$restaurant_group_id,$connect_ref){

        $latest_booking_array = array();
        $bookings_count = 0;

        if($user_role=="admin" || $user_role=="owner"){
            $sql = "SELECT bd.`restaurant_id`, bd.`booking_date`, bd.`booking_time`, bd.`booking_id`, bd.`guest_count`, bd.`guest_name`, bd.`added_at`, rd.restaurant_name FROM `booking_details` as bd, restaurant_details as rd WHERE (bd.`restaurant_id` IN (select restaurant_id from restaurant_details where restaurant_group_id = ?)) and (rd.restaurant_id = bd.restaurant_id) and bd.booking_status='active' ORDER BY bd.`added_at` DESC LIMIT 5" ;
            $dynamic_data = $restaurant_group_id;
            
        }else{
            $sql = "SELECT bd.`restaurant_id`, bd.`booking_date`, bd.`booking_time`, bd.`booking_id`, bd.`guest_count`, bd.`guest_name`, bd.`added_at`, rd.restaurant_name FROM `booking_details` as bd, restaurant_details as rd WHERE (bd.`restaurant_id` IN (select restaurant_id from user_restaurant_mapping where user_hash = ?)) and (rd.restaurant_id = bd.restaurant_id) and bd.booking_status='active' ORDER BY bd.`added_at` DESC LIMIT 5" ;
            $dynamic_data = $user_hash;
        }

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s', $dynamic_data);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$booking_date,$booking_time,$booking_id,$guest_count,$guest_name,$added_at,$restaurant_name);
            while($stmt ->fetch()){

                if($restaurant_id){

                    $bookings_count++;
                    $latest_booking_array['restaurant_id_'.$bookings_count] = $restaurant_id;
                    //date("d-m-Y",$booking_date);
                    $latest_booking_array['booking_date_'.$bookings_count] = date('D F Y', $booking_date);
                    $latest_booking_array['booking_time_'.$bookings_count] = $booking_time;
                    $latest_booking_array['booking_id_'.$bookings_count] = $booking_id;
                    $latest_booking_array['guest_count_'.$bookings_count] = $guest_count;
                    $latest_booking_array['guest_name_'.$bookings_count] = $guest_name;
                    $latest_booking_array['restaurant_name_'.$bookings_count] = $restaurant_name;

                    $date1 = new DateTime(date("d-m-Y",$added_at));
                    $date2 = new DateTime(date('d-m-Y'));
                    $current_timestamp = strtotime(date("Y-m-d H:i:s"));
                    $date_difference = $date1->diff($date2)->days; 

                    if($date_difference == 0){
                        $diff_min = $current_timestamp - $added_at;
                        $diff_min = round($diff_min / 60);
                        if($diff_min>60){

                            $diff_hour = round($diff_min / 60);
                            $diff_min_text = ($diff_hour>1) ? $diff_hour . " hours ago" : $diff_hour . " hour ago";

                        }else{

                            $diff_min_text = $diff_min. " mins ago";

                        }
                        $latest_booking_array['time_difference_'.$bookings_count] = $diff_min_text;
                    }

                    $latest_booking_array['date_difference_'.$bookings_count] = $date_difference;

                }
                
            }
            
            $stmt -> close();

            

        }

        $latest_booking_array ["bookings_count"] = $bookings_count;
        return $latest_booking_array;
}

    
    function get_booking_history_based_week($restaurant_group_id, $user_role, $user_hash, $start_timestamp, $end_timestamp, $connect_ref){

            $booking_count = 0;
            $booking_response = array();
            
            /** this could be restaurant group id or user hash, depends on user role it will be used in where condition to fetch booking summary. */ 
            $dynamic_data = ""; 

            $status_array = ["confirmed","received","arrived","no-show","cancelled"];

            foreach ($status_array as $status) {
            
                if($user_role=="admin" || $user_role=="owner"){
                    $sql = "SELECT sum(`guest_count`) FROM `booking_details` WHERE (`booking_date` between ? and ?) AND (status=?) AND  (`restaurant_id` IN (select restaurant_id from restaurant_details where restaurant_group_id = ?)) AND booking_status='active'" ;
                    $dynamic_data = $restaurant_group_id;
                    
                }else{
                    $sql = "SELECT sum(`guest_count`) FROM `booking_details` WHERE (`booking_date` between ? and ?) AND (status=?) AND  (`restaurant_id` IN (select restaurant_id from user_restaurant_mapping where user_hash = ?)) AND booking_status='active'" ;
                    $dynamic_data = $user_hash;
                }

                if($stmt = $connect_ref -> prepare($sql)){

                    $stmt -> bind_param('ssss',$start_timestamp,$end_timestamp,$status, $dynamic_data);
                    $stmt -> execute();
                    $stmt -> bind_result($booking_count);
                    $stmt -> fetch();
                    if(!$booking_count || $booking_count==null){
                        $booking_count=0;
                    } 
                    
                    $stmt -> close();

                    $booking_response [$status] = $booking_count;

                }
            
            }
            
            return $booking_response;

    }

    function get_user_role($user_hash,$connect_ref){

        $user_role = "";
        $status = "active";

        $sql = "SELECT `user_role` FROM `user_details` WHERE `user_hash`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($user_role);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $user_role;

    }

    function get_restaurant_group_id($user_hash,$connect_ref){

        $restaurant_group = "";
        $status = "active";

        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_group;

    }


    function get_bookings_count($restaurant_id,$connect_ref){

        $restaurant_count = 0;
        $guests_count = 0;
        $booking_array = array();

        $sql = "SELECT COUNT(*), SUM(`guest_count`) FROM `booking_details` WHERE `restaurant_id`=? and (`status`!='no-show' and `status`!='cancelled')";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_count,$guests_count);
            $stmt -> fetch();
            $stmt -> close();

        } 
        $booking_array["restaurant_count"] = $restaurant_count;
        $booking_array["guests_count"] = $guests_count; 

        return $booking_array;

    }


    function get_restaurant_table_allotment_status($restaurant_id,$connect_ref){

        $table_status = "";
        $status = "active";
        $restaurant_table_count = 0;

        $sql = "SELECT COUNT(*) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id, $status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_table_count);
            $stmt -> fetch();
            $stmt -> close();

        } 

        if($restaurant_table_count >= 1){
            $table_status = "true";
        }else{
            $table_status = "false";
        }
        

        return $table_status;

    }

    function get_restaurant_name($restaurant_id,$connect_ref){

        $restaurant_count = 0;
        $status = "active";

        $sql = "SELECT `restaurant_name` FROM `restaurant_details` WHERE `restaurant_id`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_name);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_name;

    }


}

class walkin_booking_class{

    function book_a_walkin($user_hash, $guest_count, $restaurant_id, $email_obj, $connect_ref){

        $current_timestamp = strtotime(date("Y-m-d H:i:s"));
        $status = "received";
        $assigned_tables = "walk-in";
        $booking_id = $this -> get_booking_id($connect_ref);
        $restaurant_group_id = $this -> get_restaurant_group_id($user_hash, $connect_ref);
        $booking_number = $this -> get_booking_number($restaurant_id, $connect_ref);
        $booking_date = strtotime(date("Y-m-d H:i:s"));
        $booking_time = date('H:i',$booking_date);
        $booking_end_time = "walk-in";
        $guest_name = "walk-in";
        $table_guest_count = $guest_count;
        $email_address = "walk-in";
        $phone_number = "walk-in";
        $comments = "walk-in";
        $email_confirmation_status = "no";
        $sms_confirmation_status = "no";
        $sms_reminder = "no";
        $restaurant_notification_status = "walk-in";
        $alergy_type = "walk-in";
        $customer_need = "walk-in";
        $country_code = "walk-in";
        $added_at = $current_timestamp;
        $booking_status = "active";
        $language = "walk-in";

        
        $booking_result = "";
        $sql = "INSERT INTO `booking_details`( `restaurant_id`, `booking_id`, `restaurant_group_id`, `booking_number`, `user_hash`, `booking_date`, `booking_time`, `booking_end_time`, `status`, `assigned_tables`, `guest_name`, `guest_count`, `table_guest_count`, `email_address`, `phone_number`, `comments`, `email_confirmation_status`, `sms_confirmation_status`, `sms_reminder`, `restaurant_notification_status`, `alergy_type`, `customer_need`, `country_code`, `added_at`, `booking_status`, `language`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                            if($stmt = $connect_ref -> prepare($sql)){
                                $stmt -> bind_param('sssssisisssiissssssssssiss',$restaurant_id,$booking_id,$restaurant_group_id,$booking_number,$user_hash,$booking_date,$booking_time,$booking_end_time,$status,$assigned_tables,$guest_name,$guest_count,$table_guest_count,$email_address,$phone_number,$comments,$email_confirmation_status,$sms_confirmation_status,$sms_reminder,$restaurant_notification_status,$alergy_type,$customer_need,$country_code,$added_at,$booking_status,$language);
                                $stmt -> execute();
                                $stmt -> close();
                                $booking_result = "success";
                                //echo $restaurant_id."-".$booking_id."-".$restaurant_group_id."-".$booking_number."-".$user_hash."-".$booking_date."-".$booking_time."-".$booking_end_time."-".$status."-".$assigned_tables."-".$guest_name."-".$guest_count."-".$table_guest_count."-".$email_address."-".$phone_number."-".$comments."-".$email_confirmation_status."-".$sms_confirmation_status."-".$sms_reminder."-".$restaurant_notification_status."-".$alergy_type."-".$customer_need."-".$country_code."-".$added_at."-".$booking_status."-".$language;
        
                            }else{
                                $booking_result = "err";
                            }

                            if($booking_result === "success"){

                                $waiter_name = $this -> get_display_name($user_hash, $connect_ref);
                                $restaurant_email = $this -> get_restaurant_email($restaurant_id,$connect_ref);
                                $restaurant_name = $this -> get_restaurant_name($restaurant_id,$connect_ref);
                                $booking_display_date = date('l d F Y', $booking_date);

                                $email_obj -> send_walkin_booking_email($guest_count, $booking_display_date, $booking_time, $restaurant_name, $restaurant_email, $status, $waiter_name);
                            
                            }

            //echo $booking_result; exit();
            return $booking_result;

        }



    function get_booking_id($connect_ref){


        $str=rand(); 
        $booking_id = sha1($str); 
        return "booking_".$booking_id;

    }



    function get_booking_number($restaurant_id, $connect_ref){


        $booking_count = 0;

        $sql = "SELECT MAX(`sno`) FROM `booking_details` WHERE `restaurant_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($booking_count);
            $stmt -> fetch();
            $stmt -> close();
            $booking_count = $booking_count+1;

        }
      
        $booking_length = strlen((string)$booking_count);
       
        if(((int)($booking_length)) == 1){

         
            return "#000".$booking_count;

        }else if((int)($booking_length) == 2){

            return "#00".$booking_count;

        }else if((int)($booking_length) == 3){

            return "#0".$booking_count;

        }else{
            return "#".$booking_count;
        }

    }



    function get_restaurant_group_id($user_hash,$connect_ref){

        $restaurant_group = "";
        $status = "active";

        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_group;

    }



    function get_display_name($user_hash,$connect_ref){

        $display_name = "";
        $status = "active";

        $sql = "SELECT `user_name` FROM `user_details` WHERE `user_hash`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($display_name);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $display_name;

    }






    function get_restaurant_email($restaurant_id,$connect_ref){

        $notification_email = "";
        $status = "active";

        $sql = "SELECT `notification_email` FROM `restaurant_details` WHERE `restaurant_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($notification_email);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $notification_email;

    }

    function get_restaurant_name($restaurant_id,$connect_ref){

        $restaurant_name = "";
        $status = "active";

        $sql = "SELECT `restaurant_name` FROM `restaurant_details` WHERE `restaurant_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_name);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_name;

    }


}