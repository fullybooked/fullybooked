<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$connect_ref = $con_obj -> connect(); 


function assign_table($guest_count,$restaurant_id,$connect_ref){

    $status = "active";

    $sql = "SELECT res_tab.`table_id` FROM `restaurant_tables` as res_tab,`booked_table` as book_tab WHERE res_tab.`restaurant_id`=? AND res_tab.`guest_count`=? AND res_tab.`status`=? AND res_tab.`restaurant_id` = book_tab.`restaurant_id` AND res_tab.`table_id` != book_tab.`table_id` LIMIT 1";

    if($stmt = $connect_ref -> prepare($sql)){

        $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
        $stmt -> execute();
        $stmt -> bind_result($table_id);
        $stmt -> fetch();
        $stmt -> close();
        
        return $table_id;
    }
    return "";
}