<?php


//require 'vendor/autoload.php'; // If you're using Composer (recommended)
// Comment out the above line if not using Composer
 require("sendgrid-php.php");
// If not using Composer, uncomment the above line and
// download sendgrid-php.zip from the latest release here,
// replacing <PATH TO> with the path to the sendgrid-php.php file,
// which is included in the download:
// https://github.com/sendgrid/sendgrid-php/releases


class password_email_sender{

    public function send_password_reset_email_link($to_address, $activation_link)
    {

        $to_name = "User";
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("no-reply@fullybooked.com", "FullyBooked");
        
        $email->setSubject("Reset Password Link ");
        $email->addTo($to_address, $to_name);
        
        $sendgrid = new \SendGrid("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");

        $mail_content = <<<EOD
        <!DOCTYPE html>
                <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
                    xmlns:o="urn:schemas-microsoft-com:office:office">
                
                <head>
                    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
                    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
                    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
                    <meta name="x-apple-disable-message-reformatting"> <!-- Disable auto-scale in iOS 10 Mail entirely -->
                    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
                
                
                    <!-- CSS Reset : BEGIN -->
                    <style>
                        html,
                        body {
                            margin: 0 auto !important;
                            padding: 0 !important;
                            height: 100% !important;
                            width: 100% !important;
                            background: #f1f1f1;
                        }
                
                        /* What it does: Stops email clients resizing small text. */
                        * {
                            -ms-text-size-adjust: 100%;
                            -webkit-text-size-adjust: 100%;
                        }
                
                        /* What it does: Centers email on Android 4.4 */
                        div[style*="margin: 16px 0"] {
                            margin: 0 !important;
                        }
                
                        /* What it does: Stops Outlook from adding extra spacing to tables. */
                        table,
                        td {
                            mso-table-lspace: 0pt !important;
                            mso-table-rspace: 0pt !important;
                        }
                
                        /* What it does: Fixes webkit padding issue. */
                        table {
                            border-spacing: 0 !important;
                            border-collapse: collapse !important;
                            table-layout: fixed !important;
                            margin: 0 auto !important;
                        }
                
                        /* What it does: Uses a better rendering method when resizing images in IE. */
                        img {
                            -ms-interpolation-mode: bicubic;
                        }
                
                        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
                        a {
                            text-decoration: none;
                        }
                
                        /* What it does: A work-around for email clients meddling in triggered links. */
                        *[x-apple-data-detectors],
                        /* iOS */
                        .unstyle-auto-detected-links *,
                        .aBn {
                            border-bottom: 0 !important;
                            cursor: default !important;
                            color: inherit !important;
                            text-decoration: none !important;
                            font-size: inherit !important;
                            font-family: inherit !important;
                            font-weight: inherit !important;
                            line-height: inherit !important;
                        }
                
                        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
                        .a6S {
                            display: none !important;
                            opacity: 0.01 !important;
                        }
                
                        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
                        .im {
                            color: inherit !important;
                        }
                
                        /* If the above doesn't work, add a .g-img class to any image in question. */
                        img.g-img+div {
                            display: none !important;
                        }
                
                        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
                        /* Create one of these media queries for each additional viewport size you'd like to fix */
                        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
                        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
                            u~div .email-container {
                                min-width: 320px !important;
                            }
                        }
                
                        /* iPhone 6, 6S, 7, 8, and X */
                        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
                            u~div .email-container {
                                min-width: 375px !important;
                            }
                        }
                
                        /* iPhone 6+, 7+, and 8+ */
                        @media only screen and (min-device-width: 414px) {
                            u~div .email-container {
                                min-width: 414px !important;
                            }
                        }
                    </style>
                </head>
                
                <body style="background-color: #fff;">
                    <center style="width: 100%; background-color: #ffffff;">
                        <div
                            style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
                        </div>
                        <div style="max-width: 600px; margin: 0 auto;" class="email-container">
                            <!-- BEGIN BODY -->
                            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
                                style="margin: auto;">
                                <tbody>
                                    <tr>
                                        <td class="bg_white logo" style="padding: 1em 2.5em; text-align: center">
                                            <h2>Hello, $to_name!</h2>
                                        </td>
                                    </tr><!-- end tr -->
                                    <tr>
                                        <td class="bg_white email-section" style="padding: 0em;">
                                            <div class="heading-section" style="text-align: center; padding: 0 30px;">
                                                 <br>
                                                
                                                <p style="font-size: 18px;padding-left: 50px;">You have requested to reset your password. Please use this link <a href="http://www.fullybooked.restaurant/forgot_password_link.html?id=$activation_link"> Reset Link </a></p>

                                            </div>
                                        </td>
                                    </tr>
                                    <br><br>
                                    <tr>
                                        <td class="bg_white"
                                            style="text-align: center; padding-left:0px; font-size: 15px;"> Wishes from  <br>FullyBooked Family                
                                        </td> 
                                    </tr>
                                   
                                </tbody>
                            </table>  
                        </div>
                    </center>
                </body>
                
                
                </html>

EOD;
        $email->addContent("text/html", $mail_content);
        
        

            $response = $sendgrid->send($email);
            $response_code = $response->statusCode();
            $response->headers();
            $response->body();
    }



}


?>
