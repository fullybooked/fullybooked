<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type

$response 		  = array();                       //response array stores response data
$embed_obj = new embed_details();   	   //new instance created for payment_request_class



if($type == "get_group_data"){


    $user_hash  = $result -> user_hash;

    //$response    = $embed_obj -> get_group_id_by_user_hash($user_hash,$connect_ref);
    $response    = $embed_obj -> list_all_restaurants($user_hash,$connect_ref);

}




echo json_encode($response);



class embed_details{

    function get_group_id_by_user_hash($user_hash,$connect_ref){

        $restaurant_group_id = 0;
        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id);
            $stmt ->fetch();
            $stmt -> close();
            
        }

        return $restaurant_group_id;

    }


    function list_all_restaurants($user_hash, $connect_ref){

        $status = "active";
        $temp   = 0;
        $restaurant_details = [];
        $booking_widget_status = "true";


        $restaurant_group_id = 0;
        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id);
            $stmt ->fetch();
            $stmt -> close();
            
        }



        $sql = "SELECT `restaurant_id`,`restaurant_name`, `booking_widget_status` FROM `restaurant_details` WHERE `restaurant_group_id`=? AND `status`=? ";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_group_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name,$booking_widget_status);
            while($stmt -> fetch()){
                $temp++; 
                $restaurant_details[$temp]["restaurant_id"]      = $restaurant_id;
                $restaurant_details[$temp]["restaurant_name"]    = $restaurant_name;
                $restaurant_details[$temp]["booking_widget_status"] = $booking_widget_status;

            }
            $stmt -> close();
        }

        $restaurant_details["restaurant_count"] = $temp;
        $restaurant_details["restaurant_group_id"]      = $restaurant_group_id;

        
        return $restaurant_details;

    }

}

