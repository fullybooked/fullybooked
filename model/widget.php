<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type

$response 		  = array();                       //response array stores response data
$widget_obj = new widget_details();   	   //new instance created for payment_request_class



if($type == "new_booking"){

  

    $language            = $result -> language;
    $restaurant_id       = $result -> restaurant_id;
    $booking_date        = $result -> booking_date;
    $booking_time        = $result -> time_value;
    $status              = "booked";
    $assigned_table      = "automatic";
    $guest_name          = $result -> guest_name;
    $guest_count         = $result -> guest_count;
    $email_address       = $result -> guest_email;
    $phone_number        = $result -> phone_number;
    $additional_notes    = $result -> comments;
    $alergy_type         = $result -> alergy_type;
    $customer_need       = $result -> customer_need;
    $country_code        = $result -> country_code;
    
   
    $response    = $widget_obj -> add_new_booking($language,$restaurant_id,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$alergy_type,$customer_need,$country_code,$response,$connect_ref);

}else if($type == "onload"){

    $user_hash = $result -> user_hash;

    $response  = $widget_obj -> get_widget_restaurant_data($user_hash,$response,$connect_ref);
   
}else if($type == "booking_time"){

    $restaurant_id = $result -> restaurant_id;

    $response["booking_time"]  = $widget_obj -> get_booking_time($restaurant_id,$connect_ref);
    $response["guest_count"]   = $widget_obj -> get_guest_count($restaurant_id,$connect_ref);

}else if($type == "booking_time_with_guest_count"){

    $restaurant_id = $result -> restaurant_id;
    $guest_count   = $result -> guest_count;

    $response  = $widget_obj -> booking_time_with_guest_count($guest_count,$restaurant_id,$connect_ref);
}



echo json_encode($response);


class widget_details{

    function add_new_booking($language,$restaurant_id,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$alergy_type,$customer_need,$country_code,$response,$connect_ref){


        $booking_id = $this -> get_booking_id($connect_ref);
        $booking_number = $this -> get_booking_number($connect_ref);

       
        $booking_status = "active";
        $added_at       = $this -> current_timestamp();
        $booking_date   = strtotime($booking_date);
       

        $sql = "INSERT INTO `booking_details`(`restaurant_id`, `booking_id`,`booking_number`, `booking_date`, `booking_time`, `status`, `assigned_tables`, `guest_name`, `guest_count`, `email_address`, `phone_number`, `comments`, `alergy_type`, `customer_need`, `country_code`, `added_at`, `booking_status`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sssssssssssssssss',$restaurant_id,$booking_id,$booking_number,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$alergy_type,$customer_need,$country_code,$added_at,$booking_status);
           
            $stmt -> execute();
            if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") $response = "true";
            else  $response = "false";

            $stmt -> close();

        }else{
            $response = "false";
        }

        $this -> insert_booking_tables($table_id,$restaurant_id,$booking_id,$booking_status,$connect_ref);

        return $response;

    }

    function current_timestamp(){

        return strtotime(date("Y-m-d H:i:s"));

    }


    function insert_booking_tables($table_id,$restaurant_id,$booking_id,$booking_status,$connect_ref){

        $sql = "INSERT INTO `booked_table`(`table_id`, `restaurant_id`, `booking_id`, `status`) VALUES (?,?,?,?)";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ssss',$table_id,$restaurant_id,$booking_id,$booking_status);
           
            $stmt -> execute();
            if(($stmt -> affected_rows) > 0 && ($stmt -> affected_rows) != "") $response = "true";
            else  $response = "false";

            $stmt -> close();

        }else{
            $response = "false";
        }


    }



    function get_assign_table($guest_count,$restaurant_id,$connect_ref){

        $status = "active";

        $sql = "SELECT res_tab.`table_id` FROM `restaurant_tables` as res_tab,`booked_table` as book_tab WHERE res_tab.`restaurant_id`=? AND res_tab.`guest_count`=? AND res_tab.`status`=? AND res_tab.`restaurant_id` = book_tab.`restaurant_id` AND res_tab.`table_id` != book_tab.`table_id` LIMIT 1";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_id);
            $stmt -> fetch();
            $stmt -> close();
            
            return $table_id;
        }
        return "";
    }




    function get_booking_id($connect_ref){

        $str=rand(); 
        $booking_id = sha1($str); 
        return "booking_".$booking_id;

        //return "booking_".$booking_count;

    }


    function get_booking_number($connect_ref){


        $booking_count = 0;

        $sql = "SELECT MAX(`sno`) FROM `booking_details`";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> execute();
            $stmt -> bind_result($booking_count);
            $stmt -> fetch();
            $stmt -> close();
            $booking_count = $booking_count+1;

        }
       
        $booking_length = strlen((string)$booking_count);
    
        if((int)($booking_length) == 1){

            return "000".$booking_count;

        }else if((int)($booking_length) == 2){

            return "00".$booking_count;

        }else if((int)($booking_length) == 3){

            return "0".$booking_count;

        }else{
            return $booking_count;
        }

    }


    function get_widget_restaurant_data($user_hash,$response,$connect_ref){

        $status = "active";
        $temp   = 0;
        $restaurant_details = [];
        $booking_time = 0;
        $guest_count = 0;


        $sql = "SELECT `restaurant_id`, `restaurant_name`,`email` FROM `restaurant_details` WHERE `owner_hash`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name,$notification_email);
            while($stmt -> fetch()){
                $temp++; 
                $restaurant_details[$temp]["restaurant_id"]   = $restaurant_id;
                $restaurant_details[$temp]["restaurant_name"] = $restaurant_name;
                $restaurant_details[$temp]["notification_email"] = $notification_email;

            }
            $stmt -> close();
        }

        $response["restaurant_details"] = $restaurant_details;

        if(sizeof($response) != 0){

            $booking_time = $this -> get_booking_time($restaurant_details[1]["restaurant_id"],$connect_ref);
            $guest_count  = $this -> get_guest_count($restaurant_details[1]["restaurant_id"],$connect_ref);

        }

        $response["booking_time"] = $booking_time;
        $response["guest_count"]  = $guest_count;

        return $response;

    }


    function get_guest_count($restaurant_id,$connect_ref){

        $status = "active";
        $minimum_group_size = 0;
        $maximum_group_size = 0;
        $guest_count = [];


        $sql = "SELECT `minimum_group_size`,`maximum_group_size`  FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($minimum_group_size,$maximum_group_size);
            $stmt -> fetch();
            $stmt -> close();
        }

        $guest_count["minimum_group_size"] = $minimum_group_size;
        $guest_count["maximum_group_size"] = $maximum_group_size;

        return $guest_count;

    }


    function get_booking_time($restaurant_id,$connect_ref){

        $temp = 0;
        $restaurant_start_time = 0;
        $timing_details = [];
        $status = "active";

        $sql = "SELECT `restaurant_start_time`,`restaurant_end_time`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_start_time,$restaurant_end_time,$booking_interval);
            while($stmt -> fetch()){

                $booking_interval = $booking_interval*60;

                while($restaurant_end_time > $restaurant_start_time){

                    $restaurant_start_time = $restaurant_start_time+$booking_interval;

                    $timing_details[$temp]["time"] = date('H:i', $restaurant_start_time);
                    $temp++; 

                }
                
                
            }
            $stmt -> close();
        }

        return $timing_details;

    }


    function booking_time_with_guest_count($guest_count,$restaurant_id,$connect_ref){

        $start_of_date = strtotime(date('Y-m-d 00:00:00'));
        $end_of_date   = strtotime(date('Y-m-d 23:59:59'));
        $booking_time_list   = [];
        $booking_time_result = [];
        $status = "active";

        $sql = "SELECT `booking_time` FROM `booking_details` WHERE `restaurant_id` =? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ssss',$restaurant_id,$start_of_date,$end_of_date,$status);
            $stmt -> execute();
            $stmt -> bind_result($booking_time);
            while($stmt -> fetch()){

                $booking_time_list.push($booking_time);
                
            }
            $stmt -> close();
        }

        $table_count = $this -> get_restaurant_table_count($guest_count,$restaurant_id,$connect_ref);


        if(sizeof($booking_time_list) != 0){

            $booking_time_result = $this -> get_booking_time_result($booking_time_list,$table_count);

        }

        return $booking_time_result;

    }   


    function get_booking_time_result($booking_time_list,$table_count){

        $booking_data = [];

        foreach ($booking_time_list as $value) {

            $time_value = $value;

            $time_count = array_count_values($time_value);

            if($time_count < $table_count){
                $booking_data.push($time_value);
            }

            $booking_time_list = array_delete($time_value, $booking_time_list);

        }

        return $booking_data;

    }


    function get_restaurant_table_count($guest_count,$restaurant_id,$connect_ref){

        $table_count = 0;
        $status = "active";

        $sql = "SELECT COUNT(`table_id`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `guest_count`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $table_count;
    }


}
