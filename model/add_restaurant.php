<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$response 		  = array();                       //response array stores response data
$restaurant_registration_obj = new restaurant_registration();   	   //new instance created for payment_request_class

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta


if($type == "restaurant_data"){


    $restaurant_name = $result -> restaurant_name; 
    $booking_notification_email = $result -> booking_notification_email;
    $phone_number = $result -> phone_number;
    $email_address = $result -> email_address; 
    $address_location = $result -> address_location;
    $booking_period = $result -> booking_period;
    $booking_interval = $result -> booking_interval;
    $duration = $result -> hidden_booking_duration;
    $booking_duration = $result -> booking_duration;
    $minimum_group_size = $result -> minimum_group_size;
    $maximum_group_size = $result -> maximum_group_size;
    $online_status = $result -> online_status;
    $same_day_booking = $result -> same_day_booking;
    $country_code = $result -> country_code;
    $owner_hash   = $result -> owner_hash;
    $restaurant_start_time = $result -> restaurant_start_time;
    $restaurant_end_time = $result -> restaurant_end_time;

    $type_of_action = $result -> type_of_action;

    if($type_of_action == "add"){
        $response    = $restaurant_registration_obj -> new_restaurant_registration($restaurant_name,$booking_notification_email,$phone_number,$email_address,$address_location,$booking_period,$booking_interval,$duration,$booking_duration,$minimum_group_size,$maximum_group_size,$online_status,$same_day_booking,$country_code,$owner_hash,$restaurant_start_time,$restaurant_end_time,$connect_ref);
    }else{
        $restaurant_id = $result -> restaurant_id;
        $response    = $restaurant_registration_obj -> update_restaurant_details($restaurant_id,$restaurant_name,$booking_notification_email,$phone_number,$email_address,$address_location,$booking_period,$booking_interval,$duration,$booking_duration,$minimum_group_size,$maximum_group_size,$online_status,$same_day_booking,$country_code,$owner_hash,$restaurant_start_time,$restaurant_end_time,$connect_ref);
    }
    

}else if($type == "restaurant_onload"){

    $restaurant_id = $result -> restaurant_id;
    $user_hash = $result -> user_hash;

    $response    =  $restaurant_registration_obj -> get_restaurant_details($user_hash,$restaurant_id,$connect_ref);
}else if($type == "restaurant_delete_details"){

    $restaurant_id = $result -> restaurant_id;
    $user_hash = $result -> user_hash;

    $response    =  $restaurant_registration_obj -> delete_restaurant($user_hash,$restaurant_id,$connect_ref);

}else if($type == "check_table_allotment"){

    $restaurant_id = $result -> restaurant_id;
    

    $response    =  $restaurant_registration_obj -> check_table_allotment($restaurant_id,$connect_ref);

}


echo json_encode($response);



class restaurant_registration{

    function new_restaurant_registration($restaurant_name,$booking_notification_email,$phone_number,$email_address,$address_location,$booking_period,$booking_interval,$duration,$booking_duration,$minimum_group_size,$maximum_group_size,$online_status,$same_day_booking,$country_code,$owner_hash,$restaurant_start_time,$restaurant_end_time,$connect_ref){
        $response_array = array();
        

        $restaurant_exist_status = $this -> check_restaurant_already_exist($restaurant_name,$owner_hash,$connect_ref);

        if($restaurant_exist_status === 0){

            $added_at      = $this -> current_timestamp();
            $status        = "active";
            $restaurant_id = $this -> generate_restaurant_id($connect_ref);

            $restaurant_group_id = $this ->get_restaurant_group_id($owner_hash, $connect_ref);

            $sql = "INSERT INTO `restaurant_details`(`restaurant_id`, `restaurant_name`, `restaurant_group_id`, `owner_hash`, `booking_widget_status`, `notification_email`, `phone_number`, `email`, `address`, `status`, `added_at`, `same_day_booking`, `duration`, `booking_period`, `booking_interval`, `booking_duration`, `minimum_group_size`, `maximum_group_size`, `default_phone_code`, `restaurant_start_time`, `restaurant_end_time`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            //echo $restaurant_id."-".$restaurant_name."-".$restaurant_group_id."-".$owner_hash."-".$online_status."-".$booking_notification_email."-".$phone_number."-".$email_address."-".$address_location."-".$status."-".$added_at."-".$same_day_booking."-".$duration."-".$booking_period."-".$booking_interval."-".$booking_duration."-".$minimum_group_size."-".$maximum_group_size."-".$country_code."-".$restaurant_start_time."-".$restaurant_end_time;

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssssssssssissssssssss',$restaurant_id,$restaurant_name,$restaurant_group_id,$owner_hash,$online_status,$booking_notification_email,$phone_number,$email_address,$address_location,$status,$added_at,$same_day_booking,$duration,$booking_period,$booking_interval,$booking_duration,$minimum_group_size,$maximum_group_size,$country_code,$restaurant_start_time,$restaurant_end_time);
                $stmt -> execute();
    
                if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") {
                    $response_array["response"] = "true";
                    $response_array["restaurant_id"] = $restaurant_id;
                }
                else  {
                    $response_array["response"] = "false";
                }
    
                $stmt -> close();
    
            }else{
                $response_array["response"] = "false";
            }

            //$this -> map_restaurant_with_user_hash($owner_hash,$restaurant_id,$connect_ref);
    
        }else{

            $response_array["response"] = "name_exist";
        }

      
        return $response_array;

    }   


    function update_restaurant_details($restaurant_id,$restaurant_name,$booking_notification_email,$phone_number,$email_address,$address_location,$booking_period,$booking_interval,$duration,$booking_duration,$minimum_group_size,$maximum_group_size,$online_status,$same_day_booking,$country_code,$owner_hash,$restaurant_start_time,$restaurant_end_time,$connect_ref){

        $response_array = array();

            $status        = "active";

            $sql = "UPDATE `restaurant_details` SET  `restaurant_name`=?, `booking_widget_status`=?, `notification_email`=?, `phone_number`=?, `email`=?, `address`=?, `status`=?,  `same_day_booking`=?, `duration`=?, `booking_period`=?, `booking_interval`=?, `booking_duration`=?, `minimum_group_size`=?, `maximum_group_size`=?, `default_phone_code`=?, `restaurant_start_time`=?, `restaurant_end_time`=? WHERE `restaurant_id`=?";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssssssssssssssssss',$restaurant_name,$online_status,$booking_notification_email,$phone_number,$email_address,$address_location,$status,$same_day_booking,$duration,$booking_period,$booking_interval,$booking_duration,$minimum_group_size,$maximum_group_size,$country_code,$restaurant_start_time,$restaurant_end_time,$restaurant_id);
                $stmt -> execute();
                $stmt -> close();
                
                $response_array["response"] = "updated";
            }else{
                
                $response_array["response"] = "false";
            }

      
        return $response_array;

    }


    function get_restaurant_details($user_hash,$restaurant_id,$connect_ref){
        $restaurant_array = array();
        $count = 0;

        $sql = "SELECT COUNT(*), `restaurant_id`, `restaurant_name`, `booking_widget_status`, `notification_email`, `phone_number`, `email`, `address`, `status`, `added_at`, `same_day_booking`, `duration`, `booking_period`, `booking_interval`, `booking_duration`, `minimum_group_size`, `maximum_group_size`, `default_phone_code`, `restaurant_start_time`, `restaurant_end_time` FROM `restaurant_details` WHERE `restaurant_id`=? ";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result( $count, $restaurant_id, $restaurant_name, $booking_widget_status, $notification_email, $phone_number, $email, $address, $status, $added_at, $same_day_booking, $duration, $booking_period, $booking_interval, $booking_duration, $minimum_group_size, $maximum_group_size, $default_phone_code, $restaurant_start_time, $restaurant_end_time);
            $stmt -> fetch();
            $stmt -> close();

        }

        if($count==1){
            $restaurant_array ["response"] = "valid";
            $restaurant_array ["restaurant_id"] = $restaurant_id;
            $restaurant_array ["restaurant_name"] = $restaurant_name;
            $restaurant_array ["booking_widget_status"] = $booking_widget_status;
            $restaurant_array ["notification_email"] = $notification_email;
            $restaurant_array ["phone_number"] = $phone_number;
            $restaurant_array ["email"] = $email;
            $restaurant_array ["address"] = $address;
            $restaurant_array ["status"] = $status;
            $restaurant_array ["added_at"] = $added_at;
            $restaurant_array ["same_day_booking"] = $same_day_booking;
            $restaurant_array ["duration"] = $duration;
            $restaurant_array ["booking_period"] = $booking_period;
            $restaurant_array ["booking_interval"] = $booking_interval;
            $restaurant_array ["booking_duration"] = $booking_duration;
            $restaurant_array ["minimum_group_size"] = $minimum_group_size;
            $restaurant_array ["maximum_group_size"] = $maximum_group_size;

            $restaurant_array ["default_phone_code"] = $default_phone_code;
            $restaurant_array ["restaurant_start_time"] = $restaurant_start_time;
            $restaurant_array ["restaurant_end_time"] = $restaurant_end_time;

        }else{
            $restaurant_array ["response"] = "invalid";
        }
        return $restaurant_array;
    }


    function map_restaurant_with_user_hash($user_hash,$restaurant_id,$connect_ref){

        $timestamp = $this -> current_timestamp();
        $status = "active";

        $sql = "INSERT INTO `user_restaurant_mapping`(`user_hash`, `restaurant_id`, `status`, `timestamp`) VALUES (?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssss',$user_hash,$restaurant_id,$status,$timestamp);
                $stmt -> execute();
                $stmt -> close();
            }

    }


    function delete_restaurant($user_hash,$restaurant_id,$connect_ref){

        // get role & restaurant group id
        $restaurant_group_id = "";
        $user_role = "";

        $sql = "SELECT `restaurant_group`,`user_role` FROM `user_details` WHERE `user_hash`=? and `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id,$user_role);
            $stmt -> fetch();
            $stmt -> close();
            
        }else{
            return "error";
        }

        if(($user_role == "admin" || $user_role == "owner") && $restaurant_group_id ){
            
            $status = "inactive";
            $sql = "UPDATE `restaurant_details` SET `status`=? WHERE `restaurant_id`=? AND `restaurant_group_id`=? ";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('sss',$status,$restaurant_id,$restaurant_group_id);
                $stmt -> execute();
                $stmt -> close();
                return "deleted";
            }else{
                return "not_deleted";
            }

        }else{
            return "error";
        }
        

    }


    function current_timestamp(){

        return strtotime(date("Y-m-d H:i:s"));

    }


    function check_restaurant_already_exist($restaurant_name,$owner_hash,$connect_ref){

        $restaurant_count = 0;

        $sql = "SELECT COUNT(*) FROM `restaurant_details` WHERE `restaurant_name`=? and `owner_hash`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_name,$owner_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_count);
            $stmt -> fetch();
            $stmt -> close();

        }

        return $restaurant_count;

    }


    function generate_restaurant_id($connect_ref){

       
        $str=rand(); 
        $restaurant_id = sha1($str); 
        return "restaurant_".$restaurant_id;

    }

    function get_restaurant_group_id($user_hash, $connect_ref){

        $restaurant_group_id = 0;
        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id);
            $stmt ->fetch();
            $stmt -> close();
            
        }

        return $restaurant_group_id;

    }

    function check_table_allotment($restaurant_id, $connect_ref){

        $allocated_tables_count = 0;
        $sql = "SELECT count(*) FROM `restaurant_tables` WHERE `restaurant_id` = ? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($allocated_tables_count);
            $stmt -> fetch();
            $stmt -> close();
            
        }

        if($allocated_tables_count>=1){
            return "enable";
        }else{
            return "disable";
        }
        

    }

}