<?php

include('header.php'); //includes the database connectivity files
require("sendgrid-php/new_register_mails.php");
require("sendgrid-php/password_reset_email.php");

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$email_obj     	 = new email_sender();                   //new instance created for email sender
$password_email_obj     	 = new password_email_sender();                   //new instance created for email sender


$response 		  = array();                       //response array stores response data
$user_registration_obj = new user_registration();   	   //new instance created for payment_request_class


define("PBKDF2_HASH_ALGORITHM", "sha512");  // Convert email to hash

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta


if($type == "sign_up"){

   
    $user_name   = $result -> user_name;
    $user_email  = $result -> user_email;
    $password    = $result -> password;
    $restaurant_name    = $result -> restaurant_name;


    $response    = $user_registration_obj -> user_signup($user_name,$user_email,$password,$response,$restaurant_name,$connect_ref,$email_obj);

}else if($type == "sign_in"){

    $user_email  = $result -> user_email;
    $password    = $result -> password;

    $response    = $user_registration_obj -> user_signin($user_email,$password,$response,$connect_ref);

}else if($type == "reset_password"){

    $new_password  = $result -> new_password;
    $activation_id  = $result -> activation_id;
   
    $response    = $user_registration_obj -> reset_password($new_password,$activation_id,$response,$connect_ref);

}else if($type == "password_activation_link"){

    $email_address  = $result -> email_address;
  
    $response       = $user_registration_obj -> password_activation_link($email_address,$response,$connect_ref,$password_email_obj);

}else if($type == "check_activation_link_id"){

    $activation_id  = $result -> activation_id;
  
    $response       = $user_registration_obj -> check_activation_link_id($activation_id,$response,$connect_ref);

}

echo json_encode($response);



class user_registration{

    function user_signup($user_name,$user_email,$password,$response,$restaurant_name,$connect_ref,$email_obj){


        $email_status = $this -> check_user_already_exist($user_email,$connect_ref);

        $user_data = array();


        if($email_status === 0){

            $user_hash     = hash(PBKDF2_HASH_ALGORITHM, $user_email);
            $added_at      = $this -> current_timestamp();
            $status        = "active";
            $password_hash = $this -> password_crypt($password, $action = 'e');

            $restaurant_group_id = $this -> get_restaurant_group_id($response,$connect_ref);

            $this -> insert_restaurant_group_id($restaurant_group_id,$restaurant_name,$user_hash,$connect_ref);
            
            $user_role = "owner";

            
            $sql = "INSERT INTO `user_details`(`user_hash`, `email`, `password`, `user_name`, `added_at`, `status`, `user_role`, `restaurant_group`) VALUES (?,?,?,?,?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssssssss',$user_hash,$user_email,$password_hash,$user_name,$added_at,$status,$user_role,$restaurant_group_id);
                $stmt -> execute();
    
                if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") {
                    
                    $user_data["status"] = "true";
                    $user_data["display_name"] = $user_name;
                    $user_data["user_hash"] = $user_hash;
                    $user_data["user_role"] = $user_role;
                    $user_data["restaurant_detail"] = $this-> get_restaurant_details($user_hash,$connect_ref);
                    $user_data["restaurant_group_id"] = $restaurant_group_id;
                    $email_obj->send_new_admin_register_email($user_name, $user_email);
                }
                else  {
                    $user_data["status"] = "false";
                }
                $stmt -> close();
    
            }else{
                $user_data["status"] = "false";
            }
    
        }else{

            $user_data["status"] = "exist";
            
        }


       
      
        return $user_data;

    }   


    function current_timestamp(){

        return strtotime(date("Y-m-d H:i:s"));

    }


    function check_user_already_exist($user_email,$connect_ref){

        $email_count = 0;

        $sql = "SELECT COUNT(*) FROM `user_details` WHERE `email`=? and `status`='active'";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$user_email);
            $stmt -> execute();
            $stmt -> bind_result($email_count);
            $stmt -> fetch();
            $stmt -> close();

        }

        return $email_count;

    }



    function get_restaurant_group_id($user_email,$connect_ref){

        /*$restaurant_group_count = 0;

        $sql = "SELECT MAX(sno) FROM `restaurant_group_mapping` ";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        if(!$restaurant_group_count){
            $restaurant_group_count = 0;
        }

        $restaurant_group_count = $restaurant_group_count+1;

        return "group_".$restaurant_group_count;*/

        $str=rand(); 
        $restaurant_id = sha1($str); 
        return "group_".$restaurant_id;

    }


    function insert_restaurant_group_id($restaurant_group_id,$restaurant_name,$owner_hash,$connect_ref){

        $status = "active";

        $sql = "INSERT INTO `restaurant_group_mapping`(`restaurant_group_id`, `restaurant_group_name`, `owner_hash`, `status`) VALUES (?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){

                $stmt -> bind_param('ssss',$restaurant_group_id,$restaurant_name,$owner_hash,$status);
                $stmt -> execute();
                $stmt -> close();
            }

    }

    


    function user_signin($user_email,$password,$response,$connect_ref){

        $password_hash = $this -> password_crypt($password, $action = 'e');
        $status    = "active";

        $user_count = 0;

        $sql = "SELECT COUNT(*),user_name,user_hash,user_role FROM `user_details` WHERE `email`=? AND `password`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_email,$password_hash);
            $stmt -> execute();
            $stmt -> bind_result($user_count,$display_name,$user_hash,$user_role);
            $stmt -> fetch();
            $stmt -> close();

        }

        $user_data = array();

        if($user_count === 0){

            $user_data["status"] =  "false";
        
        }else if($user_count === 1){

            $user_data["status"] = "true";
            $user_data["display_name"] = $display_name;
            $user_data["user_hash"] = $user_hash;
            $user_data["user_role"] = $user_role;
            $user_data["restaurant_detail"] = $this-> get_restaurant_details($user_hash,$connect_ref);
            $user_data["restaurant_group_id"] = $this-> get_restaurant_group($user_hash,$connect_ref);
                    
        }else{

            $user_data["status"] =  "error";

        }
        return $user_data;
    }   


    function get_restaurant_group($user_hash,$connect_ref){
        $restaurant_group = 0;
        $status     = "active";
        
        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `status`=? AND `user_hash`=?";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$status,$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group);
            $stmt -> fetch();
            $stmt -> close();
         
        }

        return $restaurant_group;
    }

    function reset_password($new_password,$activation_id,$response,$connect_ref){

        $status = "valid";
        $user_hash = "";

        $sql = "SELECT `user_hash` FROM `forgot_password_activation_link` WHERE `activation_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$activation_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($user_hash);
            $stmt -> fetch();
            $stmt -> close();
         
        }

        if($user_hash){
            
            $password_hash = $this -> password_crypt($new_password, $action = 'e');
            $sql = "UPDATE `user_details` SET `password`=? WHERE `user_hash`=? AND `status`='active'";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ss',$password_hash,$user_hash);
                $stmt -> execute();
                $stmt -> close();
                $response = "true";
            }else{

                $response = "false";

            }

            if($response == "true"){

                $status = "expired";
                $sql = "UPDATE `forgot_password_activation_link` SET `status`=? WHERE `activation_id`=?";
                if($stmt = $connect_ref -> prepare($sql)){
                    $stmt -> bind_param('ss',$status, $activation_id);
                    $stmt -> execute();
                    $stmt -> close();
    
                }

            }

        }else{

            $response = "false";

        }

        return $response;

    }


    function password_activation_link($email_address,$response,$connect_ref,$password_email_obj){

        $user_status = $this -> get_user_status($email_address,$connect_ref);

        if($user_status != 0){

            $unique_id = md5(uniqid(mt_rand()));
            $added_at  = $this -> current_timestamp();

            $user_hash  = $this -> get_user_hash($email_address,$connect_ref);
            $link_data  = $this -> insert_activation_link_to_db($email_address,$user_hash,$unique_id,$added_at,$connect_ref);

            if($link_data != "true") {

                $password_email_obj->send_password_reset_email_link($email_address, $unique_id);

                return "true";
                
            }else {
                return $link_data; 
            }
        
        }else{

            return "not_exist";
        }

    }


    function check_activation_link_id($activation_id,$response,$connect_ref){

        $current_timestamp  = $this -> current_timestamp();

        $one_hour_back_timestamp = $current_timestamp - 60*60;

        //echo $activation_id;
        $timestamp = 0;
        $link_status = 0;

        $sql = "SELECT count(*),`timestamp`,`status` FROM `forgot_password_activation_link` WHERE `activation_id`=? ";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$activation_id);
            $stmt -> execute();
            $stmt -> bind_result($count,$timestamp,$link_status);
            $stmt -> fetch();
            $stmt -> close();
        }else{
            $response =  "error";
        }

        if($count == 0){
            $response = "invalid_link";
        }else{
            if($link_status == "expired"){
                $response =  "link_expired";
            }else if($timestamp < $one_hour_back_timestamp){
                $response =  "time_expired";
            }else if(($link_status == "valid") && ($timestamp > $one_hour_back_timestamp)){
                $response =  "valid";
            }else{
                $response =  "error";
            }
        }
        

        return $response;
    }



    function get_user_hash($email_address,$connect_ref){

        $user_hash = "";
        $status    = "active";
        
        $sql = "SELECT `user_hash` FROM `user_details` WHERE `email`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$email_address,$status);
            $stmt -> execute();
            $stmt -> bind_result($user_hash);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $user_hash;

    }


    function insert_activation_link_to_db($email_address,$user_hash,$unique_id,$added_at,$connect_ref){

        $status = "valid";
        $sql = "INSERT INTO `forgot_password_activation_link`(`email_address`, `user_hash`, `activation_id`, `timestamp`,`status`) VALUES (?,?,?,?,?)";
        
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sssss',$email_address,$user_hash,$unique_id,$added_at,$status);
            $stmt -> execute();

            if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") $response = "true";
            else  $response = "false";

            $stmt -> close();

        }else{
            $response = "false";
        }
   
    }



    function get_user_status($email_address,$connect_ref){

        $user_count = 0;
        $status     = "active";
        
        $sql = "SELECT COUNT(*) FROM `user_details`	WHERE `status`=? AND `email`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$status,$email_address);
            $stmt -> execute();
            $stmt -> bind_result($user_count);
            $stmt -> fetch();
            $stmt -> close();
         

        }

        return $user_count;
    }





    function password_crypt($password, $action = 'e') {

        // you may change these values to your own
        $secret_key = 'fully_booked_secret_key';
        $secret_iv = 'fully_booked_secret_key';
     
        $encrypt_method = "AES-256-CBC";
        $output = false;
        $key    = hash( 'sha256', $secret_key );
        $iv     = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if($action == 'e') {
            $output = base64_encode( openssl_encrypt( $password, $encrypt_method, $key, 0, $iv ) );
        }
        else if($action == 'd'){
            $output = openssl_decrypt( base64_decode( $password ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }

    function get_restaurant_details($owner_hash,$connect_ref){

        $user_count = 0;
        $status     = "active";
        $restaurant_array = array();

        $sql = "SELECT `restaurant_name` FROM `restaurant_details`	WHERE `status`=? AND `owner_hash`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            //echo $status."-".$owner_hash;
            $stmt -> bind_param('ss',$status,$owner_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_name);
            $restaurant_count = 0;
            while($stmt ->fetch()){

                if($restaurant_name){

                    $restaurant_count++;
                    $restaurant_array['restaurant-'.$restaurant_count] = $restaurant_name;

                }
                 
            }
            $restaurant_array['restaurant_count'] = $restaurant_count;
            $stmt -> close();
         
        }

        return $restaurant_array;

    }


}