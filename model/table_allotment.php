<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$response 		  = array();                       //response array stores response data
$table_allotment_process_obj = new table_allotment_process();   	   //new instance created for payment_request_class

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta


if($type == "load_tables"){

    $user_hash = $result -> user_hash;
    $restaurant_id = $result -> restaurant_id;
    

    $response    = $table_allotment_process_obj -> get_restaurant_details($user_hash,$restaurant_id,$connect_ref);
 
}else if($type == "update_table"){
    $user_hash = $result -> user_hash;
    $restaurant_id = $result -> restaurant_id;
    $table_data_array = $result -> table_data_array;
    
    $response    = $table_allotment_process_obj -> update_table_details($user_hash,$restaurant_id,$table_data_array,$connect_ref);

}else if($type == "load_tables_summary"){
    $user_hash = $result -> user_hash;
    $restaurant_id = $result -> restaurant_id;
    $response    = $table_allotment_process_obj -> load_table_summary($user_hash,$restaurant_id,$connect_ref);

}

echo json_encode($response);



class table_allotment_process{

    function get_restaurant_details($user_hash,$restaurant_id,$connect_ref){
        $tables_array = array();
        $count = 0;
        $status = "active";

        $sql = "SELECT `table_id`, `guest_count` FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result( $table_id, $guest_count);
            while($stmt -> fetch()){
                if($table_id){
                    $count = $count+1;
                    $tables_array["table_id_".$count] = $table_id;
                    $tables_array["guest_count_".$count] = $guest_count;
                }
            }
            $stmt -> close();

        }

        $tables_array ["total_tables"] = $count ;
        return $tables_array;
    }

    function load_table_summary($user_hash,$restaurant_id,$connect_ref){

        $table_count = 0;
        $seat_count = 0;
        $table_summary = array();
        $status = "active";

        $sql = "SELECT count(`table_id`),SUM(`guest_count`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result( $table_count, $seat_count);
            $stmt -> fetch();
            $stmt -> close();

        }
        $table_summary["table_count"] = $table_count;
        if(!$seat_count){
            $seat_count=0;
        }
        $table_summary["seat_count"] = $seat_count;

        return $table_summary;
    }

    function get_table_id($restaurant_id,$connect_ref){
        $tables_id = array();
        $status = "active";

        $sql = "SELECT `table_id` FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result( $table_id);
            while($stmt -> fetch()){
                if($table_id){
                    array_push($tables_id,$table_id);
                }
            }
            $stmt -> close();

        }

        return $tables_id;
    }



    function update_table_details($user_hash,$restaurant_id,$table_data_array,$connect_ref){
        
        $result = json_decode(json_encode($table_data_array), true);
        
        $tables_count = $result ["total_tables"];

        $new_tables_id = array();
        $old_tables_id = $this -> get_table_id($restaurant_id,$connect_ref);

        for($flag=1; $flag <= $tables_count; $flag++){
            $table_id = $result["table_id_".$flag];
            $guest_count = $result["guest_count_".$flag];
            $table_name = $result["table_name_".$flag];


            if($table_id=="new_table"){
                $status = "active";

                $table_id = $this -> generate_table_id();

                $sql = "INSERT INTO `restaurant_tables`(`restaurant_id`, `table_name`, `table_id`, `guest_count`, `status`) VALUES (?,?,?,?,?)";
                if($stmt = $connect_ref -> prepare($sql)){
                    $stmt -> bind_param('sssss',$restaurant_id,$table_name,$table_id,$guest_count,$status);
                    $stmt -> execute();
                    $stmt -> close();
                    $response = "inserted";
                    
                }else{
                    $response = "false";
                }

            }else{
                
                $sql = "UPDATE `restaurant_tables` SET  `table_name`=?, `guest_count`=? WHERE `restaurant_id`=? AND `table_id`=?";

                if($stmt = $connect_ref -> prepare($sql)){
                    $stmt -> bind_param('ssss',$table_name, $guest_count, $restaurant_id,$table_id);
                    $stmt -> execute();
                    $stmt -> close();
                    $response = "updated";
                    array_push($new_tables_id,$table_id);
                }else{
                    $response = "false";
                }

            }


        }


        $id_diff_array = array_diff($old_tables_id, $new_tables_id);


        foreach ($id_diff_array as $value){ 
            $status="inactive";
            $sql = "UPDATE `restaurant_tables` SET `status`=? WHERE `restaurant_id`=? AND `table_id`=?";

                if($stmt = $connect_ref -> prepare($sql)){
                    $stmt -> bind_param('sss',$status, $restaurant_id, $value);
                    $stmt -> execute();
                    $stmt -> close();
                    $response = "updated";
                }else{
                    $response = "false";
                }
        } 

        $table_summary["response"] = $response;
        $table_summary["numbers"] = $this -> load_table_summary($user_hash,$restaurant_id,$connect_ref);

        return $table_summary;
    }


    function generate_table_id(){

        $str=rand(); 
        $table_id = sha1($str); 
        return "table_".$table_id;

    }

}