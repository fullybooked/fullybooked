<?php

include('header.php'); //includes the database connectivity files
/*
error_reporting(E_ALL);
ini_set('display_errors', 'On');
*/
$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$response 		 = array();                       //response array stores response data
$my_account_obj  = new my_account();   	   //new instance created for payment_request_class


define("PBKDF2_HASH_ALGORITHM", "sha512");  // Convert email to hash




if($type == "updateUserDetails"){

   
    $user_name       = $result -> userName;
    $user_hash       = $result -> userHash;

    $response    = $my_account_obj -> update_user_details($user_name,$user_hash,$response,$connect_ref);

}else if($type == "load_profile_details"){

    $user_hash       = $result -> userHash;

    $response    = $my_account_obj -> load_profile_details($user_hash,$connect_ref);

}else if($type == "updatePassword"){

    $user_hash       = $result -> userHash;
    $newPassword       = $result -> newPassword;
    $currentPassword       = $result -> currentPassword;


    $response    = $my_account_obj -> change_password($currentPassword,$newPassword,$user_hash,$connect_ref);

}else if($type == "change_restaurant_group_name"){

    $user_hash       = $result -> userHash;
    $restaurant_group_name       = $result -> restaurant_group_name;

    $response    = $my_account_obj -> change_restaurant_group_name($restaurant_group_name,$user_hash,$connect_ref);

}


echo json_encode($response);



class my_account{

    function update_user_details($user_name,$user_hash,$response,$connect_ref){

        $profile = array();
    
        $sql = "UPDATE `user_details` SET `user_name`=? WHERE `user_hash`=? AND `status`='active'";
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_name,$user_hash);
            $stmt -> execute();
            $stmt -> close();
            $profile ["user_name"] = $user_name;
            $profile ["response"] = "true";

        }else{

            $profile ["response"] = "false";

        }

        return $profile;
    }


    function load_profile_details($user_hash, $connect_ref){

        $profile = array();

        $sql = "SELECT `user_name`, `email`, `user_role`,`restaurant_group` FROM user_details WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($user_name,$email,$user_role,$restaurant_group_id);
            $stmt -> fetch();
            $stmt -> close();

            $profile ["user_name"] = $user_name;
            $profile ["email"] = $email;
            $profile ["user_role"] = $user_role;
            $profile ["response"] = "true";

        }else{
            $profile ["response"] = "false";
        }

        $sql = "SELECT `restaurant_group_name` FROM restaurant_group_mapping WHERE `restaurant_group_id`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_name);
            $stmt -> fetch();
            $stmt -> close();

            $profile ["restaurant_group_name"] = $restaurant_group_name;

        }else{
            $profile ["restaurant_group_name"] = "-";
        }

        return $profile;

    }


    function change_password($currentPassword,$newPassword,$user_hash,$connect_ref){

        $new_Password = $this -> password_crypt($newPassword, $action = 'e');
        $current_Password = $this -> password_crypt($currentPassword, $action = 'e');
        $match_count = 0;

        $sql = "SELECT count(*) FROM user_details WHERE `user_hash`=? AND `password`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$current_Password);
            $stmt -> execute();
            $stmt -> bind_result($match_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        if($match_count === 1){
            $sql = "UPDATE `user_details` SET `password`=? WHERE `user_hash`=? AND `status`='active'";
            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ss',$new_Password,$user_hash);
                $stmt -> execute();
                $stmt -> close();
                $response = "true";

            }else{

                $response = "false";

            }
        }else{
            $response = "old_pwd_wrong";
        }


        

        return $response;

    }

  


    function password_crypt($password, $action = 'e') {

        $secret_key = 'fully_booked_secret_key';
        $secret_iv = 'fully_booked_secret_key';
     
        $encrypt_method = "AES-256-CBC";
        $output = false;
        $key    = hash( 'sha256', $secret_key );
        $iv     = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if($action == 'e') {
            $output = base64_encode( openssl_encrypt( $password, $encrypt_method, $key, 0, $iv ) );
        }
        else if($action == 'd'){
            $output = openssl_decrypt( base64_decode( $password ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }



    function change_restaurant_group_name($restaurant_group_name,$user_hash,$connect_ref){

        $response = "false";

        $sql = "SELECT `restaurant_group`, `user_role` FROM user_details WHERE `user_hash`=? AND `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id,$user_role);
            $stmt -> fetch();
            $stmt -> close();

        }

        if($user_role === "admin" || $user_role ==="owner"){
            
            $sql = "UPDATE `restaurant_group_mapping` SET `restaurant_group_name`=? WHERE `restaurant_group_id`=? AND `status`='active'";
            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ss',$restaurant_group_name,$restaurant_group_id);
                $stmt -> execute();
                $stmt -> close();
                $response = "true";

            }else{

                $response = "false";

            }

        }

        

        return $response;

    }



}