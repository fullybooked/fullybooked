<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type


$response 		  = array();                       //response array stores response data
$sms_obj = new sms_handler();   	   //new instance created for payment_request_class

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta


if($type == "load_sms_summary"){

    $user_hash   = $result -> user_hash;

    $response    = $sms_obj -> load_sms_summary_details($user_hash,$connect_ref);

}else if($type == "save_sms_settings"){

    $user_hash   = $result -> user_hash;
    $sms_sender_name   = $result -> sms_sender_name;
    $sms_enable   = $result -> sms_enable;

    $response    = $sms_obj -> save_sms_settings($user_hash,$sms_sender_name,$sms_enable,$connect_ref);

}

echo json_encode($response);



class sms_handler{

    function load_sms_summary_details($user_hash,$connect_ref){
        $response_array = array();

        $restaurant_group_id = $this -> get_restaurant_group_id($user_hash,$connect_ref);

        $totally_sent_sms = 0;

        $sql = "SELECT count(*) FROM `sent_sms_details` WHERE `restaurant_group_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($totally_sent_sms);
            $stmt -> fetch();
            $stmt -> close();

        } 

        $response_array["totally_sent_sms"] = $totally_sent_sms;
        
        $last_seven_day_timestamp = strtotime("-1 week");

        $sql = "SELECT count(*) FROM `sent_sms_details` WHERE `restaurant_group_id`=? AND `added_at` > ?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_group_id,$last_seven_day_timestamp);
            $stmt -> execute();
            $stmt -> bind_result($last_week_sent_sms);
            $stmt -> fetch();
            $stmt -> close();

        } 

        $response_array["last_week_sent_sms"] = $last_week_sent_sms;

        $last_day_timestamp = strtotime("-1 day");

        $sql = "SELECT count(*) FROM `sent_sms_details` WHERE `restaurant_group_id`=? AND `added_at` > ?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_group_id,$last_day_timestamp);
            $stmt -> execute();
            $stmt -> bind_result($last_day_sent_sms);
            $stmt -> fetch();
            $stmt -> close();

        } 

        $response_array["last_day_sent_sms"] = $last_day_sent_sms;


        $enable_sms = "";
        $sender_name = "";
        $sql = "SELECT `enable_sms`,`sender_name` FROM `sms_settings` WHERE `restaurant_group_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($enable_sms,$sender_name);
            $stmt -> fetch();
            $stmt -> close();

        } 
        $response_array["is_enabled_sms"] = $enable_sms;
        $response_array["sender_name"] = $sender_name;

        return $response_array;

    }



    function get_restaurant_group_id($user_hash,$connect_ref){

        $restaurant_group_id = 0;
        $status = "active";

        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? AND `status`= ?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_group_id;

    }


    function save_sms_settings($user_hash,$sms_sender_name,$sms_enable,$connect_ref){
        $response = "";
        $settings_count = 0;
        $restaurant_group_id = $this -> get_restaurant_group_id($user_hash,$connect_ref);

        $sql = "SELECT count(*) FROM `sms_settings` WHERE `restaurant_group_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($settings_count);
            $stmt -> fetch();
            $stmt -> close();

        } 

        if($settings_count>=1){
            $timestamp = strtotime(date("Y-m-d H:i:s"));
            $sql = "UPDATE `sms_settings` SET `enable_sms`=?, `sender_name`=?, `timestamp`=? WHERE `restaurant_group_id`=?";
            if($stmt = $connect_ref -> prepare($sql)){

                $stmt -> bind_param('ssss',$sms_enable,$sms_sender_name,$timestamp,$restaurant_group_id);
                $stmt -> execute();
                $stmt -> close();
                $response = "updated";
            }

        }else{

            $timestamp = strtotime(date("Y-m-d H:i:s"));
            $sql = "INSERT INTO `sms_settings`( `restaurant_group_id`, `enable_sms`, `sender_name`, `timestamp`) VALUES (?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssss',$restaurant_group_id,$sms_enable,$sms_sender_name,$timestamp);
                $stmt -> execute();
                $stmt -> close();
                $response = "inserted";
            }

        }

        return $response;

    }





}