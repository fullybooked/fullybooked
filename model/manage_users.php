<?php

include('header.php'); //includes the database connectivity files
require("sendgrid-php/new_register_mails.php"); //includes the email sending files


error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type

$email_obj     	 = new email_sender();                   //new instance created for email sender

define("PBKDF2_HASH_ALGORITHM", "sha512");  // Convert email to hash


$response 		  = array();                       //response array stores response data
$user_role_management_obj = new user_role_management();   	   //new instance created for payment_request_class

// date_default_timezone_set('Asia/Calcutta');		   //default timezone set to Asia/Calcutta

if($type == "list_users"){

    $owner_hash   = $result -> user_hash;

    $response    = $user_role_management_obj -> list_all_users($owner_hash,$connect_ref);

}else if($type == "delete_user"){

    $user_hash   = $result -> user_hash;
    $response    = $user_role_management_obj -> delete_user($user_hash,$connect_ref);

}else if($type == "load_restaurant_roles"){

    $user_hash   = $result -> user_hash;
    $response    = $user_role_management_obj -> load_restaurant_roles($user_hash,$connect_ref);

}else if($type == "add_new_user"){

    $user_hash   = $result -> owner_hash;
    $display_name   = $result -> display_name;
    $password   = $result -> password;
    $user_role_dropdown   = $result -> user_role_dropdown;
    $restaurant_list_dropdown   = $result -> restaurant_list_dropdown;
    $user_email = $result -> user_email;

    $response    = $user_role_management_obj -> add_new_user($user_hash,$display_name,$user_email,$password,$restaurant_list_dropdown,$user_role_dropdown,$connect_ref,$email_obj);

}else{

}

echo json_encode($response);



class user_role_management{

    function list_all_users($owner_hash,$connect_ref){


        $restaurant_group = $this -> get_restaurant_group($owner_hash,$connect_ref);

        $user_array = array();

        $sql = "SELECT `user_hash`, `email`, `user_name`, `user_role` FROM `user_details` WHERE `restaurant_group`=? and `status`='active' and `user_role`!='owner'";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_group);
            $stmt -> execute();
            $stmt -> bind_result($user_hash,$user_email,$user_name,$user_role);
            $user_count = 0;
            while($stmt ->fetch()){

                if($user_hash){

                    $user_count++;
                    $user_array['user_hash_'.$user_count] = $user_hash;
                    $user_array['user_email_'.$user_count] = $user_email;
                    $user_array['user_name_'.$user_count] = $user_name;
                    $user_array['user_role_'.$user_count] = $user_role;
                    //$user_array['restaurant_id_'.$user_count] = $restaurant_id;

                }
                 
            }

            $user_array['user_count'] = $user_count;

            $stmt -> close();


            

            for($flag=1;$flag<=$user_count;$flag++){

                $user_hash = $user_array['user_hash_'.$flag];
                $user_role = $user_array['user_role_'.$flag];
                if($user_role == "admin"){
                    $restaurant_id_name = "-";
                }else{
                    $restaurant_id_name = $this -> get_restaurant_list($user_hash, $connect_ref);
                }
                $user_array['restaurant_info_'.$user_hash] = $restaurant_id_name;


            }

        }

        return $user_array;

    }   


    function current_timestamp(){

        return strtotime(date("Y-m-d H:i:s"));

    }


    function get_restaurant_group($owner_hash,$connect_ref){

        $restaurant_group = "";

        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$owner_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group);
            $stmt -> fetch();
            $stmt -> close();

        }

        return $restaurant_group;

    }


    function get_restaurant_list($user_hash,$connect_ref){

        $restaurant_data = array();

        $sql = "SELECT urm.`restaurant_id`, rd.restaurant_name FROM `user_restaurant_mapping` as urm, restaurant_details as rd WHERE urm.`status`='active' and `user_hash`=? and rd.restaurant_id = urm.restaurant_id";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name);
            $restaurant_count = 0;
            while($stmt ->fetch()){

                if($restaurant_id){

                    $restaurant_count++;

                    $restaurant_data['restaurant_id_'.$restaurant_count] = $restaurant_id;
                    $restaurant_data['restaurant_name_'.$restaurant_count] = $restaurant_name;
                    
                }
                 
            }
            $stmt -> close();
            $restaurant_data['restaurant_count'] = $restaurant_count;

        }

        return $restaurant_data;

    }


    function delete_user($user_hash,$connect_ref){

        $sql = "UPDATE `user_details` SET `status`='inactive' WHERE `user_hash` = ?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> fetch();
            $stmt -> close();
            return "true";
        }else{
            return "false";
        }

    }


    function load_restaurant_roles($user_hash,$connect_ref){
        
        $restaurant_name = "";
        $restaurant_count = 0;
        $restaurant_info = array();
        
            $sql = "SELECT `restaurant_name`,`restaurant_id` FROM `restaurant_details` WHERE `owner_hash`=? AND `status`='active'";
        
            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('s',$user_hash);
                $stmt -> execute();
                $stmt -> bind_result($restaurant_name,$restaurant_id);
                
                while($stmt ->fetch()){

                    if($restaurant_name){

                        $restaurant_count++;
                        $restaurant_info['restaurant_name_'.$restaurant_count] = $restaurant_name;
                        $restaurant_info['restaurant_id_'.$restaurant_count] = $restaurant_id;

                    }
                    
                }

                $restaurant_info['restaurant_count'] = $restaurant_count;

                $stmt -> close();
            }

            
            $active_status = "active";
            $roles_count = 0;
            $sql = "SELECT `role_name`,`role_id` FROM `roles` WHERE `status`=?";
       
            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('s',$active_status);
                $stmt -> execute();
                $stmt -> bind_result($role_name,$role_id);
                
                while($stmt ->fetch()){

                    if($role_name){

                        $roles_count++;
                        $restaurant_info['role_name_'.$roles_count] = $role_name;
                        $restaurant_info['role_id_'.$roles_count] = $role_id;

                    }
                    
                }

                $restaurant_info['roles_count'] = $roles_count;

                $stmt -> close();

            }

            return $restaurant_info;

    }


    function get_restaurant_name($restaurant_id,$connect_ref){
        //echo $owner_hash;
        $restaurant_name = "";

        $sql = "SELECT `restaurant_name` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`='active'";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_name);
            $stmt -> fetch();
            $stmt -> close();

        }

        return $restaurant_name;

    }


    function add_new_user($owner_hash,$display_name,$user_email,$password,$restaurant_list_dropdown,$user_role_dropdown,$connect_ref,$email_obj){

        $email_status = $this -> check_user_already_exist($user_email,$connect_ref);

        $restaurant_group = $this -> get_restaurant_group($owner_hash,$connect_ref);

        $user_response = array();

        if($email_status === 0){

            $user_hash     = hash(PBKDF2_HASH_ALGORITHM, $user_email);
            $added_at      = $this -> current_timestamp();
            $status        = "active";
            $password_hash = $this -> password_crypt($password, $action = 'e');
    

            $sql = "INSERT INTO `user_details`(`user_hash`, `email`, `password`, `user_name`, `added_at`, `status`,`added_by`,`user_role`,`restaurant_group`) VALUES (?,?,?,?,?,?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('sssssssss',$user_hash,$user_email,$password_hash,$display_name,$added_at,$status,$owner_hash,$user_role_dropdown,$restaurant_group);
                $stmt -> execute();
                $stmt -> close();
                
                if($user_role_dropdown!="admin"){
                    $this -> map_restaurant_with_user_hash($user_hash,$restaurant_list_dropdown,$connect_ref);
                }
                

                $email_obj->send_new_user_register_email($display_name, $user_email, $user_role_dropdown, $password);


                return "account_created";
                
            }
    
        }else{

            return "email_exist";

        }

    }


    function map_restaurant_with_user_hash($user_hash,$restaurant_id,$connect_ref){

        $timestamp = $this -> current_timestamp();
        $status = "active";

        $sql = "INSERT INTO `user_restaurant_mapping`(`user_hash`, `restaurant_id`, `status`, `timestamp`) VALUES (?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssss',$user_hash,$restaurant_id,$status,$timestamp);
                $stmt -> execute();
                $stmt -> close();
            }

    }

    function check_user_already_exist($user_email,$connect_ref){

        $email_count = 0;

        $sql = "SELECT COUNT(*) FROM `user_details` WHERE `email`=? and `status`='active'";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$user_email);
            $stmt -> execute();
            $stmt -> bind_result($email_count);
            $stmt -> fetch();
            $stmt -> close();

        }

        return $email_count;

    }



    function password_crypt($password, $action = 'e') {

        // you may change these values to your own
        $secret_key = 'fully_booked_secret_key';
        $secret_iv = 'fully_booked_secret_key';
     
        $encrypt_method = "AES-256-CBC";
        $output = false;
        $key    = hash( 'sha256', $secret_key );
        $iv     = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if($action == 'e') {
            $output = base64_encode( openssl_encrypt( $password, $encrypt_method, $key, 0, $iv ) );
        }
        else if($action == 'd'){
            $output = openssl_decrypt( base64_decode( $password ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }




}