<?php

include('header.php'); //includes the database connectivity files

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type

$response 		  = array();                       //response array stores response data
$search_booking_obj = new search_booking();   	   //new instance created for payment_request_class



if($type == "onload"){

    $user_hash        = $result -> user_hash;
    $restaurant_type  = $result -> restaurant_type;
    $month_type       = $result -> month_type;
    $restaurant_group_id  = $result -> restaurant_group_id;

    $month_booking_count   = $search_booking_obj -> get_month_list($user_hash,$restaurant_group_id,$restaurant_type,$connect_ref);
    $restaurant_details    = $search_booking_obj -> get_restaurant_list($user_hash,$restaurant_group_id,$connect_ref);
    $upcoming_booking_list = $search_booking_obj -> get_booking_list($month_type,$restaurant_type,$user_hash,$restaurant_group_id,$connect_ref);

    $response["month_booking_count"]  = $month_booking_count;
    $response["restaurant_details"]   = $restaurant_details;
    $response["booking_list"]         = $upcoming_booking_list;

}elseif($type == "filter_month"){

    $user_hash           = $result -> user_hash;
    $restaurant_type     = $result -> restaurant_type;
    $month_type          = $result -> month_type;
    $restaurant_group_id = $result -> restaurant_group_id;
    $period_filter       = $result -> period_filter;

    if($period_filter === "get_month_list"){

        $month_booking_count = $search_booking_obj -> get_month_list($user_hash,$restaurant_group_id,$restaurant_type,$connect_ref);

        $response["month_booking_count"]  = $month_booking_count;
    }

    $booking_list = $search_booking_obj -> get_booking_list($month_type,$restaurant_type,$user_hash,$restaurant_group_id,$connect_ref);

    
    $response["booking_list"] = $booking_list;

}


echo json_encode($response);




class search_booking{

    function get_month_list($user_hash,$restaurant_group_id,$restaurant_type,$connect_ref){

        // Current month Date && Display Month(Ex:March 2020) && Booking List

        $currenct_date = $this -> get_current_date();

        $current_month  = $this -> get_month_name($currenct_date);

        $month_start_time = $this -> get_start_timestamp($currenct_date);
        $month_end_time   = $this -> get_end_timestamp($currenct_date);

        $list_type = "month_filter";

        $current_booking_count = $this -> get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$month_start_time,$month_end_time);


        // First Previous month Date && Display Month(Ex:Feb 2020) && Booking List

        $first_previous_month_date = $this -> get_previous_month_date($currenct_date);

        $first_previous_month  = $this -> get_month_name($first_previous_month_date);

        $first_previous_month_start_time = $this -> get_start_timestamp($first_previous_month_date);
        $first_previous_month_end_time   = $this -> get_end_timestamp($first_previous_month_date);

        $first_month_booking_count = $this -> get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$first_previous_month_start_time,$first_previous_month_end_time);


        // Second Previous month Date && Display Month(Ex:January 2020) && Booking List

        $second_previous_month_date = $this -> get_previous_month_date($first_previous_month_date);
        
        $second_previous_month  = $this -> get_month_name($second_previous_month_date);

        $second_previous_month_start_time = $this -> get_start_timestamp($second_previous_month_date);
        $second_previous_month_end_time   = $this -> get_end_timestamp($second_previous_month_date);

        $second_month_booking_count = $this -> get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$first_previous_month_start_time,$first_previous_month_end_time);


        // Upcoming Booking List Count

        $list_type = "upcoming";
        $end_time  = 0;

        $current_timestamp = $this -> get_current_timestamp();


        $upcoming_booking_count = $this -> get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$current_timestamp,$end_time);

        
        // All Time List Count

        $list_type   = "all_time";
        $end_time    = 0;
        $start_time  = 0;


        $all_time_booking_count = $this -> get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$start_time,$end_time);



        $month_booking_count = array();

        $month_booking_count[0]["month"] = "Upcoming only";
        $month_booking_count[1]["month"] = $current_month;
        $month_booking_count[2]["month"] = $first_previous_month;
        $month_booking_count[3]["month"] = $second_previous_month;
        $month_booking_count[4]["month"] = "All Time";

        $month_booking_count[0]["count"]  = $upcoming_booking_count;
        $month_booking_count[1]["count"]  = $current_booking_count;
        $month_booking_count[2]["count"]  = $first_month_booking_count;
        $month_booking_count[3]["count"]  = $second_month_booking_count;
        $month_booking_count[4]["count"]  = $all_time_booking_count;

        return $month_booking_count;


    }

    function get_restaurant_list($user_hash,$restaurant_group_id,$connect_ref){

        $status = "active";
        $flag   = 0; 
        $restaurant_details = [];


        $sql = "SELECT `restaurant_id`, `restaurant_name` FROM `restaurant_details` WHERE `restaurant_group_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_group_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name);
            while($stmt -> fetch()){
                $restaurant_details[$flag]["restaurant_id"]   = $restaurant_id;
                $restaurant_details[$flag]["restaurant_name"] = $restaurant_name;
                $flag++;
            }
            $stmt -> close();

        }

        return $restaurant_details;
    }


    function get_current_date(){

        return date('Y-m-d H:i:s');
    }

    function get_current_timestamp(){

        return strtotime(date('Y-m-d H:i:s'));
    }



    function get_start_timestamp($date){

        return strtotime(date('Y-m-01 00:00:00', strtotime($date)));

    }

    function get_end_timestamp($date){

        return strtotime(date('Y-m-t 23:59:59', strtotime($date)));
        
    }

    function get_start_timestamp_by_month($month){

        return strtotime(date('01-m-Y 00:00:00',strtotime('first day of '.$month)));;

    }

    function get_end_timestamp_by_month($month){
 
        return strtotime(date('t-m-Y 12:59:59',strtotime('last day of '.$month)));

    }

    function get_month_name($date){

        return date('F Y',strtotime($date));
    }


    function get_previous_month_date($date){

        return date('Y-m-d', strtotime($date.' -1 MONTH'));
    }

    function get_booking_list_count($user_hash,$restaurant_group_id,$restaurant_type,$list_type,$connect_ref,$start_time,$end_time){

        $status         = "active";
        $booking_count  = 0;

        // echo $month_start_time."<br>".$month_end_time."<br>".$user_hash."<br>".$booking_status."<br>".$status."<br><br>";

        if($list_type === "month_filter"){

            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE (`booking_date` BETWEEN ? AND ?) AND `restaurant_group_id`=? AND `booking_status`=?";
           
            }else{

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE (`booking_date` BETWEEN ? AND ?) AND `restaurant_group_id`=? AND `booking_status`=? AND `restaurant_id`=?";

            }

            

        }else if($list_type === "upcoming"){

            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE (`booking_date` >= ?) AND `restaurant_group_id`=? AND `booking_status`=?";

            }else{

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE (`booking_date` >= ?) AND `restaurant_group_id`=? AND `booking_status`=? AND `restaurant_id`=?";
            }

            

        }else if($list_type === "all_time"){

            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE  `restaurant_group_id`=? AND `booking_status`=?";

            }else{

                $sql = "SELECT COUNT(`booking_id`) FROM `booking_details` WHERE  `restaurant_group_id`=? AND `booking_status`=? AND `restaurant_id`=?";

            }

            

        }


        if($stmt = $connect_ref -> prepare($sql)){

            if($list_type === "month_filter"){

                if($restaurant_type === "all_restaurant"){

                    $stmt -> bind_param("ssss",$start_time,$end_time,$restaurant_group_id,$status);
                
                }else{

                    $stmt -> bind_param("sssss",$start_time,$end_time,$restaurant_group_id,$status,$restaurant_type);

                }

                
    
            }else if($list_type === "upcoming"){

                if($restaurant_type === "all_restaurant"){

                    $stmt -> bind_param("sss",$start_time,$restaurant_group_id,$status);

                }else{

                    $stmt -> bind_param("ssss",$start_time,$restaurant_group_id,$status,$restaurant_type);

                }
    
               
    
            }else if($list_type === "all_time"){
                
                if($restaurant_type === "all_restaurant"){

                    $stmt -> bind_param("ss",$restaurant_group_id,$status);

                }else{

                    $stmt -> bind_param("sss",$restaurant_group_id,$status,$restaurant_type);
                }

                
    
            }
    
            $stmt -> execute();
            $stmt -> bind_result($booking_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $booking_count;

    }

    function get_booking_list($month_type,$restaurant_type,$user_hash,$restaurant_group_id,$connect_ref){

        $booking_list = array();
        $flag = 0; 

        $status = "active";

        if($month_type === "upcoming_only"){ 
            
            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE book_de.`booking_date` >= ? AND book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";

            }else{

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE book_de.`booking_date` >= ? AND book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";

            }

           
       
        }else if($month_type === "All Time"){

            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";
           
            }else{

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";

            }

           

        }else{

            if($restaurant_type === "all_restaurant"){

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE (book_de.`booking_date` BETWEEN ? AND ?) AND book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";

            }else{

                $sql = "SELECT book_de.`booking_id`,book_de.`booking_number`,book_de.`booking_date`,book_de.`booking_time`,book_de.`guest_count`,book_de.`guest_name`,book_de.`status`,book_de.`email_address`,book_de.`phone_number`,book_de.`comments`,book_de.`added_at`,rest_de.`restaurant_name`,rest_de.`restaurant_id` FROM `booking_details` as book_de,`restaurant_details` as rest_de WHERE (book_de.`booking_date` BETWEEN ? AND ?) AND book_de.`restaurant_group_id`=? AND book_de.`booking_status`=? AND book_de.`restaurant_id`=? AND book_de.`restaurant_id` = rest_de.`restaurant_id`";
            }

            

        }

        if($stmt = $connect_ref -> prepare($sql)){

            if($month_type === "upcoming_only"){

                $current_timestamp = $this -> get_current_timestamp();

                if($restaurant_type === "all_restaurant"){

                    //echo $current_timestamp."<br>".$restaurant_group_id."<br>".$status."<br>";

                    $stmt -> bind_param("sss",$current_timestamp,$restaurant_group_id,$status);

                }else{

                   // echo $current_timestamp."<br>".$restaurant_group_id."<br>".$status."<br>".$restaurant_type."<br>";

                    $stmt -> bind_param("ssss",$current_timestamp,$restaurant_group_id,$status,$restaurant_type);
                }

               
            }else if($month_type === "All Time"){

                if($restaurant_type === "all_restaurant"){

                    $stmt -> bind_param("ss",$restaurant_group_id,$status);

                }else{

                    $stmt -> bind_param("sss",$restaurant_group_id,$status,$restaurant_type);
                }


            }else{

                $start_timestamp = $this -> get_start_timestamp_by_month($month_type);
                $end_timestamp   = $this -> get_end_timestamp_by_month($month_type);

                if($restaurant_type === "all_restaurant"){

                    $stmt -> bind_param("ssss",$start_timestamp,$end_timestamp,$restaurant_group_id,$status);

                }else{

                    $stmt -> bind_param("sssss",$start_timestamp,$end_timestamp,$restaurant_group_id,$status,$restaurant_type);

                }


            }

            $stmt -> execute();
            $stmt -> bind_result($booking_id,$booking_number,$booking_date,$booking_time,$guest_count,$guest_name,$status,$email_address,$phone_number,$comments,$added_at,$restaurant_name,$restaurant_id);
            while($stmt -> fetch()){
                $flag++;
                $booking_date_format = date('d F Y',$booking_date); 
                $booking_list[$booking_date_format][$booking_time][$flag]['booking_id']       = $booking_id;
                $booking_list[$booking_date_format][$booking_time][$flag]['booking_number']   = $booking_number;
                $booking_list[$booking_date_format][$booking_time][$flag]['guest_count']      = $guest_count;
                $booking_list[$booking_date_format][$booking_time][$flag]["guest_name"]       = $guest_name;
                $booking_list[$booking_date_format][$booking_time][$flag]["status"]           = $status;
                $booking_list[$booking_date_format][$booking_time][$flag]["email_address"]    = $email_address;
                $booking_list[$booking_date_format][$booking_time][$flag]["phone_number"]     = $phone_number;
                $booking_list[$booking_date_format][$booking_time][$flag]["comments"]         = $comments;
                $booking_list[$booking_date_format][$booking_time][$flag]["added_at"]         = date('d F Y H:i:s',$added_at);
                $booking_list[$booking_date_format][$booking_time][$flag]["restaurant_id"]    = $restaurant_id;
                $booking_list[$booking_date_format][$booking_time][$flag]["restaurant_name"]  = $restaurant_name;
                $booking_list[$booking_date_format][$booking_time][$flag]["booking_date"]     = date('yy-m-d',$booking_date); 
               
            }
            $stmt -> close();

        }  

        return $booking_list;

    }

}
