<?php

include('header.php'); //includes the database connectivity files
require("sendgrid-php/new_register_mails.php"); //includes the email sending files
//require("sms.php");
require_once './vendor/autoload.php';
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$result 		 = json_decode($_POST['myData']); //decodes the data from the request from the client
$con_obj     	 = new dbcon();                   //new instance created for db connectivity
$connect_ref 	 = $con_obj -> connect();           //contains the object to store connect_red
$type            = $result -> type;                  //request type to perform particular type

$email_obj     	 = new email_sender();                   //new instance created for email sender

$response 		  = array();                       //response array stores response data
$booking_details_obj = new booking_details();   	   //new instance created for payment_request_class



if($type == "new_booking"){

    $booking_date        = $result -> booking_date;
    $booking_time        = $result -> booking_time;
    $status              = $result -> status;
    $assigned_table      = $result -> assigned_table;
    $guest_name          = $result -> guest_name;
    $guest_count         = $result -> guest_count;
    $email_address       = $result -> email_address;
    $phone_number        = $result -> phone_number;
    $additional_notes    = $result -> additional_notes;
    $send_email          = $result -> send_email;
    $send_sms            = $result -> send_sms;
    $send_advance_sms    = $result -> send_advance_sms;
    $restaurant_notify   = $result -> restaurant_notify;
    $user_hash           = $result -> user_hash;
    $restaurant_id       = $result -> restaurant_id;
    $booking_id          = $result -> booking_id;
    $language            = $result -> language;
    $alergy_type         = $result -> alergy_type;
    $customer_need       = $result -> customer_need;
    $country_code        = $result -> country_code;
    $restaurant_email    = $result -> restaurant_email;
    $booking_type        = $result -> booking_type;
    $restaurant_group_id = $result -> restaurant_group_id;
    $table_guest_count   = $result -> table_guest_count;

    $response    = $booking_details_obj -> add_new_booking($restaurant_group_id,$booking_id,$booking_type,$restaurant_id,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$send_email,$send_sms,$send_advance_sms,$restaurant_notify,$user_hash,$response,$language,$alergy_type,$customer_need,$country_code,$restaurant_email,$table_guest_count,$connect_ref,$email_obj);

}else if($type == "get_restaurant"){

    $user_hash = $result -> user_hash;

    $response  = $booking_details_obj -> get_restaurant_data($user_hash,$response,$connect_ref);
   
    $response  = $booking_details_obj -> get_table_count($response,$connect_ref);


}else if($type == "get_booking_data"){

    $date          = $result -> date;
    $user_hash     = $result -> user_hash;
    $restaurant_id = $result -> restaurant_id;


    $response  = $booking_details_obj -> get_booking_data_for_date($date,$restaurant_id,$user_hash,$response,$connect_ref);
   

}else if($type == "delete_booking"){

    $booking_id = $result -> booking_id;
    $user_hash  = $result -> user_hash;

    $response  = $booking_details_obj -> delete_booking($booking_id,$user_hash,$response,$connect_ref);

}else if($type == "get_booking_data_for_id"){

    $booking_id = $result -> booking_id;
    $user_hash = $result -> user_hash;
  
    $response  = $booking_details_obj -> get_booking_data_for_id($booking_id,$user_hash,$response,$connect_ref);


}else if($type == "get_booking_month_date"){

    $month_year = $result -> month_year;
    $user_hash  = $result -> user_hash;
    $restaurant_id = $result -> restaurant_id;


    $response  = $booking_details_obj -> get_booking_month_date($month_year,$restaurant_id,$user_hash,$response,$connect_ref);
    $response  = $booking_details_obj -> get_table_data_for_date($restaurant_id,$response,$connect_ref);


}else if($type == "widget_onload"){

    $restaurant_group_id = $result -> restaurant_group_id;
    $restaurant_id       = $result -> restaurant_id;

    $response  = $booking_details_obj -> get_widget_restaurant_data($restaurant_group_id,$restaurant_id,$response,$connect_ref);
   
}else if($type == "booking_time"){

    $restaurant_id = $result -> restaurant_id;

    $response["booking_time"]  = $booking_details_obj -> get_booking_time($restaurant_id,$connect_ref);
    $response["guest_count"]   = $booking_details_obj -> get_guest_count($restaurant_id,$connect_ref);

}else if($type == "booking_time_with_guest_count"){

    $restaurant_id = $result -> restaurant_id;
    $guest_count   = $result -> guest_count;
    $date   = $result -> date;

    //$response  = $booking_details_obj -> booking_time_with_guest_count($guest_count,$restaurant_id,$date,$connect_ref);
    $response  = $booking_details_obj -> booking_time_and_booked_time_for_guest($guest_count,$restaurant_id,$date,$connect_ref);

    $response["booking_time"]  = $booking_details_obj -> get_booking_time($restaurant_id,$connect_ref);

    
}else if($type == "get_booking_interval"){

    $restaurant_id = $result -> restaurant_id;

    $response = $booking_details_obj -> get_booking_interval($restaurant_id,$connect_ref);


}else if($type == "get_sms_settings"){

    $user_hash = $result -> user_hash;

    $restaurant_group_id = $booking_details_obj -> get_restaurant_group_id($user_hash,$connect_ref);

    $sms_db_data = $booking_details_obj -> get_sms_details($restaurant_group_id, $connect_ref);

    $is_sms_enabled = $sms_db_data["enable_sms"];

    $response["is_sms_enabled"]  =  $is_sms_enabled;

}else if($type == "get_phone_number"){

    $restaurant_id = $result -> restaurant_id;

    $response = $booking_details_obj -> get_restaurant_phone_number($restaurant_id,$connect_ref);

}   





echo json_encode($response);



class booking_details{

    function add_new_booking($restaurant_group_id,$booking_id,$booking_type,$restaurant_id,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$send_email,$send_sms,$send_advance_sms,$restaurant_notify,$user_hash,$response,$language,$alergy_type,$customer_need,$country_code,$restaurant_email,$table_guest_count,$connect_ref,$email_obj){

       $booking_process_status = "true";

        if($booking_id != "yet_to_generate"){

            $booking_number = $this -> get_exist_booking_number($booking_id,$connect_ref);
            $this -> change_exist_booking_status($booking_id,$connect_ref);
            
        }else{  

            $booking_id = $this -> get_booking_id($connect_ref);
            $booking_number  = $this -> get_booking_number($restaurant_id, $connect_ref);
            
        }

        
        $restaurant_details      = $this -> get_restaurant_name($restaurant_id,$connect_ref);
        $restaurant_name = $restaurant_details ["restaurant_name"];
        $restaurant_email = $restaurant_details ["restaurant_email"];
        $restaurant_phone_number = $restaurant_details ["restaurant_phone_number"];
        $restaurant_address = $restaurant_details ["restaurant_address"];
        

        $booking_status       = "active";
        $added_at             = $this -> current_timestamp();
        $booking_date         = strtotime($booking_date);
        
        $booking_display_date = date('l d F Y', $booking_date);

        $guest_count = (int)$guest_count;
        

        if($booking_id != "yet_to_generate"){

            //$table_id = $this -> get_assign_table($table_guest_count,$booking_time,$restaurant_id,$connect_ref);
            $assign_table_details = $this -> get_assign_table_for_booking($table_guest_count,$booking_date,$booking_time,$restaurant_id,$connect_ref);

            // echo "final_table_id ==>";
            // echo $table_id."<br>";

            // echo "\nassign_table_details\n";
            // print_r($assign_table_details);

           if($assign_table_details["table_status"] === "not_available"){

                $booking_process_status = "false";

            }else{

                $this -> insert_booking_tables($assign_table_details["table_details"],$restaurant_id,$booking_id,$booking_status,$connect_ref);

            }
            
        }

        if($booking_process_status === "true"){

             // echo $restaurant_id."-".$booking_id."_".$booking_number."-".$user_hash."-".$booking_date."-".$booking_time."-".$status."-".$assigned_table."-".$guest_name."-".$guest_count."-".$email_address."-".$phone_number."-".$additional_notes."-".$send_email."-".$send_sms."-".$send_advance_sms."-".$restaurant_notify."-".$added_at."-".$booking_status."-".$language."-".$alergy_type."-".$customer_need."-".$country_code."<br>";
        
            $sql = "INSERT INTO `booking_details`(`restaurant_group_id`,`restaurant_id`,`booking_id`,`booking_number`, `user_hash`, `booking_date`,`booking_time`,`status`, `assigned_tables`, `guest_name`, `guest_count`, `email_address`, `phone_number`, `comments`, `email_confirmation_status`, `sms_confirmation_status`, `sms_reminder`, `restaurant_notification_status`, `added_at`, `booking_status`,`language`,`alergy_type`,`customer_need`,`country_code`,`table_guest_count`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('sssssssssssssssssssssssss',$restaurant_group_id,$restaurant_id,$booking_id,$booking_number,$user_hash,$booking_date,$booking_time,$status,$assigned_table,$guest_name,$guest_count,$email_address,$phone_number,$additional_notes,$send_email,$send_sms,$send_advance_sms,$restaurant_notify,$added_at,$booking_status,$language,$alergy_type,$customer_need,$country_code,$table_guest_count);
                $stmt -> execute();
                if(($stmt -> affected_rows) > 0 && ($stmt -> affected_rows) != "") $response = "true";
                else $response = "false";
                $stmt -> close();

            }else{
                $response = "false";
            }

        

            //echo $response."<br>";

            // get booking duration minutes

            $duration = $this -> get_restaurant_booking_duration($restaurant_id,$connect_ref);

            
            $this -> insert_booking_time_in_duration($restaurant_id,$booking_id,$booking_date,$booking_time,$guest_count,$duration,$added_at,$booking_status,$connect_ref);



            // echo $response."<br>";
            // echo $table_id."<br>";
            $alergy_type = str_replace("-"," ",$alergy_type); 
            $alergy_type = str_replace(",",", ",$alergy_type); 
            $customer_need = str_replace("_"," ",$customer_need); 

            $booking_duration = $duration." minutes";
            
            if($response === "true"){

                if($send_email === "true"){
                        $email_to= "guest";
                        // this email to notify the guest if any new booking is received or order 
                        $email_obj -> send_new_restaurant_booking_email($guest_name, $guest_count, $email_address, $booking_display_date, $booking_time, $restaurant_name, $status, $booking_type, $email_to, $additional_notes, $restaurant_email, $restaurant_phone_number, $restaurant_address, $alergy_type, $customer_need,$booking_number,$booking_duration);
                    
                    
                }

                if($send_sms === "true"){

                        
                        $restaurant_group_id = $this -> get_restaurant_group_id($user_hash,$connect_ref);

                        $sms_db_data = $this -> get_sms_details($restaurant_group_id, $connect_ref);

                        $is_sms_enabled = $sms_db_data["enable_sms"];

                        if($is_sms_enabled == "true" || $is_sms_enabled == true ){

                            $sms_title = $sms_db_data["sender_name"];

                            $basic  = new \Nexmo\Client\Credentials\Basic('e9c019f6', '26vto0soDl6X9Hgg');
                            $client = new \Nexmo\Client($basic);

                            $country_code = str_replace("+","",$country_code);  // replace + from country code first character
                            $to_mobile_no = $country_code.$phone_number;
                            $msg_title = $sms_title;

                            if($booking_type === "new_booking"){

                                $msg_content = 'Dear '.$guest_name.', Your booking('.$booking_number.') at '.$restaurant_name.' on '.$booking_display_date.' at '.$booking_time.' for '.$guest_count.' guests.';
                            
                            }else{
                                
                                $msg_content = 'Dear '.$guest_name.', Your booking at '.$restaurant_name.' has been modified. Info'.$booking_display_date.' at '.$booking_time.' for '.$guest_count.' guests.';
                            
                            }
                            
                                $message = $client->message()->send([
                                    'to' => $to_mobile_no,
                                    'from' => $msg_title,
                                    'text' => $msg_content
                                ]);

                                $sms_status = "sent";
                                $sms_content = $msg_content;
                                $sql = "INSERT INTO `sent_sms_details`(`restaurant_id`,`restaurant_group_id`,`booking_id`,`phone_number`, `sms_content`, `sms_status`, `added_at`) VALUES (?,?,?,?,?,?,?)";

                                if($stmt = $connect_ref -> prepare($sql)){
                                    $stmt -> bind_param('sssssss',$restaurant_id,$restaurant_group_id,$booking_id,$to_mobile_no,$sms_content,$sms_status,$added_at);
                                    $stmt -> execute();
                                    $stmt -> close();
                                }

                        }

                }

                if($restaurant_notify === "true" && $booking_type === "new_booking"){
                    // this email to notify the restaurant if any new booking is received
                    $email_to = "restaurant";
                    $phone_number = $country_code." ".$phone_number;
                    //                                                            $guest_name, $guest_count, $restaurant_email, $guest_email, $booking_date, $booking_time, $restaurant_name, $booking_status, $booking_type, $email_to, $guest_phone, $booking_id
                    $email_obj -> send_new_restaurant_booking_email_to_restaurant($guest_name, $guest_count, $restaurant_email, $email_address, $booking_display_date, $booking_time, $restaurant_name, $status, $booking_type, $email_to, $phone_number, $booking_id, $additional_notes, $alergy_type, $customer_need,$booking_number,$booking_duration);
                
                }
            
            }
        
            return $response;
        
        }else{

            return "table_not_available";
        }

    }

    function insert_booking_time_in_duration($restaurant_id,$booking_id,$booking_date,$booking_time,$guest_count,$duration,$added_at,$booking_status,$connect_ref){

        $booking_interval = $this -> get_booking_interval($restaurant_id,$connect_ref);
        $restaurant_time_details = $this -> get_restaurant_end_time($restaurant_id,$connect_ref);

        $restaurant_start_time = number_format((str_replace(":",".",$restaurant_time_details[0])),2);
        $restaurant_end_time = number_format((str_replace(":",".",$restaurant_time_details[1])),2);

        $interval = $booking_interval["interval"];


        $duration_string = " +".$duration." minutes";


        $start_time_check         = number_format((str_replace(":",".",$booking_time)),2);
        $end_time                 = date('H:i',strtotime($start_time_check . $duration_string));
        $end_time_check           = number_format((str_replace(":",".",$end_time)),2);

        $start_time_minutes     = $this -> get_minutes($booking_time);
        $end_of_the_day_minutes = $this -> get_minutes("24:00");

        $end_time_check = ((int)($end_of_the_day_minutes - $start_time_minutes) >= (int)$duration?$end_time_check:"24.00");

        $interval_string = " +".$interval." minutes";

        $looping_time = $start_time_check;

        $restaurant_time = $booking_time;

        while($looping_time < $end_time_check && $looping_time >= $start_time_check && $looping_time <= $restaurant_end_time && $looping_time >= $restaurant_start_time){

            if($looping_time !== "0.00"){

                $sql = "INSERT INTO `booking_time`(`restaurant_id`, `booking_id`, `booking_date`, `booking_time`, `guest_count`, `status`, `added_at`) VALUES (?,?,?,?,?,?,?)";

                if($stmt = $connect_ref -> prepare($sql)){

                    $stmt -> bind_param('sssssss',$restaurant_id,$booking_id,$booking_date,$restaurant_time,$guest_count,$booking_status,$added_at);
                    $stmt -> execute();
                    $stmt -> close();
                }

                $time = date('H:i',strtotime($looping_time . $interval_string));

                $restaurant_time = $time;

                $looping_time = number_format((str_replace(":",".",$time)),2);

            }else{

                break;
            }
    
        }

    }

    function get_minutes($time_string) {
      
        $parts = explode(":", $time_string);
    
        $hours = intval($parts[0]);
        $minutes = intval($parts[1]);
    
        return $hours * 60 + $minutes;
    }

    
    function get_restaurant_end_time($restaurant_id,$connect_ref){

        $status = "active"; 
        $restaurant_start_time = "";
        $restaurant_end_time   = "";
       
        $sql = "SELECT `restaurant_start_time`,`restaurant_end_time` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_start_time,$restaurant_end_time);
            $stmt -> fetch();
            $stmt -> close();
        }

        return array($restaurant_start_time,$restaurant_end_time);


    }


    function get_restaurant_phone_number($restaurant_id,$connect_ref){

        $status = "active"; 
        $phone_number = "false";

        $sql = "SELECT `phone_number` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($phone_number);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $phone_number;

    }

    function get_restaurant_booking_duration($restaurant_id,$connect_ref){

        $status = "active"; 
        $booking_duration = "";

        $sql = "SELECT `booking_duration` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($booking_duration);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $booking_duration;

    }

    function get_sms_details($restaurant_group_id, $connect_ref){

        $count = 0;
        $enable_sms = false;
        $sender_name = "";
        $sms_data = array();

        $sql = "SELECT count(*),`enable_sms`,`sender_name` FROM `sms_settings` where `restaurant_group_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_group_id);
            $stmt -> execute();
            $stmt -> bind_result($count,$enable_sms,$sender_name);
            $stmt -> fetch();
            $stmt -> close();

        }

        $sms_data["enable_sms"] = $enable_sms;
        $sms_data["sender_name"] = $sender_name;
        $sms_data["count"] = $count;

        return $sms_data;

    }


    function get_booking_number($restaurant_id, $connect_ref){


        $booking_count = 0;

        $sql = "SELECT MAX(`sno`) FROM `booking_details` WHERE `restaurant_id`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($booking_count);
            $stmt -> fetch();
            $stmt -> close();
            $booking_count = $booking_count+1;

        }
      
        $booking_length = strlen((string)$booking_count);
       
        if(((int)($booking_length)) == 1){

         
            return "#000".$booking_count;

        }else if((int)($booking_length) == 2){

            return "#00".$booking_count;

        }else if((int)($booking_length) == 3){

            return "#0".$booking_count;

        }else{
            return "#".$booking_count;
        }

    }

    function insert_booking_tables($assign_table_details,$restaurant_id,$booking_id,$booking_status,$connect_ref){


        $table_response = "true";


        foreach ($assign_table_details as $table_data) {

            // print_r($table_data);

            // echo "\n".$table_data["table_id"]."<br>".$table_data["availble_guest_count"]."<br>".$restaurant_id."<br>".$booking_id."<br>".$booking_status."<br>";


            $sql = "INSERT INTO `booked_table`(`table_id`,`available_guest_count`,`restaurant_id`, `booking_id`, `status`) VALUES (?,?,?,?,?)";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('sssss',$table_data["table_id"],$table_data["availble_guest_count"],$restaurant_id,$booking_id,$booking_status);
               
                $stmt -> execute();
                if(($stmt -> affected_rows) > 0 && ($stmt -> affected_rows) != "") $response = "true";
                else  $table_response = "false";
    
                $stmt -> close();
    
            }else{
                $table_response = "false";
            }

        }

      

    }


    function get_exist_booking_number($booking_id,$connect_ref){

        $booking_number = "";
        $status = "active";

        $sql = "SELECT `booking_number` FROM `booking_details` WHERE `booking_id`=? AND `booking_status`=?";
        
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$booking_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($booking_number);
            $stmt -> fetch();
            $stmt -> close();
          
        }
          
        return $booking_number;

    }


    function change_exist_booking_status($booking_id,$connect_ref){

        $checking_status = "active";
        $modified_status = "inactive";


        $sql = "UPDATE `booking_details` SET `booking_status`=? WHERE `booking_id`=? AND `booking_status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sss',$modified_status,$booking_id,$checking_status);
           
            $stmt -> execute();
            if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") $response = "true";
            else  $response = "false";

            $stmt -> close();

        }else{
            $response = "false";
        }


    }

    function booked_table_count_for_guest($guest_count,$booking_time,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$connect_ref){

        $count  = 0;
        $status = "active";

        $sql = "SELECT COUNT(*) FROM `booking_details` WHERE `restaurant_id`=? AND `table_guest_count`=? AND `booking_date` BETWEEN ? AND ? AND  `booking_status`=? AND `booking_time`=?";
        
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ssssss',$restaurant_id,$guest_count,$start_of_booking_date,$end_of_booking_date,$status,$booking_time);
            $stmt -> execute();
            $stmt -> bind_result($count);
            $stmt -> fetch();
            $stmt -> close();
            
        }

        return $count;
    
    }


    function check_guest_count_available($guest_count,$booking_time,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$connect_ref){

        $table_count = $this -> find_table_count_for_guest($guest_count,$restaurant_id,$connect_ref);

        $booked_table_count = $this -> booked_table_count_for_guest($guest_count,$booking_time,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$connect_ref);

        $restaurant_guest_count = $this -> get_guest_count_for_restaurant($restaurant_id,$connect_ref);



        if($table_count === 0 || $table_count === $booked_table_count){

            if($restaurant_guest_count[count($restaurant_guest_count)-1] === $guest_count && $restaurant_guest_count[count($restaurant_guest_count)-1] <= $guest_count){

                return "table_not_available";

            }else{

                $guest_count = $this -> find_next_guest_count($guest_count,$restaurant_guest_count);

                return $this -> check_guest_count_available($guest_count,$booking_time,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$connect_ref);
    
            }
        
        }else{

            return $guest_count;
        }

    }


    function get_assign_table_for_booking($guest_count,$booking_date,$booking_time,$restaurant_id,$connect_ref){
    
        $start_of_booking_date = strtotime(date('Y-m-d 00:00:00', $booking_date));
        $end_of_booking_date   = strtotime(date('Y-m-d 23:59:59', $booking_date));

        $assign_tables = array();
        $assign_tables["table_status"] = "not_available";
    
    
        $available_guest_table = $this -> get_assigned_table_available_guest_count($booking_date,$start_of_booking_date,$end_of_booking_date,$booking_time,$restaurant_id,$connect_ref);

        $available_total_guest = $this -> get_avilable_total_guest($available_guest_table);

        krsort($available_guest_table);


        $table_details = $this -> get_restaurant_available_tables($start_of_booking_date,$end_of_booking_date,$booking_time,$restaurant_id,$connect_ref);

        $table_list = $table_details[0];
        $available_guest_count = $table_details[1];

        krsort($table_list);
        krsort($available_guest_count);

        // echo "\ntable_list ==>";
        // print_r($table_list);

        // echo "\navailable_guest_count ==>";
        // print_r($available_guest_count);


      
        $seach_guest_in_available_table = array_search($guest_count, $available_guest_count);

        // echo "\seach_guest_in_available_table ==>";
        // echo $seach_guest_in_available_table;


        if(count($table_list) !== 0){

            if($seach_guest_in_available_table){

                $table_index = $seach_guest_in_available_table; 

                $assign_tables["table_details"][0]["table_id"]             = $table_list[$table_index]["table_id"];
                $assign_tables["table_details"][0]["availble_guest_count"] = (int)($table_list[$table_index]["guest_count"])-(int)($guest_count);
                $assign_tables["table_details"][0]["table_guest_count"]    = $table_list[$table_index]["guest_count"];
                $assign_tables["table_status"]            = "available";
                $assign_tables["booking_status"]          = "automatic";

            }else{

                $next_available_table = $this -> find_next_available_table($table_list,$guest_count); //asc sort


                if($next_available_table !== "no_table_available"){

                    $assign_tables["table_details"][0]["table_id"]             = $next_available_table["table_id"];
                    $assign_tables["table_details"][0]["availble_guest_count"] = (int)($next_available_table["guest_count"])-(int)($guest_count);
                    $assign_tables["table_details"][0]["table_guest_count"]    = $next_available_table["guest_count"];
                    $assign_tables["table_status"]            = "available";
                    $assign_tables["booking_status"]          = "automatic";


                }else if(array_sum($available_guest_count) >= $guest_count){

                    $remaining_guest_count = $guest_count;
                    $temp = 0;

                    // echo "\nremaining_guest_count ==>";
                    // echo $remaining_guest_count;
                    

                    foreach ($table_list as $key => $table_data) {

                        if($remaining_guest_count !== 0){

                            $available_seat_count = abs((int)($remaining_guest_count) - (int)($table_data["guest_count"]));

                            $available_seat_count = ((int)$remaining_guest_count < (int)$table_data["guest_count"])?$available_seat_count:0;

                    
                            $assign_tables["table_details"][$temp]["table_id"]             = $table_data["table_id"];
                            $assign_tables["table_details"][$temp]["availble_guest_count"] =  $available_seat_count;
                            $assign_tables["table_details"][$temp]["table_guest_count"]    = $table_data["guest_count"];
                            $assign_tables["table_status"]               = "available";
                            $assign_tables["booking_status"]             = "automatic";

                            $remaining_guest_count = (int)($remaining_guest_count) - (int)($table_data["guest_count"]);

                            $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                            $temp++;

                            unset($table_list[$key]);

                            if($remaining_guest_count !== 0){

                                $search_result = $this -> check_remaining_guest_count_is_available($assign_tables,$table_list,$remaining_guest_count,$temp);
                           
                                if($search_result[0] === "guest_search_available"){

                                    $assign_tables         = $search_result[1];
                                    $temp                  = $search_result[2];
                                    $remaining_guest_count = $search_result[3];
                                    $table_list            = $search_result[4];

                                }else if($search_result[0] === "guest_search_not_available"){

                                    continue;
                                }

                            }

                        }else{
                            break;
                        }
                    }

                   
                }else if((array_sum($available_guest_count)+(int)$available_total_guest) >= $guest_count){

                    $remaining_guest_count = $guest_count;
                    $temp = 0;

                    foreach ($table_list as $key => $table_data) {

                        if($remaining_guest_count !== 0){

                            $available_seat_count = abs((int)($remaining_guest_count) - (int)($table_data["guest_count"]));

                            $available_seat_count = ((int)$remaining_guest_count < (int)$table_data["guest_count"])?$available_seat_count:0;

                    
                            $assign_tables["table_details"][$temp]["table_id"]             = $table_data["table_id"];
                            $assign_tables["table_details"][$temp]["availble_guest_count"] =  $available_seat_count;
                            $assign_tables["table_details"][$temp]["table_guest_count"]    = $table_data["guest_count"];
                            $assign_tables["table_status"]               = "available";
                            $assign_tables["booking_status"]             = "automatic";

                            $remaining_guest_count = (int)($remaining_guest_count) - (int)($table_data["guest_count"]);

                            $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                            $temp++;
                        }else{
                            break;
                        }
                    }

                    if($remaining_guest_count !== 0){

                        foreach ($available_guest_table as $key => $available_table_data) {
                           
                            if($remaining_guest_count !== 0){

                                $available_seat_count = abs((int)($remaining_guest_count) - (int)($available_table_data["guest_count"]));
    
                                $available_seat_count = ((int)$remaining_guest_count < (int)$available_table_data["guest_count"])?$available_seat_count:0;
    
                        
                                $assign_tables["table_details"][$temp]["table_id"]             = $available_table_data["table_id"];
                                $assign_tables["table_details"][$temp]["availble_guest_count"] =  $available_seat_count;
                                $assign_tables["table_details"][$temp]["table_guest_count"]    = $available_table_data["guest_count"];
                                $assign_tables["table_status"]                                = "available";
                                $assign_tables["booking_status"]                              = "manual_booking";
    
                                $remaining_guest_count = (int)($remaining_guest_count) - (int)($available_table_data["guest_count"]);
    
                                $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                                $temp++;
                            }else{
                                break;
                            }
                        }   
                    }


                }else{

                    if(count($available_guest_table) !== 0 && $available_total_guest >= $guest_count){

                        $remaining_guest_count = $guest_count;
                        $temp = 0;

                        foreach ($available_guest_table as $key => $table_data) {

                            if($remaining_guest_count !== 0){

                                $available_seat_count = abs((int)($remaining_guest_count) - (int)($table_data["guest_count"]));
    
                                $available_seat_count = ((int)$remaining_guest_count < (int)$table_data["guest_count"])?$available_seat_count:0;
    
                        
                                $assign_tables["table_details"][$temp]["table_id"]             = $table_data["table_id"];
                                $assign_tables["table_details"][$temp]["availble_guest_count"] =  $available_seat_count;
                                $assign_tables["table_details"][$temp]["table_guest_count"]    = $table_data["guest_count"];
                                $assign_tables["table_status"]                                = "available";
                                $assign_tables["booking_status"]                              = "manual_booking";
    
                                $remaining_guest_count = (int)($remaining_guest_count) - (int)($table_data["guest_count"]);
    
                                $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                                $temp++;
                            }else{
                                break;
                            }
                        }   

                    }

                }   
            }

        }else{

            if(count($available_guest_table) !== 0 && $available_total_guest >= $guest_count){

                $remaining_guest_count = $guest_count;
                $temp = 0;


               foreach ($available_guest_table as $key => $table_data) {

                    if($remaining_guest_count !== 0){

                        $available_seat_count = abs((int)($remaining_guest_count) - (int)($table_data["guest_count"]));

                        $available_seat_count = ((int)$remaining_guest_count < (int)$table_data["guest_count"])?$available_seat_count:0;

                
                        $assign_tables["table_details"][$temp]["table_id"]             = $table_data["table_id"];
                        $assign_tables["table_details"][$temp]["availble_guest_count"] =  $available_seat_count;
                        $assign_tables["table_details"][$temp]["table_guest_count"]    = $table_data["guest_count"];
                        $assign_tables["table_status"]                                = "available";
                        $assign_tables["booking_status"]                              = "manual_booking";

                        $remaining_guest_count = (int)($remaining_guest_count) - (int)($table_data["guest_count"]);

                        $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                        $temp++;
                    }else{
                        break;
                    }
                }   

            }

        }

       
        return $assign_tables;


    }

    function check_remaining_guest_count_is_available($assign_tables,$table_list,$remaining_guest_count,$temp){

        foreach ($table_list as $key => $table_list_data) {   // $arr is your initial array
            
            if ($table_list_data['guest_count'] == $remaining_guest_count) {
               
                $available_seat_count = abs((int)($remaining_guest_count) - (int)($table_list_data["guest_count"]));

                $available_seat_count = ((int)$remaining_guest_count < (int)$table_list_data["guest_count"])?$available_seat_count:0;

        
                $assign_tables["table_details"][$temp]["table_id"]             = $table_list_data["table_id"];
                $assign_tables["table_details"][$temp]["availble_guest_count"] = $available_seat_count;
                $assign_tables["table_details"][$temp]["table_guest_count"]    = $table_list_data["guest_count"];
                $assign_tables["table_status"]                                 = "available";
                $assign_tables["booking_status"]                               = "automatic";

                $remaining_guest_count = (int)($remaining_guest_count) - (int)($table_list_data["guest_count"]);

                $remaining_guest_count = ($remaining_guest_count < 0)?0:$remaining_guest_count;
                $temp++;
                
                unset($table_list[$key]);

                if($remaining_guest_count !== 0){

                    return $this -> check_remaining_guest_count_is_available($assign_tables,$table_list,$remaining_guest_count,$temp);

                }else{

                    return array("guest_search_available",$assign_tables,$temp,$remaining_guest_count,$table_list);
                    break;  

                }
            }
        }

        return array("guest_search_not_available");
        
    }

    function get_avilable_total_guest($available_guest_count){

        $sum = 0;
        foreach ($available_guest_count as $table_data) {
            $sum += $table_data["guest_count"];
        }
        return $sum;
    }

    function find_next_available_table($table_list,$guest_count){

        $table_list_data = $table_list;

        ksort($table_list_data); 

        //print_r($table_list_data);

        foreach ($table_list_data as $table_data) {

            if ((int)$table_data["guest_count"] > $guest_count) {
                return $table_data;
            }
        }
       
        return "no_table_available";

    }

    function sortByOrder($a, $b) {
        return $a['order'] - $b['order'];
    }


    function get_restaurant_available_tables($start_of_booking_date,$end_of_booking_date,$booking_time,$restaurant_id,$connect_ref){

        $status = "active";
        $booked_status = "cancelled";
        $table_list = [];
        $available_guest_count = array();
        $temp = 1;

       
        $sql = "SELECT `table_id`, `guest_count` FROM `restaurant_tables` WHERE `table_id` NOT IN (SELECT `table_id` FROM `booked_table` WHERE `status`=? AND `booking_id` IN (SELECT `booking_id` FROM `booking_time` WHERE `restaurant_id`=? AND `booking_date` BETWEEN ? AND ? AND `booking_time`=? AND `status`=?)) AND `restaurant_id`=? AND `status`=?";
    
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ssssssss',$status,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$booking_time,$status,$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_id,$table_guest_count);
            while($stmt -> fetch()){
                $table_list[$temp]["table_id"]    = $table_id;
                $table_list[$temp]["guest_count"] = $table_guest_count;
                $available_guest_count[$temp] = $table_guest_count;
                $temp++;
               
            }
            $stmt -> close();
            
        }

        return array($table_list, $available_guest_count);

    }


    function get_assigned_table_available_guest_count($booking_date,$start_of_booking_date,$end_of_booking_date,$booking_time,$restaurant_id,$connect_ref){

        $status                = "active";
        $booked_status         = "cancelled";
        $table_assigned_list   = [];
        $temp = 0;

        // echo "\n".$status."<br>".$restaurant_id."<br>".$start_of_booking_date."<br>".$end_of_booking_date."<br>".$booking_time."<br>".$status."\n";

        $sql = "SELECT `table_id`,`available_guest_count` FROM `booked_table` WHERE `status`=? AND `booking_id` IN (SELECT `booking_id` FROM `booking_time` WHERE `restaurant_id`=? AND `booking_date` BETWEEN ? AND ? AND `booking_time`=? AND `status`=?)";
    
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ssssss',$status,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$booking_time,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_id,$available_guest_count);
            while($stmt -> fetch()){

                if($available_guest_count === 0){

                    // echo "\ntable if enter\n";


                    foreach ($table_assigned_list as $key => $table_data) {
                        
                        if ($table_data["table_id"] === $table_id) {
                            unset($table_data[$key]);
                        }

                    }
                
                }else{

                    // echo "\ntable else enter\n";

                    $table_assigned_list[$temp]["table_id"]    = $table_id;
                    $table_assigned_list[$temp]["guest_count"] = $available_guest_count;
                    $temp++;
                }

            
            }
            $stmt -> close();
            
        }

        return $table_assigned_list;

    }



    function get_assign_table($guest_count,$booking_time,$restaurant_id,$connect_ref){

        $status = "active";

        $restaurant_table = [];
        $temp = 0;

        $start_of_booking_date = strtotime(date('Y-m-d 00:00:00'));
        $end_of_booking_date   = strtotime(date('Y-m-d 23:59:59'));

        $guest_count = $this -> check_guest_count_available($guest_count,$booking_time,$restaurant_id,$start_of_booking_date,$end_of_booking_date,$connect_ref);

        if($guest_count !== "table_not_available"){

            $sql = "SELECT `table_id` FROM `restaurant_tables` WHERE `restaurant_id`=? AND `guest_count`=? AND `status`=?";
       
            if($stmt = $connect_ref -> prepare($sql)){
    
                $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
                $stmt -> execute();
                $stmt -> bind_result($table_id);
                while($stmt -> fetch()){
                    $restaurant_table[$temp] = $table_id;
                    $temp++;
    
                }
                $stmt -> close();
                
            }
    
            $booked_tables = [];
            $temp = 0;
    
    
            $sql = "SELECT book_tab.`table_id` FROM `booked_table` as book_tab,`booking_details` as book_det WHERE book_tab.`restaurant_id`=? AND book_tab.`status`=? AND (book_det.`booking_date` BETWEEN ? AND ?) AND book_det.`guest_count`=? AND book_det.`booking_time`=? AND book_tab.`restaurant_id`= book_det.`restaurant_id` AND book_tab.`booking_id` = book_det.`booking_id`";
    
            if($stmt = $connect_ref -> prepare($sql)){
    
                $stmt -> bind_param('ssssss',$restaurant_id,$status,$start_of_booking_date,$end_of_booking_date,$guest_count,$booking_time);
                $stmt -> execute();
                $stmt -> bind_result($table_id);
                while($stmt -> fetch()){
                    $booked_tables[$temp] = $table_id;
                    $temp++;
                }
                $stmt -> close();
                
            }
    
    
            $out1 = array_diff($restaurant_table, $booked_tables);
            $out2 = array_diff($booked_tables, $restaurant_table);
            $final_book_table = array_merge($out1, $out2);
            
            // echo "<br>";
            // echo "array1 ==>";
            // print_r($out1);
            // echo "<br>";
            // echo "array2 ==>";
            // print_r($out2);
            // echo "<br>";
            // echo "final array ==>";
            // print_r($final_book_table);
            // echo "<br>";
    
            if($final_book_table != "" ){
    
                return $final_book_table[0];

            }else{

                return "false";
            }

        }

        return "false";
    }

    function find_next_guest_count($guest_count,$restaurant_guest_count){

        asort($restaurant_guest_count);

        $nearby_guest_count = 0;

        for($index = 0; $index < sizeof($restaurant_guest_count);$index++){

            if($restaurant_guest_count[$index] > $guest_count){

           
                $nearby_guest_count = $restaurant_guest_count[$index];

                break;
            }
            
        }

        return $nearby_guest_count;

    }


    function find_table_count_for_guest($guest_count,$restaurant_id,$connect_ref){

        $count = 0;
        $status = "active";

        $sql = "SELECT COUNT(`table_id`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `guest_count`=? AND `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
            $stmt -> execute();
            $stmt -> bind_result($count);
            $stmt -> fetch();
            $stmt -> close();       
        }
        return $count;
    }

    function get_guest_count_for_restaurant($restaurant_id,$connect_ref){

        $status = "active";
        $guest_count_list = []; 
        $temp = 0;

        $sql = "SELECT `guest_count` FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";
        
        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($guest_count);
            while($stmt -> fetch()){
                $guest_count_list[$temp] = $guest_count;
                $temp++;
            }
            $stmt -> close();       
        }

        return $guest_count_list;
    }



    function current_timestamp(){

        return strtotime(date("Y-m-d H:i:s"));

    }


    function get_booking_id($connect_ref){

        /*$booking_count = 0;

        $sql = "SELECT MAX(`sno`) FROM `booking_details`";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> execute();
            $stmt -> bind_result($booking_count);
            $stmt -> fetch();
            $stmt -> close();
            $booking_count = $booking_count+1;

        }

        return "booking_".$booking_count;*/

        $str=rand(); 
        $booking_id = sha1($str); 
        return "booking_".$booking_id;

    }


    function get_booking_data_for_date($date,$restaurant_id,$user_hash,$response,$connect_ref){

        $timestamp             = strtotime($date);
       
        $start_of_booking_date = strtotime(date('Y-m-d 00:00:00', $timestamp));
        $end_of_booking_date   = strtotime(date('Y-m-d 23:59:59', $timestamp));
        $status                = "active";
        $temp                  = 0;
        $total_guest_count     = 0;
        $booking_data          = [];
        $booked_table          = [];
        $booked_time           = [];


      //  echo $start_of_booking_date."<br>".$end_of_booking_date."<br>".$status."<br>".$user_hash."<br>".$restaurant_id."<br>".$status."<br>".$status."<br>";


        // $sql = "SELECT book_de.`restaurant_id`, book_de.`booking_id`, book_de.`booking_time`, book_de.`status`, book_de.`guest_name`, 
        // book_de.`guest_count`,book_de.`booking_date`,book_tab.`table_id`,res_tab.`table_name`,book_tab.`start_time_slot`,
        // book_tab.`end_time_slot` FROM `booking_details` as book_de,`booked_table` as book_tab,`restaurant_tables` as res_tab WHERE 
        // (book_de.`booking_date` BETWEEN ? AND ?) AND book_de.`booking_status`=? AND book_de.`user_hash`=? AND book_de.`restaurant_id`=? 
        // AND book_tab.`restaurant_id` = book_de.`restaurant_id`  AND book_tab.`booking_id` = book_de.`booking_id` AND 
        // res_tab.`restaurant_id`= book_de.`restaurant_id` AND res_tab.`table_id` = book_tab.`table_id` AND book_tab.`status`=? AND 
        // res_tab.`status`=?";

        $sql = "SELECT `restaurant_id`, `booking_id`, `booking_time`, `booking_number`, `status`, `guest_name`, `guest_count`, `booking_date` FROM `booking_details` WHERE `booking_date` BETWEEN ? AND ? AND `booking_status`=?  AND `restaurant_id`=?";
        

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ssss',$start_of_booking_date,$end_of_booking_date,$status,$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$booking_id,$booking_time,$booking_number,$status,$guest_name,$guest_count,$booking_date);
            while($stmt -> fetch()){
                $temp++; 
                $total_guest_count += $guest_count;
                $booking_data[$temp]["restaurant_id"]    = $restaurant_id;
                $booking_data[$temp]["booking_id"]       = $booking_id;
                $booking_data[$temp]["booking_time"]     = $booking_time;
                $booking_data[$temp]["booking_number"]     = $booking_number;
                $booking_data[$temp]["status"]           = $status;
                $booking_data[$temp]["guest_name"]       = $guest_name;
                $booking_data[$temp]["guest_count"]      = $guest_count;
                $booking_data[$temp]["booking_date"]     = date('F d Y', $booking_date);

            }
            $stmt -> close();

            $response["booking_count"] = $temp;
            $response["guest_count"]   = $total_guest_count;
        }

        for($index = 1; $index <= sizeof($booking_data);$index++){


            $restaurant_id = $booking_data[$index]["restaurant_id"];
            $booking_id    = $booking_data[$index]["booking_id"];
            $status        = "active";
            $temp_table    = 0;
            $temp_time     = 0;

           // echo $restaurant_id."<br>".$booking_id."<br>".$status."<br>".$status."<br>";

            $sql = "SELECT book_tab.`table_id`, book_tab.`start_time_slot`, book_tab.`end_time_slot`,res_tab.`table_name` FROM `booked_table` as book_tab,`restaurant_tables` as res_tab WHERE book_tab.`restaurant_id`=? AND book_tab.`booking_id`=? AND book_tab.`status`=? AND res_tab.`restaurant_id`= book_tab.`restaurant_id` AND res_tab.`table_id` = book_tab.`table_id` AND res_tab.`status`=?";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ssss',$restaurant_id,$booking_id,$status,$status);
                $stmt -> execute();
                $stmt -> bind_result($table_id,$start_time_slot,$end_time_slot,$table_name);
                while($stmt -> fetch()){
                    $temp_table++;
                    $booked_table[$booking_id][$temp_table]["table_id"]         = $table_id;
                    $booked_table[$booking_id][$temp_table]["table_name"]       = $table_name;
                    $booked_table[$booking_id][$temp_table]["start_time_slot"]  = $start_time_slot;
                    $booked_table[$booking_id][$temp_table]["end_time_slot"]    = $end_time_slot;
                    
                }   
                 
                $stmt -> close();
            }



            $sql = "SELECT `booking_time` FROM `booking_time` WHERE `booking_id`=? AND `restaurant_id`=? AND `status`=?";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('sss',$booking_id,$restaurant_id,$status);
                $stmt -> execute();
                $stmt -> bind_result($booking_time);
                while($stmt -> fetch()){
                    $temp_time++;
                    $booked_time[$booking_id][$temp_time]["booking_time"] = $booking_time;
                }   
                 
                $stmt -> close();
            }




        }


        $response["booking_data"]  = $booking_data;
        $response["booked_table"]  = $booked_table;
        $response["booked_time"]   = $booked_time;

        return $response;

    }


    function get_table_data_for_date($restaurant_id,$response,$connect_ref){

        $table_details  = [];
        $timing_details = [];


        $status = "active";

        $temp = 0;

        $sql = "SELECT  `table_name`, `table_id`,`guest_count` FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_name,$table_id,$guest_count);
            while($stmt -> fetch()){
                $table_details[$temp]["table_name"]  = $table_name;
                $table_details[$temp]["table_id"]    = $table_id;
                $table_details[$temp]["guest_count"] = $guest_count;
                $temp++; 
                
            }
            $stmt -> close();
        }



        $timing_details = $this -> get_booking_time($restaurant_id,$connect_ref);

        $response["table_details"]  = $table_details;
        $response["timing_details"] = $timing_details;


        return $response;




    }

    function get_restaurant_data($user_hash,$response,$connect_ref){

        $status = "active";
        $temp   = 0;
        //$restaurant_group_id = $this -> get_restaurant_group_id($user_hash,$connect_ref);

        $restaurant_group_id = "";
        $user_role = "";
        $sql = "SELECT `restaurant_group`,`user_role` FROM `user_details` WHERE `user_hash`=? and `status`='active'";

        if($stmt = $connect_ref -> prepare($sql)){

            $stmt -> bind_param('s',$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group_id,$user_role);
            $stmt -> fetch();
            $stmt -> close();
        }else{
            return "error";
        }

        if( ($user_role !== "admin") && ($user_role !== "owner")){

            $sql = "SELECT `restaurant_id` FROM `user_restaurant_mapping` WHERE `user_hash`=? and `status`='active'";

            if($stmt = $connect_ref -> prepare($sql)){

                $stmt -> bind_param('s',$user_hash);
                $stmt -> execute();
                $stmt -> bind_result($restaurant_id);
                $stmt -> fetch();
                $stmt -> close();
                
            }else{
                return "error";
            }

            $sql = "SELECT `restaurant_id`, `restaurant_name`,`notification_email`,`minimum_group_size`,`maximum_group_size`,`restaurant_start_time`,`restaurant_end_time`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";
            $dynamic_condition_data = $restaurant_id;
           
        }else if(($user_role === "admin") || ($user_role === "owner")){

            $sql = "SELECT `restaurant_id`, `restaurant_name`,`notification_email`,`minimum_group_size`,`maximum_group_size`,`restaurant_start_time`,`restaurant_end_time`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_group_id`=? AND `status`=?";
            $dynamic_condition_data = $restaurant_group_id;
           
        }


        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$dynamic_condition_data,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name,$notification_email,$minimum_group_size,$maximum_group_size,$restaurant_start_time,$restaurant_end_time,$booking_interval);
            while($stmt -> fetch()){
                $temp++; 
                $response[$temp]["restaurant_id"]      = $restaurant_id;
                $response[$temp]["restaurant_name"]    = $restaurant_name;
                $response[$temp]["notification_email"] = $notification_email;

                $response[$temp]["minimum_group_size"]    = $minimum_group_size;
                $response[$temp]["maximum_group_size"]    = $maximum_group_size;
                $response[$temp]["restaurant_start_time"] = $restaurant_start_time;
                $response[$temp]["restaurant_end_time"]   = $restaurant_end_time;
                $response[$temp]["booking_interval"]      = $booking_interval;

            }
            $stmt -> close();
        }

        return $response;

    }

    function get_table_count($response,$connect_ref){

        $status = "active";


        for($index=1; $index <= sizeof($response); $index++){

            $restaurant_id = $response[$index]["restaurant_id"];
            $table_count   = 0;

            $sql = "SELECT COUNT(`table_id`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";

            if($stmt = $connect_ref -> prepare($sql)){
                $stmt -> bind_param('ss',$restaurant_id,$status);
                $stmt -> execute();
                $stmt -> bind_result($table_count);
                $stmt -> fetch();
                    $response[$index]["table_count"]   = $table_count;
                $stmt -> close();
            }
    
        }

        return $response;

    }


    function delete_booking($booking_id,$user_hash,$response,$connect_ref){

        $status = "inactive";
       
        $sql = "UPDATE `booking_details` SET `booking_status`=? WHERE `booking_id`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$status,$booking_id);
            $stmt -> execute();
            
            if(($stmt -> affected_rows) != 0 && ($stmt -> affected_rows) != "") $response = "true";
            else  $response = "false";

            $stmt -> close();

        }else{
            $response = "false";
        }

        $sql = "UPDATE `booked_table` SET `status`=? WHERE `booking_id`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$status,$booking_id);
            $stmt -> execute();
            
            $response = "true";

            $stmt -> close();

        }else{
            $response = "false";
        }

        



        return $response;
    }

    function get_restaurant_group_id($user_hash,$connect_ref){

        $restaurant_group = "";
        $status = "active";

        $sql = "SELECT `restaurant_group` FROM `user_details` WHERE `user_hash`=? and `status`=?";
       
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$user_hash,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_group);
            $stmt -> fetch();
            $stmt -> close();

        } 

        return $restaurant_group;

    }

   function get_booking_data_for_id($booking_id,$user_hash,$response,$connect_ref){

        $status = "active";
        $restaurant_data = [];
       
        $sql = "SELECT book_de.`restaurant_id`, book_de.`booking_date`, book_de.`booking_time`, book_de.`status`, book_de.`assigned_tables`, book_de.`guest_name`, book_de.`guest_count`, book_de.`email_address`, book_de.`phone_number`, book_de.`comments`, book_de.`email_confirmation_status`, book_de.`sms_confirmation_status`, book_de.`sms_reminder`, book_de.`restaurant_notification_status`,res_de.`restaurant_name`,book_de.`added_at`,book_de.`alergy_type`,book_de.`customer_need`,book_de.`language` FROM `booking_details` as book_de,`restaurant_details` as res_de WHERE book_de.`booking_id`=? AND book_de.`booking_status`=? AND res_de.`restaurant_id`= book_de.`restaurant_id` ";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$booking_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$booking_date,$booking_time,$status,$assigned_tables,$guest_name,$guest_count,$email_address,$phone_number,$comments,$email_confirmation_status,$sms_confirmation_status,$sms_reminder,$restaurant_notification_status,$restaurant_name,$added_at,$alergy_type,$customer_need,$language);
            while($stmt -> fetch()){
                $response["restaurant_id"]                  = $restaurant_id;
                $response["booking_date"]                   = date('m/d/yy', $booking_date);
                $response["booking_time"]                   = $booking_time;
                $response["status"]                         = $status;
                $response["assigned_tables"]                = $assigned_tables;
                $response["guest_name"]                     = $guest_name;
                $response["guest_count"]                    = $guest_count;
                $response["email_address"]                  = $email_address;
                $response["phone_number"]                   = $phone_number;
                $response["comments"]                       = $comments;
                $response["email_confirmation_status"]      = $email_confirmation_status;
                $response["sms_confirmation_status"]        = $sms_confirmation_status;
                $response["email_confirmation_status"]      = $email_confirmation_status;
                $response["sms_reminder"]                   = $sms_reminder;
                $response["restaurant_notification_status"] = $restaurant_notification_status;
                $response["restaurant_name"]                = $restaurant_name;
                $response["alergy_type"]                    = $alergy_type;
                $response["customer_need"]                  = $customer_need;
                $response["language"]                       = $language;
                $response["added_at"]                       = date('j F Y H:i', $added_at);

            }
            $stmt -> close();
        }

        return $response;

   }


   function get_booking_month_date($month_year,$restaurant_id,$user_hash,$response,$connect_ref){

    $start_timestamp = $this -> first_date_of_the_month($month_year);
    $end_timestamp   = $this -> end_date_of_the_month($month_year);
    $temp            = 0;
    $status          = "active";
    $booking_month   = [];
  

  //  $sql  = "SELECT  DISTINCT `booking_date` FROM `booking_details` WHERE (`booking_date` BETWEEN ? AND ?) AND `booking_status`=? AND `restaurant_id`=?";
    $sql  = "SELECT  DISTINCT `booking_date` FROM `booking_details` WHERE `booking_status`=? AND `restaurant_id`=?";
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$status,$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($booking_date);
            while($stmt -> fetch()){
                $temp++; 
                $booking_month[$temp]["name"] = "ad";
                $booking_month[$temp]["date"] = date("yy-m-d",$booking_date);

            }
            $stmt -> close();
        }

        $response["booking_month"] = $booking_month;

        return $response;

   }

    function first_date_of_the_month($month_year){

        return strtotime(date("Y-m-d", strtotime($month_year)) . ", first day of this month");

    }

    function end_date_of_the_month($month_year){

        return strtotime(date("Y-m-d 23:59:59", strtotime($month_year)) . ", last day of this month");
    
    }

  
    function get_restaurant_name($restaurant_id,$connect_ref){

        $status = "active";
        $restaurant_details = array();
        $restaurant_name   = "";

        $sql = "SELECT  `restaurant_name`,`email`,`phone_number`,`address` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_name,$email,$phone_number,$address);
            $stmt -> fetch();
            $stmt -> close();
        }

        $restaurant_details["restaurant_name"] = $restaurant_name;
        $restaurant_details["restaurant_email"] = $email;
        $restaurant_details["restaurant_phone_number"] = $phone_number;
        $restaurant_details["restaurant_address"] = $address;

        return $restaurant_details;

    }


    function get_widget_restaurant_data($restaurant_group_id,$restaurant_selected_id,$response,$connect_ref){

        $status = "active";
        $temp   = 0;
        $restaurant_details = [];
        $booked_time  = [];
        $booking_time = 0;
        $guest_count = 0;
        $booking_widget_status = "true";



        $sql = "SELECT `restaurant_id`,`restaurant_name`,`notification_email`,`default_phone_code` FROM `restaurant_details` WHERE `restaurant_group_id`=? AND `status`=? AND `booking_widget_status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sss',$restaurant_group_id,$status,$booking_widget_status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_id,$restaurant_name,$notification_email,$default_phone_code);
            while($stmt -> fetch()){
                $temp++; 
                $restaurant_details[$temp]["restaurant_id"]      = $restaurant_id;
                $restaurant_details[$temp]["restaurant_name"]    = $restaurant_name;
                $restaurant_details[$temp]["notification_email"] = $notification_email;
                $restaurant_details[$temp]["default_phone_code"] = $default_phone_code;

            }
            $stmt -> close();
        }

       
        $response["restaurant_details"] = $restaurant_details;

        if($restaurant_selected_id !== "undefined" && $restaurant_selected_id !== "null" && $restaurant_selected_id !== null){

            $booking_time = $this -> get_booking_time($restaurant_selected_id,$connect_ref);
            $guest_count  = $this -> get_guest_count($restaurant_selected_id,$connect_ref);

            if($guest_count["maximum_group_size"] >= 2){

                $date = date('d-m-Y');

                $booked_time = $this -> booking_time_and_booked_time_for_guest(2,$restaurant_id,$date,$connect_ref);

            }

        }else{

            if(sizeof($response) != 0){

                $booking_time = $this -> get_booking_time($restaurant_details[1]["restaurant_id"],$connect_ref);
                $guest_count  = $this -> get_guest_count($restaurant_details[1]["restaurant_id"],$connect_ref);
                
                if($guest_count["maximum_group_size"] >= 2){

                    $date = date('d-m-Y');

                    $booked_time = $this -> booking_time_and_booked_time_for_guest(2,$restaurant_id,$date,$connect_ref);

                }
            }
        }

        $response["booking_time"] = $booking_time;
        $response["guest_count"]  = $guest_count;
        $response["booked_time"]  = $booked_time;

        return $response;

    }


    function get_booking_interval($restaurant_id,$connect_ref){

        $status = "active";
        $timing_details = [];


        $sql = "SELECT `restaurant_start_time`,`restaurant_end_time`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_start_time,$restaurant_end_time,$booking_interval);
            $stmt -> fetch();

                $timing_details["start_time"] = $restaurant_start_time;
                $timing_details["end_time"]   = $restaurant_end_time;
                $timing_details["interval"]   = $booking_interval;

                
            $stmt -> close();
        }

        return $timing_details;

    }



    function get_booking_time($restaurant_id,$connect_ref){

        $temp = 0;
        $restaurant_start_time = 0;
        $timing_details = [];
        $status = "active";


        $sql = "SELECT `restaurant_start_time`,`restaurant_end_time`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($restaurant_start_time,$restaurant_end_time,$booking_interval);
            $stmt -> fetch();

                $start_time_check = number_format((str_replace(":",".",$restaurant_start_time)),2);
                $end_time_check   = number_format((str_replace(":",".",$restaurant_end_time)),2);

                $interval_string = " +".$booking_interval." minutes";

                $looping_time = $start_time_check;

                $restaurant_time = $restaurant_start_time;

                while($looping_time <= $end_time_check && $looping_time >= $start_time_check){

                    if($looping_time !== "0.00"){
                    
                        $timing_details[$temp]["time"] = $restaurant_time;

                        $time = date('H:i',strtotime($looping_time . $interval_string));

                        $restaurant_time = $time;

                        $looping_time = number_format((str_replace(":",".",$time)),2);

                        $temp++; 

                    }else{

                        break;
                    }
            
                }

        

            $stmt -> close();
        }

       

        return $timing_details;

    }



    function get_guest_count($restaurant_id,$connect_ref){

        $status = "active";
        $minimum_group_size = 0;
        $maximum_group_size = 0;
        $guest_count = [];


        $sql = "SELECT `minimum_group_size`,`maximum_group_size`  FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($minimum_group_size,$maximum_group_size);
            $stmt -> fetch();
            $stmt -> close();
        }

        $guest_count["minimum_group_size"] = $minimum_group_size;
        $guest_count["maximum_group_size"] = $maximum_group_size;

        return $guest_count;

    }

    function get_booking_duration_and_interval($restaurant_id,$connect_ref){

        $booking_duration = 0;
        $booking_interval = 0;

        $sql = "SELECT `booking_duration`,`booking_interval` FROM `restaurant_details` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($booking_duration,$booking_interval);
            $stmt -> fetch();
            $stmt -> close();
        }

        $booking_duration_array["booking_duration"] = $booking_duration;
        $booking_duration_array["booking_interval"] = $booking_interval;

        return $booking_duration_array;

    }


    function booking_time_and_booked_time_for_guest($guest_count,$restaurant_id,$date,$connect_ref){


        $start_of_date = strtotime(date("$date 00:00:00"));
        $end_of_date   = strtotime(date("$date 23:59:59"));

        $booking_time_result    = [];
        $booking_hide_time      = [];
        $restaurant_seat_status = "available";
        
        $restaurant_seat_count = $this -> get_restaurant_seat_count($restaurant_id,$connect_ref);


        if($restaurant_seat_count >= $guest_count){

            $booking_time_list       = $this -> get_booked_time_list($restaurant_id,$start_of_date,$end_of_date,$connect_ref);
            $booking_start_time_list = $this -> get_booked_start_time_list($restaurant_id,$start_of_date,$end_of_date,$connect_ref);

            $booking_interval        = $this -> get_booking_interval($restaurant_id,$connect_ref);
            $interval                = $booking_interval["interval"];
            $duration                = $this -> get_restaurant_booking_duration($restaurant_id,$connect_ref);

            $booked_temp = 0;
    
            foreach ($booking_time_list as $key => $value) {
    
                $booked_time = $value;
    
                $booked_seat_count = $this -> get_booked_seat_count_for_this_time($booked_time,$start_of_date,$end_of_date,$restaurant_id,$connect_ref);
          
                if($restaurant_seat_count < ($booked_seat_count+$guest_count)){
    
                    $booking_hide_time[$booked_temp]["time"] = $booked_time;
                    $booked_temp++;
    
                }
    
            }


            foreach ($booking_start_time_list as $key => $value) {
    
                $booked_time = $value;

                $booked_seat_count = $this -> get_booked_seat_count_for_this_time($booked_time,$start_of_date,$end_of_date,$restaurant_id,$connect_ref);
            
                if($restaurant_seat_count <= ($booked_seat_count+$guest_count)){


                    $start_time       = date('H:i',strtotime($booked_time . (" -".$interval." minutes")));
                    $start_time_check = number_format((str_replace(":",".",$start_time)),2);
                    $end_time         = date('H:i',strtotime($booked_time . (" -".$duration." minutes")));
                    $end_time_check   = number_format((str_replace(":",".",$end_time)),2);

                    $interval_string = " -".$interval." minutes";

                    $looping_time = $start_time_check;

                    $restaurant_time = $start_time;

                    while($looping_time > $end_time_check && $looping_time <= $start_time_check){

                        if($looping_time !== "0.00"){

                            $booked_looping_seat_count = $this -> get_booked_seat_count_for_this_time($restaurant_time,$start_of_date,$end_of_date,$restaurant_id,$connect_ref);

                            if($restaurant_seat_count < ($booked_looping_seat_count+$booked_seat_count+$guest_count)){

                                $booking_hide_time[$booked_temp]["time"] = $restaurant_time;
                                $booked_temp++;

                            }

                            $time = date('H:i',strtotime($looping_time . $interval_string));

                            $restaurant_time = $time;

                            $looping_time = number_format((str_replace(":",".",$time)),2);


                        }else{

                            break;
                        }
                
                    }
                }
    
            }

        }else{

            $restaurant_seat_status = "not_available";

        }
        
        $booking_time_result["booked_time"] = $booking_hide_time;
        $booking_time_result["restaurant_seat_status"] = $restaurant_seat_status;


        return $booking_time_result;

    }

    function get_booked_time_list($restaurant_id,$start_of_date,$end_of_date,$connect_ref){

        $status = "active";
        $booking_status = "no-show";
        $booking_status1 = "cancelled";
        $booking_time_list = [];
        $flag = 0;

        $sql = "SELECT DISTINCT `booking_time` FROM `booking_time` WHERE `booking_id` IN (SELECT `booking_id` FROM `booking_details` WHERE `restaurant_id`=? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=? AND (`status` != ? AND `status` != ?)) AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sssssss',$restaurant_id,$start_of_date,$end_of_date,$status,$booking_status,$booking_status1,$status);
            $stmt -> execute();
            $stmt -> bind_result($booking_time);
            while($stmt -> fetch()){

                $booking_time_list[$flag] = $booking_time;
                
                $flag++;
            }
            $stmt -> close();
        }

        return $booking_time_list;
    }

    function get_booked_start_time_list($restaurant_id,$start_of_date,$end_of_date,$connect_ref){

        $status = "active";
        $booking_status = "no-show";
        $booking_status1 = "cancelled";
        $booking_start_time_list = [];
        $flag = 0;

        $sql = "SELECT `booking_time` FROM `booking_details` WHERE `restaurant_id`=? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=? AND (`status` != ? AND `status` != ?)";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ssssss',$restaurant_id,$start_of_date,$end_of_date,$status,$booking_status,$booking_status1);
            $stmt -> execute();
            $stmt -> bind_result($booking_time);
            while($stmt -> fetch()){

                $booking_start_time_list[$flag] = $booking_time;
                
                $flag++;
            }
            $stmt -> close();
        }

        return $booking_start_time_list;

    }


    function get_restaurant_seat_count($restaurant_id,$connect_ref){

        $status = "active";
        $total_seat_count = 0;

        $sql = "SELECT SUM(`guest_count`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ss',$restaurant_id,$status);
            $stmt -> execute();
            $stmt -> bind_result($total_seat_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $total_seat_count;

    }


    function get_booked_seat_count_for_this_time($booked_time,$start_of_date,$end_of_date,$restaurant_id,$connect_ref){

        $status = "active";
        $booked_seat_count = 0;

        $sql = "SELECT SUM(`guest_count`) FROM `booking_details` WHERE `booking_id` IN (SELECT `booking_id` FROM `booking_time` WHERE `booking_date` BETWEEN ? AND ? AND `restaurant_id`=? AND `booking_time`=? AND `status`=?)";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sssss',$start_of_date,$end_of_date,$restaurant_id,$booked_time,$status);
            $stmt -> execute();
            $stmt -> bind_result($booked_seat_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $booked_seat_count;

    }


    function booking_time_with_guest_count($guest_count,$restaurant_id,$date,$connect_ref){

        // $start_of_date = strtotime(date('Y-m-d 00:00:00'));
        // $end_of_date   = strtotime(date('Y-m-d 23:59:59'));

        $start_of_date = strtotime(date("$date 00:00:00"));
        $end_of_date   = strtotime(date("$date 23:59:59"));



        $booking_time_list   = [];
        $booking_time_result = [];
        $status = "active";
        $booking_status = "no-show";
        $booking_status1 = "cancelled";
        $flag = 0;

        $guest_count = (int)$guest_count;
      
        $table_count_for_check = $this -> find_table_count_for_guest($guest_count,$restaurant_id,$connect_ref);


        // $table_total_count = $this -> get_restaurant_table_total_count($restaurant_id,$connect_ref);
        // $booked_count_for_time = $this -> get_total_booked_count_for_time();


        if($table_count_for_check === 0){

            $restaurant_guest_count = $this -> get_guest_count_for_restaurant($restaurant_id,$connect_ref);


            if($restaurant_guest_count[count($restaurant_guest_count)-1] !== $guest_count && $restaurant_guest_count[count($restaurant_guest_count)-1] >= $guest_count){

                $guest_count = $this -> find_next_guest_count($guest_count,$restaurant_guest_count);
                    
            }

        }

      // echo "<br>".$restaurant_id."<br>".$start_of_date."<br>".$end_of_date."<br>".$status."<br>".$guest_count."<br>".$booking_status."<br>".$booking_status1."<br>";

       $sql = "SELECT `booking_time` FROM `booking_details` WHERE `restaurant_id` =? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=? AND `table_guest_count`=? AND (`status` != ? AND `status` != ?)";
       //$sql = "SELECT `booking_time` FROM `booking_details` WHERE `restaurant_id` =? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=? AND `guest_count` = ?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sssssss',$restaurant_id,$start_of_date,$end_of_date,$status,$guest_count,$booking_status,$booking_status1);
            $stmt -> execute();
            $stmt -> bind_result($booking_time);
            while($stmt -> fetch()){

                $booking_time_list[$flag] = $booking_time;
                
                $flag++;
            }
            $stmt -> close();
        }

        $booking_time_result["table_guest_count"] = $guest_count;
        
    
        $table_count = $this -> get_restaurant_table_count($guest_count,$restaurant_id,$connect_ref);

        $booking_time_result["table_count"] = $table_count;


        if(sizeof($booking_time_list) != 0){

            $booking_preference = $this -> get_booking_duration_and_interval($restaurant_id,$connect_ref);

            $booking_time_result["booking_time_for_table"] = $this -> get_booking_time_result($booking_time_list,$table_count,$guest_count,$restaurant_id,$start_of_date,$end_of_date,$booking_preference,$connect_ref);


        }

        $booking_time_result["booking_time"]  = $this -> get_booking_time($restaurant_id,$connect_ref);


        return $booking_time_result;

    }   

    function get_booking_time_result($booking_time_list,$table_count,$guest_count,$restaurant_id,$start_of_date,$end_of_date,$booking_preference,$connect_ref){

        $booking_result = [];

        $booking_data = [];
        $booking_available_time = [];
        $temp = 0;
        $temp_time = 0;


        $booking_time_count = array_count_values($booking_time_list);

        foreach ($booking_time_count as $key => $value) {

            $time_value = $key;
            $time_count = $value;

            if($table_count < $time_count){

                $booking_data["time"] = $time_value;
                $temp++;
            
            }else if($table_count == $time_count){


                $guest_booking_status_array = array();

                $guest_booking_status_array =  $this -> get_guest_count_for_booking_availability($guest_count,$restaurant_id,$start_of_date,$end_of_date,$time_value,$connect_ref);

                if($guest_booking_status_array["status"] === "booking not available"){

                    $booking_data[$temp]["time"] = $time_value;
                    $temp++;

                }else if($guest_booking_status_array["status"] === "booking available"){

                    $booking_available_time[$temp_time]["time"] = $time_value;
                    $booking_available_time[$temp_time]["guest_count"] = $guest_booking_status_array["guest_count"];
                    $temp_time++;

                }

            }else{

                $booking_available_time[$temp_time]["time"] = $time_value;
                $booking_available_time[$temp_time]["guest_count"] = $guest_count;
                $temp_time++;

            }
        }

        $booking_result["booking_hide_time"]      = $booking_data;
        $booking_result["booking_available_time"] = $booking_available_time;

        return $booking_result;

    }

    function get_restaurant_table_total_count($restaurant_id,$connect_ref){

        $table_count = 0;

        $sql = "SELECT COUNT(*) FROM `restaurant_details` WHERE `restaurant_id`=? AND `status` = 'active'";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('s',$restaurant_id);
            $stmt -> execute();
            $stmt -> bind_result($table_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $table_count;

    }



    function get_guest_count_for_booking_availability($guest_count,$restaurant_id,$start_of_date,$end_of_date,$time_value,$connect_ref){

        $table_count            = $this -> get_restaurant_table_count($guest_count,$restaurant_id,$connect_ref);

        $booked_table_count     = $this -> get_booked_table_count($guest_count,$restaurant_id,$start_of_date,$end_of_date,$time_value,$connect_ref);
      
        $restaurant_guest_count = $this -> get_guest_count_for_restaurant($restaurant_id,$connect_ref);

    
        if($table_count === $booked_table_count){

            if($restaurant_guest_count[count($restaurant_guest_count)-1] === $guest_count && $restaurant_guest_count[count($restaurant_guest_count)-1] <= $guest_count){

                $guest_booking_result = array();

                $guest_booking_result["status"]      = "booking not available";
                $guest_booking_result["guest_count"] = $guest_count;

                return $guest_booking_result;
                    
            }else{

                $guest_count = $this -> find_next_guest_count($guest_count,$restaurant_guest_count);

                return $this -> get_guest_count_for_booking_availability($guest_count,$restaurant_id,$start_of_date,$end_of_date,$time_value,$connect_ref);
    
            }
       
        }else{
   
            $guest_booking_result = array();

            $guest_booking_result["status"]      = "booking available";
            $guest_booking_result["guest_count"] = $guest_count;

           return $guest_booking_result;
        }

    }


    function get_booked_table_count($guest_count,$restaurant_id,$start_of_date,$end_of_date,$time_value,$connect_ref){

        $booked_count    = 0;
        $status          = "active";
        $booking_status  = "no-show";
        $booking_status1 = "cancelled";

        $sql = "SELECT COUNT(*) FROM `booking_details` WHERE `restaurant_id` =? AND `booking_date` BETWEEN ? AND ? AND `booking_status`=? AND `table_guest_count`=? AND (`status` != ? AND `status` != ?) AND `booking_time`=?";
        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('ssssssss',$restaurant_id,$start_of_date,$end_of_date,$status,$guest_count,$booking_status,$booking_status1,$time_value);
            $stmt -> execute();
            $stmt -> bind_result($booked_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $booked_count;


    }


    function get_restaurant_table_count($guest_count,$restaurant_id,$connect_ref){

        $table_count = 0;
        $status = "active";

        //echo $restaurant_id."<br>".$guest_count."<br>".$status."<br>";

        $sql = "SELECT COUNT(`table_id`) FROM `restaurant_tables` WHERE `restaurant_id`=? AND  `guest_count` = ? AND `status`=?";

        if($stmt = $connect_ref -> prepare($sql)){
            $stmt -> bind_param('sss',$restaurant_id,$guest_count,$status);
            $stmt -> execute();
            $stmt -> bind_result($table_count);
            $stmt -> fetch();
            $stmt -> close();
        }

        return $table_count;
    }



}

