-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 07, 2020 at 09:31 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fully_booked`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_details`
--

CREATE TABLE `booking_details` (
  `sno` int(10) NOT NULL,
  `restaurant_id` varchar(50) NOT NULL,
  `user_hash` varchar(200) NOT NULL,
  `booking_date` int(12) NOT NULL,
  `status` varchar(10) NOT NULL,
  `assigned_tables` varchar(10) NOT NULL,
  `guest_name` varchar(50) NOT NULL,
  `guest_count` int(5) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `phone_number` int(20) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `email_confirmation_status` varchar(10) NOT NULL,
  `sms_confirmation_status` varchar(10) NOT NULL,
  `sms_reminder` varchar(10) NOT NULL,
  `restaurant_notification_status` varchar(10) NOT NULL,
  `added_at` int(12) NOT NULL,
  `booking_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_preferences`
--

CREATE TABLE `booking_preferences` (
  `sno` int(10) NOT NULL,
  `restaurant_id` varchar(50) NOT NULL,
  `same_day_booking` varchar(12) NOT NULL,
  `booking_period` int(5) NOT NULL,
  `booking_interval` int(5) NOT NULL,
  `booking_duration` int(5) NOT NULL,
  `minimum_guest_size` int(5) NOT NULL,
  `maximum_guest_size` int(5) NOT NULL,
  `phone_number_prefix` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `added_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_notification`
--

CREATE TABLE `email_notification` (
  `sno` int(10) NOT NULL,
  `restaurant_id` varchar(50) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `email_status` varchar(15) NOT NULL,
  `email_response` varchar(300) NOT NULL,
  `added_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password_activation_link`
--

CREATE TABLE `forgot_password_activation_link` (
  `sno` int(10) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `user_hash` varchar(200) NOT NULL,
  `activation_id` varchar(50) NOT NULL,
  `timestamp` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forgot_password_activation_link`
--

INSERT INTO `forgot_password_activation_link` (`sno`, `email_address`, `user_hash`, `activation_id`, `timestamp`) VALUES
(1, 'nandhiniwebmail@gmail.com', '88e2e4f02955d957ceb152243601c7dd2ebe0dc1f0ed0af4adc9883e3fc7f70a4588a04d1f553100e957812b0f27d95f28c592ca8fed5f545d05352c05dae82b', '5f38e21e0d706cdc2f5376d493724011', 1583200789),
(2, 'nandhiniwebmail@gmail.com', '88e2e4f02955d957ceb152243601c7dd2ebe0dc1f0ed0af4adc9883e3fc7f70a4588a04d1f553100e957812b0f27d95f28c592ca8fed5f545d05352c05dae82b', 'f913a62fcb6d4459ac5d9c59114f86bb', 1583200844);

-- --------------------------------------------------------

--
-- Table structure for table `logout_track`
--

CREATE TABLE `logout_track` (
  `sno` int(10) NOT NULL,
  `user_hash` varchar(200) NOT NULL,
  `added_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_details`
--

CREATE TABLE `restaurant_details` (
  `sno` int(11) NOT NULL,
  `restaurant_id` varchar(50) NOT NULL,
  `restaurant_name` varchar(100) NOT NULL,
  `booking_widget_type` varchar(10) NOT NULL,
  `notification_email` varchar(100) NOT NULL,
  `phone_number` int(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `added_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_notification`
--

CREATE TABLE `sms_notification` (
  `sno` int(10) NOT NULL,
  `restaurant_id` varchar(50) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `sms_status` varchar(15) NOT NULL,
  `sms_response` varchar(300) NOT NULL,
  `added_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `sno` int(10) NOT NULL,
  `user_hash` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `added_at` int(12) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`sno`, `user_hash`, `email`, `password`, `user_name`, `added_at`, `status`) VALUES
(1, '88e2e4f02955d957ceb152243601c7dd2ebe0dc1f0ed0af4adc9883e3fc7f70a4588a04d1f553100e957812b0f27d95f28c592ca8fed5f545d05352c05dae82b', 'nandhiniwebmail@gmail.com', 'eXdRd24yeUUweE5ObDJiNTI3dTRpUT09', 'test', 1582450836, 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `booking_preferences`
--
ALTER TABLE `booking_preferences`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `email_notification`
--
ALTER TABLE `email_notification`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `forgot_password_activation_link`
--
ALTER TABLE `forgot_password_activation_link`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `logout_track`
--
ALTER TABLE `logout_track`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `restaurant_details`
--
ALTER TABLE `restaurant_details`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `sms_notification`
--
ALTER TABLE `sms_notification`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`sno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_details`
--
ALTER TABLE `booking_details`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_preferences`
--
ALTER TABLE `booking_preferences`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_notification`
--
ALTER TABLE `email_notification`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forgot_password_activation_link`
--
ALTER TABLE `forgot_password_activation_link`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logout_track`
--
ALTER TABLE `logout_track`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restaurant_details`
--
ALTER TABLE `restaurant_details`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_notification`
--
ALTER TABLE `sms_notification`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
