function booking_details(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'booking', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.add_new_booking = function(booking_id,booking_type,restaurant_id,user_hash,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,language,restaurant_notify){

        var booking_data = {booking_id,restaurant_id,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,restaurant_notify,user_hash,language,type:"new_booking"};

        var response_data = this.send_data(booking_data);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "true"){

            this.clear_bookign_form();

            if(booking_type === "new"){

                $("#booking_msg_modal").modal("show");

                $("html, body").animate({ scrollTop: 0 }, "slow");

            }else{


            }

           
        }else{

            alert("Something Wrong, try again");
        }



    }


    this.get_restaurant_link = function(){

        var user_hash = gsession.getSession("user_hash");

        var restaurant_data = {user_hash,type:"get_restaurant"};

        var response_data = this.send_data(restaurant_data);
    
        var parsed_data   = this.process_data(response_data);

        this.add_restaurant_dropdown(parsed_data);

    }

    this.add_restaurant_dropdown = function(restaurant_data){

        var temp = "<option value='' selected>Select Restaurants</option>";
       
        var restaurant_count = Object.keys(restaurant_data).length;

        for(let index=1; index <=restaurant_count; index++){

            var restaurant_id      = restaurant_data[index].restaurant_id;
            var restaurant_name    = restaurant_data[index].restaurant_name;
            var notification_email = restaurant_data[index].notification_email;


            temp += `<option value =${restaurant_id} data-email=${notification_email}>${restaurant_name}</option>`;

        }


        $("#restaurant_list_dropdown").html(temp);

    }


    this.clear_bookign_form = function(){

        //$("#restaurant_list_dropdown").val("");
        $("#booking_date").val("");
        $("#booking_time").val("");
        $("#guest-name").val("");
        $("#guest-count").val("");
        $("#email-address").val("");
        $("#phone-number").val("");
        $("#additional-notes").val("");

        $('#send_email').prop('checked', false); 
        $('#send_sms').prop('checked', false); 
        $('#send_advance_sms').prop('checked', false); 
        $('#restaurant_notify').prop('checked', false); 

    }


    this.find_booking_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");

        return id;

    }

    this.get_booking_data_by_id = function(booking_id){

        var user_hash  = gsession.getSession("user_hash");

        var data = {booking_id,user_hash,type:"get_booking_data_for_id"};

        var response_data = this.send_data(data);
    
        var parsed_data   = this.process_data(response_data);

        this.process_edit_booking_data(parsed_data);


    }


    this.process_edit_booking_data = function(data){


            var selected_restaurant_id         = data.restaurant_id;
            var booking_date                   = data.booking_date;
            var booking_time                   = data.booking_time;
            var status                         = data.status;
            var assigned_tables                = data.assigned_tables;
            var table_name                     = data.table_name;
            var guest_name                     = data.guest_name;
            var guest_count                    = data.guest_count;
            var email_address                  = data.email_address;
            var phone_number                   = data.phone_number;
            var comments                       = data.comments;
            var email_confirmation_status      = data.email_confirmation_status;
            var sms_confirmation_status        = data.sms_confirmation_status;
            var email_confirmation_status      = data.email_confirmation_status;
            var sms_reminder                   = data.sms_reminder;
            var restaurant_notification_status = data.restaurant_notification_status;
         
           $("#restaurant_list_dropdown").val(selected_restaurant_id);


            $(".booking-date").val(booking_date);
            $("#booking_time").val(booking_time);

            $('.booking-date').datepicker({
                setDate: booking_date,
            // setDate:"03/09/2020"
            });

            $('#booking_time').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                defaultTime: booking_time,
            });
    

            $("#status").val(status);
            $("#assigned-table").val(assigned_tables);
            $("#guest-name").val(guest_name);
            $("#guest-count").val(guest_count);
            $("#additional-notes").val(comments);
            $("#email-address").val(email_address);
            $("#phone-number").val(phone_number);

            
            email_confirmation_status == 1? $("#send_email").prop("checked"):"";
            sms_confirmation_status == 1? $("#send_sms").prop("checked"):"";
            sms_reminder == 1? $("#send_advance_sms").prop("checked"):"";
            restaurant_notification_status == 1? $("#restaurant_notify").prop("checked"):"";
        
        

    }
}




$(document).ready(function() {

    var booking_details_obj = new booking_details();
    
    booking_details_obj.get_restaurant_link();





    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true,
        autoclose: true,
        startDate: new Date(), 
        orientation: "bottom" ,
        selectMultiple:true
    });

    hideOldDays();
    hideNewDays();

    $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        defaultTime: '01',
    });

        
    var booking_id = booking_details_obj.find_booking_id();

    if(booking_id !== null){

        booking_details_obj.get_booking_data_by_id(booking_id);
    }


        
    $("#booking_msg_modal").modal("hide");


});



function hideOldDays(){

    var x = $('.datepicker .datepicker-days table tr td.old');
    if(x.length > 0){
        x.css('visibility', 'hidden');
        if(x.length === 7){
            x.parent().hide();
        }
    }

}



function hideNewDays(){

    var x = $('.datepicker .datepicker-days tr td.new');
    if(x.length > 0){
        x.hide();
    }

}



$(document).on('click','.add-new-booking',function(){
    
    var restaurant_id = $("#restaurant_list_dropdown option:selected").val();
    var booking_date = $("#booking_date").val();
    var booking_time = $("#booking_time").val();
    var status = $("#status").val();
    var assigned_table = $("#assigned-table").val();
    var guest_name = $("#guest-name").val();
    var guest_count = $("#guest-count").val();
    var email_address = $("#email-address").val();
    var phone_number = $("#phone-number").val();
    var additional_notes = $("#additional-notes").val();
    var send_email = $("#send_email").is(':checked'); 
    var send_sms = $("#send_sms").is(':checked'); 
    var send_advance_sms = $("#send_advance_sms").is(':checked'); 
    var restaurant_notify = $("#restaurant_notify").is(':checked'); 
    var language = $("#language").val();
    var user_hash = gsession.getSession("user_hash");

    var booking_details_obj = new booking_details();

    // validation area

    var mandatory_field_status = "passed";
    $( ".mandatory_field" ).each(function() {

        var user_input = $( this ).val();
        if(!user_input){
            mandatory_field_status = "failed";
            $(this).addClass("validation_border_error")
        }

    });

    console.log(mandatory_field_status)
    if(mandatory_field_status === "failed"){

        $(".validation_error").html("Please fill mandatory fields");
        $(".booking_completed").html("");

    }else{
        
        $(".validation_error").html("");
        $(".booking_completed").html("New booking added");
        var booking_id = booking_details_obj.find_booking_id();
        var booking_type = "update_booking";

        if(booking_id === null){

            var booking_id  = "yet_to_generate";
            var booking_type = "new_booking";
        }


        booking_details_obj.add_new_booking(booking_id,booking_type,restaurant_id,user_hash,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,language,restaurant_notify);


    }


    
});



$(document).on('change','#restaurant_list_dropdown',function(){
    
    var restaurant_email = $("#restaurant_list_dropdown option:selected").attr("data-email");

    console.log(restaurant_email);


    $(".notification_email").html(restaurant_email);

});



$(document).on('change','.mandatory_field',function(){
    
    $(this).removeClass("validation_border_error");
    $(".validation_error").html("");

});

$(document).on('change','.booking-input',function(){
    
    $(".booking_completed").html("");

});
