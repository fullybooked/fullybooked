
function dynamic_menu_control(){

    this.clear_session = function(){

        gsession.clearSession("display_name");
        gsession.clearSession("login_status");
        gsession.clearSession("user_hash");
        gsession.clearSession("restaurant_count");
        gsession.clearSession("user_role");
        gsession.clearSession("restaurant_group_id");
        document.location.href = "index.html"; 

    }



    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'dashboard', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.load_summary_data = function(){

        var user_hash = gsession.getSession("user_hash");
        var user_details = {user_hash, "type":"load_summary"}

        var response_data = this.send_data(user_details);
    
        var parsed_data   = this.process_data(response_data);

        this.generate_restaurant_summary(parsed_data);


    }

    this.generate_restaurant_summary = function(summary_data){

        var available_restaurant_count = summary_data["restaurant_count"];
        var restaurant_group_id = summary_data["restaurant_group_id"];
        
        if(available_restaurant_count>=1){

                    gsession.setSession("restaurant_count", available_restaurant_count);

                    for(let restaurant_flag=1; restaurant_flag <= available_restaurant_count; restaurant_flag++){
                        gsession.setSession("restaurant_"+restaurant_flag, summary_data["restaurant_"+restaurant_flag]);
                        gsession.setSession("restaurant_id_"+restaurant_flag, summary_data["restaurant_id_"+restaurant_flag]);
                    }
        }

        gsession.setSession("user_role", summary_data["user_role"]);
        gsession.setSession("restaurant_group_id", restaurant_group_id);

    }

    this.enable_booking_manu = function(){

        var restaurant_id = this.get_restaurant_id();

        $(".nav-link-booking").removeClass("collapsed");
        $("#collapseBooking").addClass("show");
        $(".nav-link-booking").attr("aria-expanded","true");    

        $(".booking_"+restaurant_id).addClass("active");

    }

    this.enable_restaurants_manu = function(){

        var restaurant_id = this.get_restaurant_id();

        $(".nav-link_restaurant").removeClass("collapsed");
        $("#collapseRestaurant").addClass("show");
        $(".nav-link_restaurant").attr("aria-expanded","true");    
        if(restaurant_id){
            $(".restaurant_"+restaurant_id).addClass("active");
        }else{
            $(".new_restaurant").addClass("active");
        }

    }
    

    this.get_restaurant_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var restaurant_id = url.searchParams.get("id");

        return restaurant_id;
  
    }

    this.role_handling = function(){

        var role_type = gsession.getSession("user_role");
        //console.log(role_type);

    }
    
}


$(document).ready(function() {

    var dynamic_menu_control_obj = new dynamic_menu_control();

    dynamic_menu_control_obj.load_summary_data();
    
    var restaurant_count = gsession.getSession("restaurant_count");
    var role_type = gsession.getSession("user_role");
    var restaurant_group_id = gsession.getSession("restaurant_group_id");

    /**************** Booking Menu ********************/


        var restaurant_booking_menu_template = `
        <a class="nav-link collapsed nav-link-booking" href="#" data-toggle="collapse" data-target="#collapseBooking" aria-expanded="true" aria-controls="collapseBooking">
            <i class="fas fa-fw fa-book"></i>
            <span>Bookings</span>
            </a>
            <div id="collapseBooking" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">           
        `;

        for (var flag=1;flag<=restaurant_count;flag++){

            restaurant_booking_menu_template += `<a class="collapse-item booking_restaurant booking_${gsession.getSession("restaurant_id_"+flag)}" href="./bookings_report.html?id=${gsession.getSession("restaurant_id_"+flag)}" data-restaurant_id=${gsession.getSession("restaurant_id_"+flag)}>${gsession.getSession("restaurant_"+flag)}</a>`;
        
        }


        restaurant_booking_menu_template += `</div> </div>`;

        $("#restaurant_booking_list").append(restaurant_booking_menu_template);

    /******************************************************/    



    /**************** Widget Menu ************************/

    $('#widget_link').attr('href', "./widget.html?id="+restaurant_group_id);

    /******************************************************/    


    /**************** Restaurants Menu ********************/
    if(role_type==="admin" || role_type==="owner" || role_type ==="manager"){
        var restaurant_menu_template = `
        <a class="nav-link collapsed nav-link_restaurant" href="#" data-toggle="collapse" data-target="#collapseRestaurant" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-hotel"></i>
            <span>Restaurants</span>
            </a>
            <div id="collapseRestaurant" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">           
        `;

        for (var flag=1;flag<=restaurant_count;flag++){

            restaurant_menu_template += `<a class="collapse-item restaurant_list restaurant_${gsession.getSession("restaurant_id_"+flag)}" href="./restaurant.html?id=${gsession.getSession("restaurant_id_"+flag)}" data-restaurant_id=${gsession.getSession("restaurant_id_"+flag)}>${gsession.getSession("restaurant_"+flag)}</a>`;
        
        }

        
        if(role_type==="admin" || role_type==="owner" ){
            restaurant_menu_template += `<h6 class="collapse-header">New</h6>
           <a class="collapse-item restaurant_list new_restaurant" href="./add_restaurant.html"> Add restaurant </a>`;
        }
        


       restaurant_menu_template += ` </div>
        </div>`;

        $("#restaurant_list").append(restaurant_menu_template);
    }
    /******************************************************/   
    
    
    /**************** Role based logic Menu ********************/
    
    if(role_type==="admin" || role_type==="owner" ){
        var dynamic_my_users_menu = `<a class="nav-link" href="./my_users.html">
        <i class="fas fa-fw fa-users"></i>
        <span>All Users</span></a>`;

        $("#my_users_menu").html(dynamic_my_users_menu);
        
    }else{
        
        $("#widget_link").remove();
        $("#setup_list").remove();
        
    }
    

    //dynamic_menu_control_obj.role_handling();


    /******************************************************/   

  
    var display_name = gsession.getSession("display_name");
    $(".display_name").text(display_name);


    if(window.location.href.indexOf("bookings_report") > -1) {
        
        dynamic_menu_control_obj.enable_booking_manu();

    }


    if(window.location.href.indexOf("/restaurant.html") > -1 || window.location.href.indexOf("/add_restaurant.html") > -1) {
        
        dynamic_menu_control_obj.enable_restaurants_manu();

    }


});



$(document).on('click','#logout_btn',function(){
    
    var dynamic_menu_control_obj = new dynamic_menu_control();

    dynamic_menu_control_obj.clear_session();

});
