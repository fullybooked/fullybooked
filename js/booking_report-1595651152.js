function booking_report() {
  this.send_data = function (user_data) {
    var response_data = "";

    ajaxcall.send_data(user_data, "booking", function (data) {
      response_data = data;
    });

    return response_data;
  };

  this.process_data = function (data) {
    var data = JSON.parse(data);
    return data;
  };

  this.get_booking_data = function (date) {
    var user_hash = gsession.getSession("user_hash");
    var restaurant_id = this.get_restaurant_id();

    var data = { date, restaurant_id, user_hash, type: "get_booking_data" };

    var response_data = this.send_data(data);

    var parsed_data = this.process_data(response_data);

    this.display_booking_data(parsed_data);
  };

  this.get_booking_month_date = function (month_year) {
    var user_hash = gsession.getSession("user_hash");
    var restaurant_id = this.get_restaurant_id();

    var data = {
      month_year,
      restaurant_id,
      user_hash,
      type: "get_booking_month_date",
    };

    var response_data = this.send_data(data);

    var parsed_data = this.process_data(response_data);

    this.display_month_booking_date(parsed_data.booking_month);
    this.display_table_details(parsed_data);
  };

  this.display_month_booking_date = function (data) {
    $(".loader").hide();
    $(".table_assignment_loader").hide();

    var selected_date_array = [];

    for (var index = 1; index <= Object.keys(data).length; index++) {
      selected_date_array.push(data[index]);
    }

    var booking_report_obj = new booking_report();

    console.log(selected_date_array);

    $(".inline-datepicker").html(" ");

    $(".inline-datepicker").pignoseCalendar({
      weeks: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      scheduleOptions: {
        colors: {
          offer: "#2fabb7",
          ad: "#5c6270",
        },
      },
      schedules: selected_date_array,
      select: function (date, context) {
        var selected_date = date[0].format("MM/DD/YYYY");
        booking_report_obj.display_booking_header(new Date(date[0]));
        booking_report_obj.get_booking_data(selected_date);
      },
      prev: function (info, context) {
        booking_report_obj.update_booking_details_header(info, context);
      },
      next: function (info, context) {
        booking_report_obj.update_booking_details_header(info, context);
      },
    });
  };

  this.update_booking_details_header = function (info, context) {
    var year = info.year; // current year (number type), ex: 2017
    var month_number = info.month; // current month (number type), ex: 6

    var date = `${year}-${month_number}-01`;

    $(".inline-datepicker").pignoseCalendar("set", date);

    this.display_booking_header(new Date(date));
    this.get_booking_data(date);

    // var month_year = $(".pignose-calendar-top-month").text()+" "+$(".pignose-calendar-top-year").text();
    // this.get_booking_month_date(month_year);
  };

  this.display_table_details = function (data) {
    var table_details = data.table_details;
    var timing_details = data.timing_details;

    // Timing Details Col Display

    if (Object.keys(timing_details).length != 0) {
      var timing_temp = ` <th scope="col"></th>`;

      $.each(timing_details, function (index, value) {
        timing_temp += `<th scope="col">${value.time}</th>`;
      });

      $(".table-assignment-header").html(timing_temp);
    } else {
      timing_temp = `<th>No tables found</th>`;

      $(".table-assignment-header").html(timing_temp);
    }

    // Table Details Col Display

    if (Object.keys(table_details).length != 0) {
      var total_guest_count = 0;
      var total_tables = Object.keys(table_details).length;

      var table_temp = "";

      $.each(table_details, function (index, table_value) {
        total_guest_count += Number(table_value.guest_count);
        table_temp += `<tr>`;
        table_temp += `<td>${table_value.table_name}<br><small>${table_value.guest_count} seats</small></td>`;

        $.each(timing_details, function (index, timing_value) {
          var booking_time_temp = timing_value.time.replace(":", "_");

          table_temp += `<td class="${booking_time_temp} ${table_value.table_id} ${booking_time_temp}_${table_value.table_id} table_time_td"></td>`;
        });

        table_temp += `</tr>`;
      });

      $(".table-assignment-body").html(table_temp);

      $(".table-configuration").html(
        `Table Configuration: ${total_tables} tables and ${total_guest_count} seats`
      );
    }
  };

  this.display_booking_data = function (data) {
    var booking_array = data.booking_data;
    var booked_table = data.booked_table;
    var booked_time  = data.booked_time;
    var booking_total_count = data.booking_count;
    var guest_total_count = data.guest_count;
   
    this.display_booking_summary_data(booking_total_count, guest_total_count);

    $(".table_time_td").removeClass("is_booking");
    $(".table_time_td span").remove();

    // var booking_count = Object.keys(booking_array).length;
    var booking_count = data["booking_count"];

    // console.log("booking_count ==>");
    // console.log(booking_count);

    if (booking_total_count > 0) {
      var booking_data = "";

      for (var temp = 1; temp <= booking_total_count; temp++) {
        var restaurant_id = booking_array[temp].restaurant_id;
        var booking_id = booking_array[temp].booking_id;
        var booking_time = booking_array[temp].booking_time;
        var booking_number = booking_array[temp].booking_number;
        var status = booking_array[temp].status;
        var table_name = booking_array[temp].table_name;
        var guest_name = booking_array[temp].guest_name;
        var guest_count = booking_array[temp].guest_count;
        var booking_date = booking_array[temp].booking_date;
        var table_id = booking_array[temp].table_id;
        var start_time_slot = booking_array[temp].start_time_slot;

        var booked_time_length   = 0;
        var booked_table_length  = 0;

        if(booked_time[booking_id]){  
          booked_time_length  = Object.keys(booked_time[booking_id]).length;
        }

        if(booked_table[booking_id]){  
          booked_table_length = Object.keys(booked_table[booking_id]).length;
        }

    



       
        if (booking_date != "") {
          booking_date = booking_date.replace(/ /g, "_");
        }

        booking_data += `<tr id=booking_row_${booking_id}>
                                    <td>${booking_number}</td>
                                    <td>${booking_time}</td>
                                    <td><i class="fas fa-users" data-toggle="modal" data-target="#myModal"></i> ${guest_count}</td>`;

        booking_data +=` <td>`;

        if ( booked_table_length === 0) {
          booking_data += `walk-in`;
        } else {
          for (var temp_table = 1; temp_table <= booked_table_length; temp_table++) {

            var table_name = (booked_table[booking_id][temp_table].table_name).replace("_", " ");
            var table_id = booked_table[booking_id][temp_table].table_id;
            
            booking_data += (temp_table === 1)?`${table_name}`:`,${table_name}`;


            for (var temp_time = 1; temp_time <= booked_time_length; temp_time++) {

              var booking_time = booked_time[booking_id][temp_time].booking_time;

              var booking_time_temp = booking_time.replace(":", "_");


              $("." + booking_time_temp + "_" + table_id).html(
                `<span class="is_booking_details" data-booking_id=${booking_id}>${guest_name}<br>${guest_count} seats</span>`
              );
              $("." + booking_time_temp + "_" + table_id).addClass("is_booking");

            }

           

          }
        }
        booking_data +=` </td>`;

        
        booking_data += `<td>${guest_name}</td><td>${status}</td>`;

        if (table_name === "walk-in") {
          booking_data += ` <td>-</td>`;
        } else {
          booking_data += ` <td><a href="./new_booking.html?id=${booking_id}" class="fas fa-edit booking_edit_button" data-booking_date=${booking_date} data-booking_time=${booking_time} data-guest_count=${guest_count} data-guest_name=${guest_name} data-booking_id=${booking_id}></a></td>`;
        }

        booking_data += `<td>
                                    <i class="fas fa-trash booking_delete_button" data-toggle="modal" data-booking_date=${booking_date} data-booking_time=${booking_time} data-guest_count=${guest_count} data-guest_name=${guest_name} data-booking_id=${booking_id}></i>
                                    </td>
                                </tr>`;

      
     
      }

      console.log(booking_data);

      $(".booking_data_body").html(booking_data);

   

    } else {
      $(".booking_data_body").html(
        `<tr><td colspan="7" class="text-center">No Booking</td></tr>`
      );
    }
  };

  this.display_booking_header = function (selected_date) {
    var current_month = selected_date.toLocaleString("default", {
      month: "long",
    });

    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var current_day = weekday[selected_date.getDay()];

    var booking_header_date =
      current_day +
      " " +
      selected_date.getDate() +
      " " +
      current_month +
      " " +
      selected_date.getFullYear();

    booking_header_date += `<span class="booking_details"></span>`;

    $(".booking_header_text").html(booking_header_date);
  };

  this.display_booking_summary_data = function (
    booking_total_count,
    guest_total_count
  ) {
    var booking_summary = "";

    if (booking_total_count != 0) {
      booking_summary = booking_total_count + "booking ";
    }

    if (guest_total_count != 0) {
      if (guest_total_count == 1)
        booking_summary += "&" + guest_total_count + "guest";
      else booking_summary += "& " + guest_total_count + "guests";
    }

    if (booking_summary != "") {
      booking_summary = " (" + booking_summary + ")";
    }

    $(".booking_details").html(booking_summary);
  };

  this.delete_booking = function (booking_id, user_hash) {
    var delete_data = { booking_id, user_hash, type: "delete_booking" };

    var response_data = this.send_data(delete_data);

    var parsed_data = this.process_data(response_data);

    if (parsed_data == "true") {
      $("#booking_row_" + booking_id).remove();

      /*if($('#booking_data_body tr').length == 0){

                $(".booking_data_body").html(`<tr><td colspan="7" class="text-center">No Booking</td></tr>`);
            }*/

      $(".modal-title").removeClass("delete-modal-header");
    } else {
      alert("Network Error, try again later");
    }
  };

  this.get_restaurant_id = function () {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");

    return id;
  };

  this.get_booking_date = function () {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var booking_date = url.searchParams.get("date");

    return booking_date;
  };
}

$(document).ready(function () {
  var booking_report_obj = new booking_report();

  $(".loader").show();
  $(".table_assignment_loader").show();

  $.fn.pignoseCalendar("setting", {
    // date format follows moment sheet.
    // Check below link.
    // https://momentjs.com/docs/#/parsing/string-format/
    format: "YYYY-MM-DD",
    // Starting day of week. (0 is Sunday[default], 6 is Saturday
    // and all day of week is in consecutive order.
    // In this example, We will start from Saturday.
    week: 6,
    language: "en-short",
    languages: {
      // You will set `custom` language.
      "en-short": {
        // Weeks sun ~ sat.
        weeks: ["S", "M", "T", "W", "H", "F", "A"],
        // Month long names January ~ December
        monthsLong: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "Jun",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ],
        // Month short names Jan ~ Dec
        months: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ],
      },
    },
  });

  // default Today Date

  var booking_date = booking_report_obj.get_booking_date();

  // IF Booking Date Available in URL
  if (!booking_date) {
    var booking_date = new Date().toDateString("MM/DD/YYYY");
  } else {
    booking_date = moment(booking_date).format("ddd MMM DD YYYY");
  }

  $(".inline-datepicker").pignoseCalendar({
    weeks: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    //minDate:moment(booking_date),
    date: moment(booking_date),
    select: function (date, context) {
      var selected_date = date[0].format("MM/DD/YYYY");
      booking_report_obj.display_booking_header(new Date(date[0]));
      booking_report_obj.get_booking_data(selected_date);
    },
    prev: function (info, context) {
      booking_report_obj.update_booking_details_header(info, context);
    },
    next: function (info, context) {
      booking_report_obj.update_booking_details_header(info, context);
    },
  });

  booking_report_obj.display_booking_header(new Date(booking_date));

  var month_year =
    $(".pignose-calendar-top-month").text() +
    " " +
    $(".pignose-calendar-top-year").text();
  booking_report_obj.get_booking_month_date(month_year);

  booking_report_obj.get_booking_data(booking_date);
});

// $(document).on("click",".booking_edit_button",function () {

//     var title = "Update booking Details";

//     $("#booking_edit_modal .modal-title").html(title);
//     $("#booking_edit_modal").modal("show");

//     var booking_id = $(this).attr("data-booking_id");
//     var user_hash  = gsession.getSession("user_hash");

//     var booking_report_obj = new booking_report();

//     booking_report_obj.get_booking_data_for_id(booking_id,user_hash);

//     $(".action_button").removeClass("booking-delete");
//     $(".action_button").removeClass("danger");

//     $(".action_button").addClass("edit_booking");
//     $(".action_button").addClass("success");
//     $(".action_button").text("Save");

//     $(".action_button").attr("data-booking_id",booking_id);

// });

$(document).on("click", ".booking_delete_button", function () {
  var booking_date = $(this).attr("data-booking_date");
  var booking_time = $(this).attr("data-booking_time");
  var guest_count = $(this).attr("data-guest_count");
  var guest_name = $(this).attr("data-guest_name");
  var booking_id = $(this).attr("data-booking_id");

  if (booking_date != "") {
    booking_date = booking_date.replace(/_/g, " ");
  }

  var title = "Do you want to delete this booking?";

  $("#booking_edit_modal .modal-title").html(title);
  $("#booking_edit_modal").modal("show");

  $(".action_button").addClass("booking-delete");
  $(".action_button").addClass("danger");

  $(".action_button").removeClass("edit_booking");
  $(".action_button").removeClass("success");

  $(".action_button").text("Delete");

  $(".booking-delete").attr("data-booking_id", booking_id);

  $(".modal-title").addClass("delete-modal-header");

  var modal_body = `<p style="font-size: 0.9rem;">We will not notify your guests if you delete this booking.</p>
                      <label style="font-size: 0.9rem;">Guest: <strong>${guest_name}, ${guest_count} guests</strong></label><br>
                      <label style="font-size: 0.9rem;">When: <strong>${booking_date} ${booking_time}</strong></label>`;

  $(".modal-body").html(modal_body);
});

$(document).on("click", ".booking_view_button", function () {
  var title = "View the booking";

  $("#booking_edit_modal .modal-title").html(title);
  $("#booking_edit_modal").modal("show");
});

$(document).on("click", ".booking-delete", function () {
  var booking_id = $(this).attr("data-booking_id");
  var user_hash = gsession.getSession("user_hash");

  var booking_report_obj = new booking_report();

  booking_report_obj.delete_booking(booking_id, user_hash);
});

$(document).on("click", ".edit_booking", function () {
  var booking_id = $(this).attr("data-booking_id");

  var restaurant_id = $(".edit_restaurant_list option:selected").val();
  var booking_date = $(".edit-booking-date").val();
  var booking_time = $("#edit_booking_time").val();
  var status = $(".edit_status").val();
  var assigned_table = $(".edit_assigned-table").val();
  var guest_name = $("#edit-guest-name").val();
  var guest_count = $("#edit-guest-count").val();
  var email_address = $("#edit-email-address").val();
  var phone_number = $("#edit-phone-number").val();
  var additional_notes = $("#edit-additional-notes").val();
  var send_email = $("#edit-send_email").is(":checked");
  var send_sms = $("#edit-send_sms").is(":checked");
  var send_advance_sms = $("#edit-send_advance_sms").is(":checked");
  var restaurant_notify = $("#edit-restaurant_notify").is(":checked");
  var user_hash = gsession.getSession("user_hash");

  var booking_details_obj = new booking_details();

  var booking_status = booking_details_obj.add_new_booking(
    booking_id,
    restaurant_id,
    user_hash,
    booking_date,
    booking_time,
    status,
    assigned_table,
    guest_name,
    guest_count,
    email_address,
    phone_number,
    additional_notes,
    send_email,
    send_sms,
    send_advance_sms,
    restaurant_notify
  );

  if (booking_status == "true") {
    var booking_report_obj = new booking_report();
    booking_report_obj.get_booking_data(booking_date);
  }
});

$(document).on("click", ".is_booking_details", function () {
  var booking_id = $(this).attr("data-booking_id");

  window.open(`./booking_details.html?id=${booking_id}`);
});
