function search_booking(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'search_booking', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }

    this.get_onload_data  = function(){

        var user_hash       = gsession.getSession("user_hash");
        var restaurant_group_id = gsession.getSession("restaurant_group_id");
        var month_type      = "upcoming_only";
        var restaurant_type = "all_restaurant";

        var data = {user_hash,restaurant_group_id,month_type,restaurant_type,"type":"onload"};

        var response_data = this.send_data(data);
    
        var parsed_data   = this.process_data(response_data);

        this.display_month(parsed_data.month_booking_count);
        this.display_restaurant(parsed_data.restaurant_details);
        this.display_booking_list(parsed_data.booking_list,restaurant_type);  

    }


    this.display_month = function(data){

        var temp = "";
        var active_count = 0; 
        

        $.each(data, function( index, value ){
            
            var month = value["month"];

            var count = value["count"];

            var active_class   =  "";
            var upcoming_class =  "";
            var month_filter =  month;

            if(month == "Upcoming only"){

                upcoming_class = "upcoming"
                active_class = "filter_active";
                active_count = count;
                month_filter = "upcoming_only"
            
            }

            temp += `<div class="mb-3 filter_month" id="${upcoming_class}" data-filter_type="${month_filter}" data-booking_count="${count}">
                        <a href="#" class="${active_class}">
                        <i class="far fa-calendar-alt mr-2"></i>  ${month} (${count})
                        </a>
                    </div>`;

        });

        $(".month_name").html(temp);
        $(".booking_count").html(active_count+" Upcoming Bookings");

    }

    this.display_restaurant = function(restaurant_details){

        var restaurant_count = Object.keys(restaurant_details).length;

        if(restaurant_count > 1){

            var temp = `<h3 class="booking-filter-header mb-4"><strong>Restaurants</strong></h3>`;

            temp += `<div class="mb-3 filter_restaurant" id="all_restaurant" data-filter_type="all_restaurant">
                            <a href="#" class="filter_active_restaurant"><i class="fas fa-map-marker-alt mr-2"></i>  All Restaurant
                            </a>
                        </div>`;

            $.each(restaurant_details, function( index, value ){  

            var restaurant_id   = value["restaurant_id"];
            var restaurant_name = value["restaurant_name"];

            temp += `<div class="mb-3 filter_restaurant" id="${restaurant_id}" data-filter_type="${restaurant_id}">
                        <a href="#" class=""><i class="fas fa-map-marker-alt mr-2"></i> ${restaurant_name}</a>
                    </div>`;

            });

            $(".restaurant_list_details").html(temp);
        
        }else if(restaurant_count == 1){

            var restaurant_id   = restaurant_details[0]["restaurant_id"];
            var restaurant_name = restaurant_details[0]["restaurant_name"];

            $(".restaurant_list_details").attr("data-restaurant_id",restaurant_id);

            
        }

        $(".restaurant_list_details").attr("data-restaurant_count",restaurant_count);
   
       
    }



    this.display_booking_list = function(data,restaurant_type){

        console.log(restaurant_type);


        var booking_temp  = "";

        var download_temp = "";


        if(Object.keys(data).length !== 0){

                var restaurant_header_name = "";
                var restaurant_header_id   = "";
                var restaurant_temp        = "";
           
                $.each(data, function( index, value ){

                    var date = index;

                    var date_temp    = "";
                    var temp         = "";
                    var booking_date = "";
                    
                   
                    $.each(value, function( index, value ){

                        var time = index;
                       
                        var time_rowspan = Object.keys(value).length;

                        temp += `<tr>
                                    <td rowspan="${time_rowspan}" style="width:12% !important;">${time}</th>`;


                        $.each(value, function( index, value ){

                            var booking_id      = value["booking_id"];
                            var booking_number  = value["booking_number"];
                            var guest_count     = value["guest_count"];
                            var guest_name      = value["guest_name"];
                            var status          = value["status"];
                            var email_address   = value["email_address"];
                            var phone_number    = value["phone_number"];
                            var comments        = value["comments"];
                            var restaurant_name = value["restaurant_name"];
                            var restaurant_id   = value["restaurant_id"];
                            var added_at        = value["added_at"];
                            var language        = "";
                            var link            = `http://localhost/booking_details.html?id=${booking_id}`;
                            booking_date        = value["booking_date"];

                            var email_tag       = email_address != '' ? `<span>Email:</span><span style="color: #000000c7;"> ${email_address}</span></br>` : "";
                            var phone_number_tag = phone_number != '' ? `<span>Phone:</span><span style="color: #000000c7;"> ${phone_number}</span></br>` : "";
                            var comments_tag     = comments != '' ? `<span>Comments:</span><span style="color: #000000c7;"> ${comments}</span></br>` : "";


                            temp += `<td style="width:10% !important;"><i class="fas fa-user-friends"></i> ${guest_count}</td>
                                        <td style="width:12% !important;"><small>${booking_number}</small></td>
                                        <td><a href="booking_details.html?id=${booking_id}">${guest_name}</a>
                                            <div class="mt-2 mb-2 user_details">
                                                <small class="user_details_tag">
                                                    ${email_tag}
                                                    ${phone_number_tag}
                                                    ${comments_tag}
                                                </small>
                                            </div>

                                        </td>
                                        <td class="text-right" style="width:5% !important;"><h4><span class="badge badge-secondary">${status}</span></h4></td>
                                    </tr>`;


                            download_temp += `<tr><td>#1001</td><td>${restaurant_name}</td><td>${date} ${time}</td><td>${status}</td><td>${guest_name}</td>`;
                            download_temp += `<td>${guest_count}</td><td>${email_address}</td>><td>${phone_number}</td><td>${language}</td>`;
                            download_temp += `<td>${comments}</td><td>${added_at}</td><td>${link}</td></tr>`;

                            if(restaurant_type === "all_restaurant"){

                                if(restaurant_name != restaurant_header_name){

                                    var restaurant_count = $(".restaurant_list_details").attr("data-restaurant_count");
    
                                    if(restaurant_count >1){
    
                                        restaurant_temp  = `<tr>
                                                                <th colspan="5" class="pt-4" style="color: #282a38cf !important;font-size: 0.98rem;"><medium>${restaurant_name}</medium>&nbsp;&nbsp;&nbsp;&nbsp;<a href="bookings_report.html?id="${restaurant_id}"><small>see all</small></a></th>
                                                            </tr>`;
    
                                    }
    
                                
                                    restaurant_header_name = restaurant_name;   
                                    restaurant_header_id   = restaurant_id;         
                                
                                }else{

                                    restaurant_temp = "";

                                }
                            }

                          


                        });
                    });

                  
                    date_temp = `<tr>
                                    <th colspan="5" class="pt-4" style="color: #282a38cf !important;font-size: 0.98rem;"><medium>${date}</medium>&nbsp;&nbsp;&nbsp;&nbsp;<a href=bookings_report.html?id=${restaurant_header_id}&date=${booking_date}><small>see all</small></a></th>
                                </tr>`;

                
                    booking_temp += restaurant_temp + date_temp + temp;


                });
        

                booking_temp += `<tr class='notfound'>
                                    <td colspan='4'>No record found</td>
                                </tr>`;

        }else{

            booking_temp = "No bookings match your search criteria.";
        }


        $(".search_result_body").html(booking_temp);

        $(".notfound").hide();
       
        $(".search_booking_download_body").html(download_temp);


        this.user_detailed_result();

     //   this.add_scroll_top();

    }

    // this.add_scroll_top = function(){

    // if ($(".search-booking").height() > $(window).height() ) {

    //     $(".search-booking").append();

    // }



    // }

    this.user_detailed_result = function(){

        var detailed_result = $("#detailed_result").val();

        if(detailed_result === "True"){
    
            $(".user_details").show();
        
        }else{

            $(".user_details").hide();
        }   

    }


    this.get_booking_list_by_filter = function(restaurant_type,month_type,period_filter){

        var user_hash   = gsession.getSession("user_hash");
        var restaurant_group_id = gsession.getSession("restaurant_group_id");

        var data = {user_hash,restaurant_group_id,restaurant_type,month_type,period_filter,"type":"filter_month"};

        var response_data = this.send_data(data);
    
        var parsed_data   = this.process_data(response_data);

        this.display_booking_list(parsed_data.booking_list,restaurant_type);

        if(period_filter === "get_month_list"){

            this.display_month(parsed_data.month_booking_count);
        }



    }

    this.search_booking_result = function(search_text){

        var detailed_result = $("#detailed_result").val();

        if(detailed_result === "True"){

            $(".user_details").show();
        
        }else if(detailed_result === "False"){

            $(".user_details").hide();
        }   

        $(".notfound").hide();

        // Hide all table tbody rows
        $('table tbody tr').hide();

        // Count total search result
        var search_length = $('table tbody tr:not(.notfound) td:contains("'+search_text+'")').length;

        if(search_length > 0){

            // Searching text in columns and show match row
            $('table tbody tr:not(.notfound) td:contains("'+search_text+'")').each(function(){
                $(this).closest('tr').show();
            });

        }else{

            $('.notfound').show();
        }

    }
}



$(document).ready(function() {

    var search_booking_obj = new search_booking();

    search_booking_obj.get_onload_data();

});


$(document).on("click",".filter_month",function () {
    
    $(".filter_month").children().removeClass("filter_active");
    $(this).children().addClass("filter_active");

    var month_type   = $(this).attr("data-filter_type");
    var booking_count = $(this).attr("data-booking_count");

    $(".booking_count").html(booking_count);

    if(month_type === "upcoming_only"){

        $(".booking_count").html(booking_count+" upcoming bookings");

    }else if(month_type === "All Time"){

        $(".booking_count").html(booking_count+" bookings");

    }else{

        $(".booking_count").html(booking_count+" bookings in "+month_type);
    }

    var restaurant_type = "all_restaurant";

    if($(".restaurant_list_details").attr("data-restaurant_count") > 1){

        var restaurant_type   = $(".filter_active_restaurant").parent().attr("data-filter_type");

    }

  

    var search_booking_obj = new search_booking();

    var period_filter = "do_not_get_month_list";

    search_booking_obj.get_booking_list_by_filter(restaurant_type,month_type,period_filter);


});




$(document).on("click",".filter_restaurant",function () {
    
    $(".filter_restaurant").children().removeClass("filter_active_restaurant");
    $(this).children().addClass("filter_active_restaurant");

    var restaurant_type   = $(this).attr("data-filter_type");
    var month_type       = $(".filter_active").parent().attr("data-filter_type");
   
    var search_booking_obj = new search_booking();

    var period_filter = "get_month_list";


    search_booking_obj.get_booking_list_by_filter(restaurant_type,month_type,period_filter);


});

$(document).on("click",".search_booking",function () {

    var search_text = $(".search_text").val();

    var search_booking_obj = new search_booking();

    search_booking_obj.search_booking_result(search_text);

});



$(document).on("click",".search_text",function (e) {

    var keyCode = e.which;
    
    if (keyCode == 13) {
        
        var search_text = $(".search_text").val(); 

        var search_booking_obj = new search_booking();

        search_booking_obj.search_booking_result(search_text);
    
    }
});



$(document).on("click",".get_search_booking",function () {

    var search_text = $(".search_booking_data").val(); 

    $(".search_booking_data").val("");

    $(".search_text").val(search_text);

    var search_booking_obj = new search_booking();

    search_booking_obj.search_booking_result(search_text);

});

$(document).on("click",".search_booking_data",function (e) {

    var keyCode = e.which;
    
    if (keyCode == 13) {
        
        var search_text = $(".search_booking_data").val(); 

        $(".search_booking_data").val("");
    
        $(".search_text").val(search_text);
    
        var search_booking_obj = new search_booking();
    
        search_booking_obj.search_booking_result(search_text);
    
    }
});


$(document).on("click",".cancel_search_booking",function (e) {

    $(".search_text").val(""); 
    $('#detailed_result').val("False");

    $(".filter_month").children().removeClass("filter_active");
    $("#upcoming").children().addClass("filter_active");

    var month_type      = $("#upcoming").attr("data-filter_type");
    var booking_count   = $("#upcoming").attr("data-booking_count");  
    var restaurant_type = "all_restaurant";

    $(".booking_count").html(booking_count);

    $(".booking_count").html(booking_count+" upcoming bookings");

    var search_booking_obj = new search_booking();

    var period_filter = "do_not_get_month_list";

    search_booking_obj.get_booking_list_by_filter(restaurant_type,month_type,period_filter);

});



$(document).on("click","#detailed_result",function (e) {

    var detailed_data_result = $(this).val();

     if(detailed_data_result === "True"){

        detailed_data_result = "False";

    }else if(detailed_data_result === "False"){

        detailed_data_result = "True";

    }

    $(this).val(detailed_data_result);


});







$(document).on("click",".download_csv_file",function (e) {

    var titles = [];
    var data = [];
  
    /*
     * Get the table headers, this will be CSV headers
     * The count of headers will be CSV string separator
     */
    $('.search_booking_download_table th').each(function() {
      titles.push($(this).text());
    });
  
    /*
     * Get the actual data, this will contain all the data, in 1 array
     */
    $('.search_booking_download_table td').each(function() {
      data.push($(this).text());
    });
    
    /*
     * Convert our data to CSV string
     */
    var CSVString = prepCSVRow(titles, titles.length, '');
    CSVString = prepCSVRow(data, titles.length, CSVString);
  
    /*
     * Make CSV downloadable
     */
    var downloadLink = document.createElement("a");
    var blob = new Blob(["\ufeff", CSVString]);
    var url = URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = "data.csv";
  
    /*
     * Actually download CSV
     */
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  });
  
     /*
  * Convert data array to CSV string
  * @param arr {Array} - the actual data
  * @param columnCount {Number} - the amount to split the data into columns
  * @param initial {String} - initial string to append to CSV string
  * return {String} - ready CSV string
  */
  function prepCSVRow(arr, columnCount, initial) {
    var row = ''; // this will hold data
    var delimeter = ','; // data slice separator, in excel it's `;`, in usual CSv it's `,`
    var newLine = '\r\n'; // newline separator for CSV row
  
    /*
     * Convert [1,2,3,4] into [[1,2], [3,4]] while count is 2
     * @param _arr {Array} - the actual array to split
     * @param _count {Number} - the amount to split
     * return {Array} - splitted array
     */
    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
    // don't know how to explain this
    // you just have to like follow the code
    // and you understand, it's pretty simple
    // it converts `['a', 'b', 'c']` to `a,b,c` string
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;

  }