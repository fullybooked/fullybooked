function sms_handler(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'sms_settings', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.load_sms_settings_balance_data = function(user_hash){

        var user_details = {user_hash, "type":"load_sms_summary"}

        var response_data = this.send_data(user_details);
    
        var parsed_data   = this.process_data(response_data);

        return parsed_data;

    }


    this.assign_balance_data = function(data){

        var totally_sent_sms = data ["totally_sent_sms"];
        var last_week_sent_sms = data ["last_week_sent_sms"];
        var last_day_sent_sms = data ["last_day_sent_sms"];
        

        $(".total_sms").html(totally_sent_sms);
        $(".this_week_sms").html(last_week_sent_sms);
        $(".today_sent_sms").html(last_day_sent_sms);

        var is_enabled_sms = data ["is_enabled_sms"];
        var sender_name = data ["sender_name"];

        if(is_enabled_sms === "true"){
            $('#sms_enable').prop('checked', true);
        }

        if(sender_name !== "" && sender_name !== null){
            $("#sender_name").val(sender_name);
        }

    }

    this.save_sms_settings = function(sms_sender_name,sms_enable,user_hash){

        var user_details = {sms_sender_name,sms_enable,user_hash, "type":"save_sms_settings"}

        var response_data = this.send_data(user_details);
    
        var parsed_data   = this.process_data(response_data);

        //return parsed_data;

        if(parsed_data === "updated" || parsed_data === "inserted" ){
            alert("SMS Settings Saved!")
        }else{
            alert("Error occured, pls try again.")
        }


    }


}

$(document).ready(function() {

    var sms_obj = new sms_handler();

    var user_hash = gsession.getSession("user_hash");
    var result_data = sms_obj.load_sms_settings_balance_data(user_hash);

    sms_obj.assign_balance_data(result_data);

    var user_role = gsession.getSession("user_role");
    
    if(user_role !== "admin" && user_role !== "owner"){

        $(".sms_settings_area").remove();

    }


});




$(document).on('click','#save_sms_settings',function(){
    
    var sms_sender_name = $("#sender_name").val().trim();
    if(sms_sender_name && sms_sender_name.length<=20){

        var sms_enable          = $("#sms_enable").is(':checked'); 
        sms_enable        = sms_enable == true ? 'true' : 'false';
        var user_hash = gsession.getSession("user_hash");

        var sms_obj = new sms_handler();
        sms_obj.save_sms_settings(sms_sender_name,sms_enable,user_hash);

    }else{

        alert("enter the sender name, maximum characters 10");

    }
    
});



$(document).on('blur','#sender_name',function(evt){

    var sender_name = $("#sender_name").val().trim();
    
    if(sender_name && sender_name.length<=20){
        $(this).removeClass("validation_border_error");
        $(".sender_name_label").removeClass("validation_label_error");
    }else{
        $(this).addClass("validation_border_error");
        $(".sender_name_label").addClass("validation_label_error");
    }
    
});



$(document).on('keypress','#sender_name',function(){
    
    $(this).removeClass("validation_border_error");
    $(".sender_name_label").removeClass("validation_label_error");
    
});