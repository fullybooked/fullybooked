function user_registration(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'user_registration', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }

    this.user_signup = function(user_name,user_email,password,restaurant_name){

        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 

        var signup_data   = {user_name,user_email,password,restaurant_name,"type":"sign_up"};

        var response_data = this.send_data(signup_data);
    
        var parsed_data  = this.process_data(response_data);
        
        if(parsed_data["status"] === "true"){
            var restaurant_count = parsed_data["restaurant_detail"]["restaurant_count"];
            var restaurant_group_id = parsed_data["restaurant_group_id"];

            gsession.setSession("display_name", parsed_data["display_name"]);
            gsession.setSession("login_status", "true");
            gsession.setSession("user_hash", parsed_data["user_hash"]);
            gsession.setSession("user_role", parsed_data["user_role"]);
            gsession.setSession("restaurant_group_id", restaurant_group_id);
            gsession.setSession("restaurant_count", restaurant_count);

            for(let restaurant_flag=1; restaurant_flag <= restaurant_count; restaurant_flag++){
                gsession.setSession("restaurant_"+restaurant_flag, parsed_data["restaurant_detail"]["restaurant-"+restaurant_flag]);
            }

            //this will redirect us in same window
            document.location.href = "dashboard.html";

        }else if(parsed_data["status"] == "exist"){
            
            $.unblockUI();
            $(".signup_error").html("Email ID Alredy Exist");
            $(".signup_error").show();
        
        }else if(parsed_data["status"] == "false"){

            $.unblockUI();
            $(".signup_error").html("Error Occured. Try again later");
            $(".signup_error").show();

        }
        


    }



    this.user_signin = function(user_email,password){

        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 

        var signin_data   = {user_email,password,"type":"sign_in"};

        var response_data = this.send_data(signin_data);
    
        var parsed_data  = this.process_data(response_data);

        if(parsed_data["status"] == "true"){
            var restaurant_count = parsed_data["restaurant_detail"]["restaurant_count"];
            var restaurant_group_id = parsed_data["restaurant_group_id"];

            gsession.setSession("display_name", parsed_data["display_name"]);
            gsession.setSession("login_status", "true");
            gsession.setSession("user_hash", parsed_data["user_hash"]);
            gsession.setSession("user_role", parsed_data["user_role"]);

            gsession.setSession("restaurant_count", restaurant_count);
            gsession.setSession("restaurant_group_id", restaurant_group_id);

            for(let restaurant_flag=1; restaurant_flag <= restaurant_count; restaurant_flag++){
                gsession.setSession("restaurant_"+restaurant_flag, parsed_data["restaurant_detail"]["restaurant-"+restaurant_flag]);
            }

            //this will redirect us in same window
            document.location.href = "dashboard.html";

        }else if(parsed_data["status"] == "false"){

            //alert("Email or Password Wrong");
            $(".login_error").html("Email or Password Wrong, Please check your credentials");
            $(".login_error").show();
            $.unblockUI();

        }else{
            $(".login_error").html("Error Occured. Try again later");
            $(".login_error").show();
            $.unblockUI();

        }

    }

    this.reset_password = function(new_password,user_hash){
    
        var reset_password_data = {new_password,user_hash,"type":"reset_password"};

        var response_data = this.send_data(reset_password_data);
    
        var parsed_data  = this.process_data(response_data);

        if(parsed_data == "true"){

            //this will redirect us in same window
            document.location.href = "index.html";

        }else if(parsed_data == "error"){

            alert("Error Occured. Try again later");
        }


    }


    this.forgot_password_activation_link = function(email_address,user_hash){

        var password_activation_link = {email_address,user_hash,"type":"password_activation_link"};

        var response_data = this.send_data(password_activation_link);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "not_exist"){

            alert("Given email is not associated with any account");

        }else if(parsed_data == "false"){

            alert("Error Occured. Try again later");
        
        }else{

            alert('Please check your e-mail, we have sent a password reset link to your registered email.<br>')
        }


    }



    this.is_valid_email = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }



    this.signin_process = function(){
        
        var user_email = $("#email").val().trim();
        var password   = $("#password").val().trim();
        var validation_result = "pass";

        if(!user_email){
            $("#email").addClass("validation_error");
            validation_result = "failed";
        }

        if(!password){
            $("#password").addClass("validation_error");
            validation_result = "failed";
        }

        if(validation_result === "pass"){
            this.user_signin(user_email,password);
        }

    }

    this.signup_process = function(){
        
        var user_name  = $("#username").val().trim();
        var user_email = $("#email").val().trim();
        var password   = $("#password").val().trim();
        var restaurant_name   = $("#restaurant_name").val().trim();

        var validation_result = "pass";

        if(!user_name){
            $("#username").addClass("validation_error");
            validation_result = "failed";
        }
        if(!user_email){
            $("#email").addClass("validation_error");
            validation_result = "failed";
        }
        if(!password){
            $("#password").addClass("validation_error");
            validation_result = "failed";
        }
        if(!restaurant_name){
            $("#restaurant_name").addClass("validation_error");
            validation_result = "failed";
        }

        

        if(validation_result === "pass"){
            this.user_signup(user_name,user_email,password,restaurant_name);
        }
        

    }


}

$(document).ready(function() {

    var user_registration_obj = new user_registration();

});


$(document).on('click','#user-signup',function(){
                
    var user_registration_obj = new user_registration();

    user_registration_obj.signup_process();

});


$(document).on('click','#user-signin',function(){
    
    var user_registration_obj = new user_registration();

    user_registration_obj.signin_process();

});


$(document).on('click','#reset-password',function(){
              
    var new_password     = $("#new-password").val();
    var confirm_password = $("#confirm-password").val();
    var user_hash        = "";


    if(new_password !== confirm_password){

        alert("password doesn't matched !!!");
    
    }else{

        var user_registration_obj = new user_registration();

        user_registration_obj.reset_password(new_password,user_hash);
    
    }

});


$(document).on('click','#password-activation',function(){
              
    var email_address  = $("#email-address").val();
    var user_hash      = "";


    var user_registration_obj = new user_registration();

    user_registration_obj.forgot_password_activation_link(email_address,user_hash);
   
});






$(document).on('keypress','.signin_enter_key_press_handler',function(event){
              
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		var user_registration_obj = new user_registration();
        user_registration_obj.signin_process();	
	}
   
});

$(document).on('keypress','.signup_enter_key_press_handler',function(event){
              
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		var user_registration_obj = new user_registration();

        user_registration_obj.signup_process();	
	}
   
});


$(document).on('keypress','.user_input',function(event){
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
    
	if(keycode !== 13){ 
		$(this).removeClass("validation_error");
        $(".login_error").html(" ");
        $(".login_error").hide();

        $(".signup_error").html(" ");
        $(".signup_error").hide();
    }
    
});




$(document).on('blur','#email',function(evt){
    var email_address = $("#email").val();
    var user_registration_obj = new user_registration();
    var email_status = user_registration_obj.is_valid_email(email_address);
    
    if(email_status === false){

        $(this).addClass("validation_border_error");
        $(".email_validation_error").show();

    }else{
        $(this).removeClass("validation_border_error");
        $(".email_validation_error").hide();
    }
    

});


$(document).on('keypress','#email',function(event){
    
    $(this).removeClass("validation_border_error");
    $(".email_validation_error").hide();
    
});


$(document).on('keypress','.user_input',function(event){
    
    $(this).removeClass("validation_border_error");
    
});


$(document).on('blur','#restaurant_name',function(evt){
    var restaurant_group_name = $("#restaurant_name").val().trim();
    
    if(restaurant_group_name){

        $(this).removeClass("validation_border_error");
       
    }else{
        $(this).addClass("validation_border_error");
       
    }
    
});


$(document).on('blur','#username',function(evt){
    var username = $("#username").val().trim();
    
    if(username){

        $(this).removeClass("validation_border_error");
       
    }else{
        $(this).addClass("validation_border_error");
       
    }
    
});


$(document).on('blur','#password',function(evt){
    var password = $("#password").val().trim();
    
    if(password){

        $(this).removeClass("validation_border_error");
       
    }else{
        $(this).addClass("validation_border_error");
       
    }
    
});