function restaurant(){

            /** User input validation */
            this.user_input_validation = function(){

                var validation_result = "pass";
                $( ".restaurant_textbox" ).each(function( index ) {
                    if(! $( this ).val()){
                        validation_result = "fail";
                        $(this).addClass("validation_border_error");
                    }
                });

                var booking_notification_email = $("#booking_notification_email").val();
                var validate_booking_notification_email = this.validate_email(booking_notification_email);
                
                if(validate_booking_notification_email === false){
                    validation_result = "fail";
                    $("#booking_notification_email").addClass("validation_border_error");
                }

                var email_address = $("#email_address").val();
                var validate_email_address= this.validate_email(email_address);
                if(validate_email_address === false){
                    validation_result = "fail";
                    $("#email_address").addClass("validation_border_error");
                }

                $( ".mandatory_field" ).each(function( index ) {
                var max_value = parseInt($(this).attr('max'));
                var min_value = parseInt($(this).attr('min'));
                var value = $(this).val().trim();
                if(value>=min_value && value<=max_value){
                    
                    $(this).removeClass("validation_border_error");

                }else{

                    validation_result = "fail";
                    $(this).addClass("validation_border_error");

                }
                });


                return validation_result;

            }

            this.send_data = function(user_data){

                var response_data='';
                
                    ajaxcall.send_data(user_data, 'add_restaurant', function(data) {
                        
                    response_data = data;
                    
                    });
                    
                return response_data;
            }


            this.process_data = function(data) {

                var data        = JSON.parse(data);
                return data;
            }


            this.add_restaurant = function(user_details){

                $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 
                var response_data = this.send_data(user_details);
            
                var parsed_data   = this.process_data(response_data);

                var response = parsed_data["response"];
        
                if( response == "true" ){
                    $.unblockUI();
                    //alert("New Restaurant Added");
                    this.clear_input();

                    var restaurant_id = parsed_data["restaurant_id"];
                    var navigation_url = "./table_allotment.html?id="+restaurant_id;
                    //$('#table_allocation_link').attr('href', navigation_url);
                    $(".restaurant_response").html("<p>Restaurant added successfully!</p> <br><br> <a class='btn btn-success' id='table_allocation_link' href='"+navigation_url+"'>Next Step: Allocate Tables</a>");
                    $("#restaurant_msg_modal").modal("show");
                    // #table_allocation_link
                    
        
                }else if(response === "name_exist"){
                    $.unblockUI();
                    $(".restaurant_response").html("Restaurant name already exists");
                    $("#restaurant_msg_modal").modal("show");

                }else if(response === "updated"){
                    $.unblockUI();
                    $(".restaurant_response").html("Restaurant updated successfully!");
                    $("#restaurant_msg_modal").modal("show");
                    
                }else{
                    $.unblockUI();
                    $(".restaurant_response").html("Something went wrong, please try again.");
                    $("#restaurant_msg_modal").modal("show");
                }
        
        
            }


            // this.get_restaurant_timestamp = function(restaurant_time){

            //     var time = restaurant_time.split(':');

            //     var date = new Date();
            //     date.setHours(time[0], time[1], 0);   // Set hours, minutes and seconds

            //     return (Math.round(date / 1000.0));
            // }


            this.get_user_input = function(type_of_action){

                var restaurant_name = $("#restaurant_name").val();
                var booking_notification_email = $("#booking_notification_email").val();
                var phone_number = $("#phone_number").val();
                var email_address = $("#email_address").val();
                var address_location = $("#address_location").val();
                //var booking_period = $("#booking_period").val();
                var booking_period = 60; // static data
                var booking_interval = $("#booking_interval").val();
                var booking_duration = $("#booking_duration").val();

                var minimum_group_size = $("#minimum_group_size").val();
                var maximum_group_size = $("#maximum_group_size").val();

                var restaurant_start_time = $("#restaurant_start_time").val();
                var restaurant_end_time = $("#restaurant_end_time").val();



                // restaurant_start_time = this.get_restaurant_timestamp(restaurant_start_time);
                // restaurant_end_time   = this.get_restaurant_timestamp(restaurant_end_time);


                if ($('#online_status').is(":checked"))
                {
                    var online_status="true";
                }else{
                    var online_status="false";
                }

                /*if ($('#same_day_booking').is(":checked"))
                {
                    var same_day_booking="true";
                }else{
                    var same_day_booking="false";
                }*/
                var same_day_booking="false";
                var hidden_booking_duration = "0";

                var country_code = $('#country_code').val();
                var owner_hash = gsession.getSession("user_hash");

                
                var type = "restaurant_data";

                if(type_of_action=="edit"){
                    var restaurant_id = this.get_restaurant_id();
                }else{
                    var restaurant_id = "no";
                    type_of_action = "add";
                }


                var user_details = {type_of_action,restaurant_id, restaurant_name, booking_notification_email, phone_number, email_address, address_location, booking_period,hidden_booking_duration, booking_interval, booking_duration, minimum_group_size, maximum_group_size, online_status, same_day_booking, country_code, owner_hash,restaurant_start_time,restaurant_end_time, type};

                return user_details;

            }


            this.clear_input = function(){

                $(".restaurant_textbox").val("");
                $(".restaurant_checkbox").prop("checked", false);
                $('.restaurant_dropdown').prop('selectedIndex',0);

            }

            this.get_restaurant_id = function(){
                var url_string = window.location.href;
                var url = new URL(url_string);
                var restaurant_id = url.searchParams.get("id");
                return restaurant_id;
            }

            this.delete_restaurant = function(){

                var user_hash = gsession.getSession("user_hash");
                var restaurant_id = this.get_restaurant_id();
                if(user_hash && restaurant_id){

                    var restaurant_delete_details = {restaurant_id,user_hash, "type":"restaurant_delete_details"};
                    var response_data = this.send_data(restaurant_delete_details);
                    var parsed_data   = this.process_data(response_data);

                    if(parsed_data === "deleted"){
                        alert("Restaurant Deleted");
                        window.location = "./dashboard.html";
                    }else {
                        alert("Error Occured, Pls Try Again.");
                    }

                }

            }


            this.load_restaurant_details = function(restaurant_id,user_hash){

                var restaurant_details = {restaurant_id,user_hash, "type":"restaurant_onload"};

                var response_data = this.send_data(restaurant_details);
            
                var parsed_data   = this.process_data(response_data);

                var validate_response = parsed_data["response"];

                if(validate_response === "valid"){

                    var restaurant_id= parsed_data["restaurant_id"];
                    var restaurant_name= parsed_data["restaurant_name"];
                    var booking_widget_status = parsed_data["booking_widget_status"];
                    var notification_email = parsed_data["notification_email"];
                    var phone_number = parsed_data["phone_number"];
                    var email = parsed_data["email"];
                    var address = parsed_data["address"];
                    var status = parsed_data["status"];
                    //var added_at = parsed_data["added_at"];
                    //var same_day_booking = parsed_data["same_day_booking"];
                    //var duration = parsed_data["duration"];
                    //var booking_period = parsed_data["booking_period"];
                    var booking_interval = parsed_data["booking_interval"];
                    var booking_duration = parsed_data["booking_duration"];
                    var minimum_group_size = parsed_data["minimum_group_size"];
                    var maximum_group_size = parsed_data["maximum_group_size"];
                    var default_phone_code = parsed_data["default_phone_code"];
                    var restaurant_start_time = parsed_data["restaurant_start_time"];
                    var restaurant_end_time = parsed_data["restaurant_end_time"];


                    $("#restaurant_name").val(restaurant_name);
                    $("#booking_notification_email").val(notification_email);
                    if(booking_widget_status==="true"){
                        $("#online_status").prop("checked", true);
                    }else{
                        $("#online_status").prop("checked", false);
                    }
                    $("#phone_number").val(phone_number);
                    $("#email_address").val(email);
                    $("#address_location").val(address);
                    /*if(same_day_booking){
                    }*/
                    //$("#booking_period").val(booking_period);
                    $("#booking_interval").val(booking_interval);

                    $("#booking_duration").val(booking_duration);
                    $("#minimum_group_size").val(minimum_group_size);
                    $("#maximum_group_size").val(maximum_group_size);

                    $("#restaurant_start_time").val(restaurant_start_time);
                    $("#restaurant_end_time").val(restaurant_end_time);

                    $('#restaurant_start_time').timepicker({
                        timeFormat: 'HH:mm',
                        interval: 60,
                        defaultTime: restaurant_start_time,
                    });

                    $('#restaurant_end_time').timepicker({
                        timeFormat: 'HH:mm',
                        interval: 60,
                        defaultTime: restaurant_end_time,
                    });
                    
                    $('#country_code').val(default_phone_code);
                    $('#table_allocation_link').attr('href', "./table_allotment.html?id="+restaurant_id);

                }

                var user_role = gsession.getSession("user_role");
                if((user_role !== "admin") && (user_role !== "owner")){
                    $(".restaurant_delete_area").remove();
                }

            }


            this.validate_email = function(email){

                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    return regex.test(email);
                  
            }


            this.is_valid_phone_number = function(phone_number) {
                var phoneNum = phone_number.replace(/[^\d]/g, '');
                if(phoneNum.length > 6 && phoneNum.length <= 13) 
                {  
                    return true;  
                }else{
                    return "failed";
                }

            }


            this.is_table_allocated = function(online_status, restaurant_id) {

                if(restaurant_id !== "invalid"){

                    if(online_status === "enable"){

                        
                        var restaurant_details = {"restaurant_id":restaurant_id, type:"check_table_allotment"};
                        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 
                        var response_data = this.send_data(restaurant_details);
                        var response = this.process_data(response_data);
                        $.unblockUI();

                        if(response === "disable"){
                            $(".online_status_error").addClass("show_error_message");
                            $('#online_status').prop('checked', false);
                            //alert("out "+response)
                        }else{
                            //alert(response)
                        }
    
                    }else{
    
                    }
                }else{
                    $(".online_status_error").addClass("show_error_message");
                    $('#online_status').prop('checked', false);
                    //alert("ok")
                }
                
                

            }


}

$(document).on('click','#restaurant_save_btn',function(){
   
   var restaurant_obj = new restaurant();

   var type_of_action = "add";
   type_of_action = $(this).attr("data-type_of_action");


   var validation_result = restaurant_obj.user_input_validation();

   if(validation_result === "pass"){

        var user_data = restaurant_obj.get_user_input(type_of_action);
       restaurant_obj.add_restaurant(user_data);

   }else{
       $(".validation_error").text("Please fill all the details correctly.");
   }

});




$(document).on('click','#delete_restaurant_link',function(){

    $("#deleteModal").modal("show");
 
});


$(document).on('click','#delete_restaurant_btn',function(){

    var restaurant_obj = new restaurant();

    var validation_result = restaurant_obj.delete_restaurant();
 
});


$(document).on('click','#restaurant_clear_btn',function(){


   var restaurant_obj = new restaurant();

   var validation_result = restaurant_obj.clear_input();

});




$(document).on('keypress','.restaurant_textbox',function(){
    
    $(this).removeClass("validation_border_error");
    $(".validation_error").html("");

});




$(document).on('keypress','.number_only_validation',function(evt){
    
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

});


$(document).ready(function() {

    var restaurant_obj = new restaurant();

    var user_role = gsession.getSession("user_role");
    
    if(user_role !== "admin" && user_role !== "owner" && user_role !== "manager"){
        
        alert("Permission denied.");
        window.location = "./dashboard.html";
        
    }

    var file, n;

    file = window.location.pathname;
    n = file.lastIndexOf('/');
    if (n >= 0) {
        file = file.substring(n + 1);
    }

    if(file==="restaurant.html"){

        var restaurant_id = restaurant_obj.get_restaurant_id();
        
        var user_hash = gsession.getSession("user_hash");

        restaurant_obj.load_restaurant_details(restaurant_id,user_hash);
    }
//alert(file);


$('#restaurant_start_time').timepicker({
    timeFormat: 'HH:mm',
    interval: 60,
    defaultTime: '00',
});

$('#restaurant_end_time').timepicker({
    timeFormat: 'HH:mm',
    interval: 60,
    defaultTime: '00',
});

});




$(document).on('change','.mandatory_field',function(){
    
    $(this).removeClass("validation_border_error");
    //$(".validation_error").html("");

});


$(document).on('blur','#restaurant_name',function(evt){

    var restaurant_name = $("#restaurant_name").val().trim();

    var name_length = restaurant_name.length;

    if(name_length<3 || name_length>50){
        $(this).addClass("validation_border_error");
        $(".restaurant_name_validation_error").show();
    }else{
        $(this).removeClass("validation_border_error");
        $(".restaurant_name_validation_error").hide();
    }
    
});



$(document).on('blur','#booking_notification_email',function(evt){
    var email_address = $("#booking_notification_email").val();
    var restaurant_obj = new restaurant();
    var email_status = restaurant_obj.validate_email(email_address);

    if(email_status === false){

        $(this).addClass("validation_border_error");
        $(".email_validation_error").show();

    }else{
        $(this).removeClass("validation_border_error");
        $(".email_validation_error").hide();
    }
    

});


$(document).on('keypress','#booking_notification_email',function(){
    
    $(".email_validation_error").hide();

});


$(document).on('keypress','#restaurant_name',function(){
    
    $(".restaurant_name_validation_error").hide();

});



$(document).on('blur','#email_address',function(evt){
    var email_address = $("#email_address").val();
    var restaurant_obj = new restaurant();
    var email_status = restaurant_obj.validate_email(email_address);

    if(email_status === false){

        $(this).addClass("validation_border_error");
        $(".email_validation_error1").show();

    }else{
        $(this).removeClass("validation_border_error");
        $(".email_validation_error1").hide();
    }
    

});


$(document).on('keypress','#email_address',function(){
    
    $(".email_validation_error1").hide();

});


$(document).on('blur','#phone_number',function(evt){
    
    var phone_number = $("#phone_number").val();
    var restaurant_obj = new restaurant();
    var phone_number_status = restaurant_obj.is_valid_phone_number(phone_number);

    if(phone_number_status === "failed"){

        $(this).addClass("validation_border_error");
        $(".phno_validation_error").addClass("validation_error");

    }else{
        $(this).removeClass("validation_border_error");
        $(".phno_validation_error").removeClass("validation_error");
    }

});


$(document).on('keypress','#email_address',function(){
    
    $(".email_validation_error1").hide();

});



$(document).on('blur','.mandatory_field',function(evt){
    var max_value = parseInt($(this).attr('max'));
    var min_value = parseInt($(this).attr('min'));
    var value = $(this).val().trim();
    if(value>=min_value && value<=max_value){
        
        $(this).removeClass("validation_border_error");

    }else{
        
        $(this).addClass("validation_border_error");

    }
    

});


$(document).on('change', '#online_status', function() {
    // your code
    var restaurant_obj = new restaurant();

    if ($('#online_status').is(":checked"))
    {
        var online_status="enable";
    }else{
        var online_status="disable";
    }
    
    

    var file, n;

    file = window.location.pathname;
    n = file.lastIndexOf('/');
    if (n >= 0) {
        file = file.substring(n + 1);
    }
    var restaurant_id = "";
    if(file==="add_restaurant.html"){
        
        restaurant_id = "invalid";
        restaurant_obj.is_table_allocated(online_status,restaurant_id);
        
    }else{
        
        restaurant_id = restaurant_obj.get_restaurant_id();
        restaurant_obj.is_table_allocated(online_status,restaurant_id);
        
    }
    
    

});