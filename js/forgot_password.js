function forgot_password(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'user_registration', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }

    this.reset_password = function(){


        var new_password     = $("#new-password").val();
        var confirm_password = $("#confirm-password").val();

        if(!new_password && !confirm_password){

            alert("Please fill the password");

        }else if(new_password !== confirm_password){

            alert("password doesn't matched !!!");
        
        }else{

            //var forgot_password_obj = new forgot_password();

            var activation_id = this.get_activation_link_id();

            if(activation_id){

                var reset_password_data = {new_password, activation_id, "type":"reset_password"};

                var response_data = this.send_data(reset_password_data);
            
                var parsed_data  = this.process_data(response_data);

                if(parsed_data == "true"){

                    $("#formContent").remove();
                    $("#error_msg_area").html("<h2>Password Changed Successfully!</h2>");

                }else if(parsed_data == "error"){

                    alert("Error Occured. Try again later");
                }


            }else{
                alert("Invalid URL");
            }

        }
    
        
    }


    this.forgot_password_activation_link = function(email_address){

        var password_activation_link = {email_address,"type":"password_activation_link"};

        var response_data = this.send_data(password_activation_link);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "not_exist"){

            alert("Given email is not associated with any account.");

        }else if(parsed_data == "false"){

            alert("Error Occured. Try again later.");
        
        }else if(parsed_data == "true"){

            alert('Please check your e-mail, we have sent a password reset link to your email.')
        }

    }

    this.check_activation_link_id = function(activation_id){

        var activation_id_info = {activation_id, "type":"check_activation_link_id"};

        var response_data = this.send_data(activation_id_info);
    
        var parsed_data   = this.process_data(response_data);

        //console.log(parsed_data);

        if(parsed_data == "valid"){

        }else if(parsed_data == "link_expired"){
            $("#formContent").remove();
            $("#error_msg_area").html("<h2>Link has been expired</h2>");
        }else if(parsed_data == "time_expired"){
            $("#formContent").remove();
            $("#error_msg_area").html("<h2>Time has been expired for this link.</h2>");
        }else if(parsed_data == "error"){
            $("#formContent").remove();
            $("#error_msg_area").html("<h2>Something went wrong, generate new link.</h2>");
        }else if(parsed_data == "invalid_link"){
            $("#formContent").remove();
            $("#error_msg_area").html("<h2>Invalid Link</h2>");
        }

    }


    this.get_activation_link_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var activation_id = url.searchParams.get("id");
        return activation_id;

    }


}

$(document).ready(function() {

    var forgot_password_obj = new forgot_password();
    

    var file, n;

    file = window.location.pathname;
    n = file.lastIndexOf('/');
    if (n >= 0) {
        file = file.substring(n + 1);
    }

    if(file==="forgot_password_link.html"){

        var activation_id = forgot_password_obj.get_activation_link_id();
        if(activation_id){
            forgot_password_obj.check_activation_link_id(activation_id);
        }else{
            $("#formContent").remove();
            $("#error_msg_area").html("<h2>Invalid URL</h2>");
        }

    }

});



$(document).on('click','#reset-password',function(){
              
    var forgot_password_obj = new forgot_password();
    forgot_password_obj.reset_password();

});


$(document).on('click','#password-activation',function(){
              
    var email_address  = $("#email-address").val().trim();

    if(email_address){

        var forgot_password_obj = new forgot_password();
        forgot_password_obj.forgot_password_activation_link(email_address);

    }else{
        alert("Enter your email to get reset password link.");
    }

    
   
});






$(document).on('keypress','.user_input',function(event){
              
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		var forgot_password_obj = new forgot_password();
        forgot_password_obj.reset_password();
	}
   
});
