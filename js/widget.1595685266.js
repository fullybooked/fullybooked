function widget_details(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'booking', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }

    this.get_onload_details = function(restaurant_group_id,restaurant_id){

       
        var data = {restaurant_group_id,restaurant_id,"type":"widget_onload"};

        var response_data = this.send_data(data);
        var parsed_data   = this.process_data(response_data);

        this.restaurant_details(parsed_data.restaurant_details);
        this.generate_booking_time(parsed_data.booking_time);
        this.generate_guest_details(parsed_data.guest_count);
        this.hide_booked_data(parsed_data.booked_time);
      
    }

    this.restaurant_details = function(restaurant_data){

        var temp = "<option value='' disabled> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Restaurants</option>";
       
        var restaurant_count = Object.keys(restaurant_data).length;

        var selected_restaurant_name = "";
        var selected_restaurant_id   = this.find_restaurant_id();

        for(let index=1; index <=restaurant_count; index++){

            var restaurant_id      = restaurant_data[index].restaurant_id;
            var restaurant_name    = restaurant_data[index].restaurant_name;
            var notification_email = restaurant_data[index].notification_email;
            var country_code       = restaurant_data[index].default_phone_code;

            if(index == 1){

                temp += `<option value=${restaurant_id} data-country_code=${country_code} data-email=${notification_email} selected> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${restaurant_name}</option>`;
                

            }else{

                temp += `<option value=${restaurant_id} data-country_code=${country_code} data-email=${notification_email}> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${restaurant_name}</option>`;
           
            }

           
        }

        
        $("#restaurant-name").html(temp);
        $("#restaurant-name").val(selected_restaurant_id);
        selected_restaurant_name = $("#restaurant-name option:selected").text(); 
        $("#restaurant-name").attr("data-selected_restaurant_id",selected_restaurant_id);
        $("#restaurant-name").attr("data-selected_restaurant_name",selected_restaurant_name);
        var current_country_phone_code = $("#restaurant-name option:selected").attr("data-country_code");
        //alert(current_country_phone_code)
        //$("#restaurant-name option:selected").val();
        $(".country_code").val(current_country_phone_code);


        $(".restaurant-table-header").html(selected_restaurant_name.trim());

        


    }

    this.get_current_time =  function(){

        var current_date = new Date();
        var time = (current_date.getHours() < 10 ? '0' : '')+current_date.getHours() + "." + (current_date.getMinutes() < 10 ? '0' : '')+current_date.getMinutes();
       
        return time;
    }

    this.get_current_date = function(){

        var new_date = new Date(); 

        return (new_date.getDate() + '/' + Number(new_date.getMonth() + 1) + '/' + new_date.getFullYear());

    }

    this.get_selected_date = function(){

        var selected_date = $("#booking_date").datepicker('getDate');
       
        selected_date = new Date(selected_date);

        return (selected_date.getDate() + '/' + Number(selected_date.getMonth() + 1) + '/' + selected_date.getFullYear());
    }


    this.generate_booking_time = function(booking_time){

        var guest_count   = $(".guest-count").find("option:selected").val();

        $(".time_picker_loader").hide();


        $(".timepicker-body").html("");


        var time_temp  = "";
        var time_count = 1;

        var current_time = this.get_current_time();

        var current_date = this.get_current_date();
        var selected_date = this.get_selected_date();
     
        for(let index = 0; index < booking_time.length; index++) {

            var timepicker_class = "is-available";
            var disabled_attr    = "";

            var booking_time_value = booking_time[index].time;

            booking_time_value = booking_time_value.toString();

            var booking_time_check = parseFloat(booking_time_value.replace(':', '.'));
            var booking_time_class = booking_time_value.replace(':', '_');

            if(current_date == selected_date){

                if(parseFloat(current_time) >= booking_time_check){

                    timepicker_class = "is-not-available";
                    //disabled_attr    = "disabled";
                }

            }


            time_temp += `<span class="timepicker-slot ${timepicker_class} timepicker-time booking_time_${booking_time_class}" data-time_value="${booking_time[index].time}" data-guest_count="${guest_count}">${booking_time[index].time}</span>`;


        }

       $(".timepicker-body").html(time_temp);
    
    }


    this.generate_guest_details = function(guest_count){

        var minimun_count = parseInt(guest_count.minimum_group_size);
        var maximun_count = parseInt(guest_count.maximum_group_size);
        
        var guest_temp = `<option value="number_of_guest"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of Guests</option>`;

        if(minimun_count != 0 && maximun_count != 0){
            
            for(let index = minimun_count; index <= maximun_count; index++) {

                var selectedAttr = "";

                if(index === 2){
                    selectedAttr = "selected";
                }

                guest_temp += `<option value="${index}" ${selectedAttr}> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${index} guest</option>`
        

            }

        }
    
        $(".guest-count").html(guest_temp);

    }



    this.add_booking_details = function(restaurant_group_id,language,restaurant_id,restaurant_name,guest_count,booking_date,booking_time,alergy_type,customer_need,guest_name,email_address,country_code,phone_number,additional_notes,restaurant_email,booking_type,table_guest_count){


        var booking_data = {restaurant_group_id,language,restaurant_id,guest_count,booking_date,booking_time,alergy_type,customer_need,guest_name,email_address,country_code,phone_number,additional_notes,restaurant_email,booking_type,table_guest_count,"status":"received","assigned_table":"automatic","send_email":"true","send_sms":"false","send_advance_sms":"false","restaurant_notify":"true","user_hash":"","booking_id":"yet_to_generate","type":"new_booking"};

        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 


        var response_data = this.send_data(booking_data);
        var parsed_data   = this.process_data(response_data);

        this.add_response_status(parsed_data,restaurant_id,restaurant_name,booking_time);
    
        
    }

    this.add_response_status = function(parsed_data,restaurant_id,restaurant_name,booking_time){   

        $.unblockUI();


        if(parsed_data == "true"){

            $(window).scrollTop(0);


            //$(".booking-success-modal").show();


            // $(".booking_message").html("Booking added successfully !!!");
            // $(".booking_message").css({"color":"#3240B7"});

            $(".choose_language").removeClass("active_language");
            $(".en_language").addClass("active_language");


            var selected_restaurant_id   = $("#restaurant-name").attr("data-selected_restaurant_id");
            var selected_restaurant_name = $("#restaurant-name").attr("data-selected_restaurant_name");
            selected_restaurant_name     = $.trim(selected_restaurant_name);


            $("#restaurant-name").val(selected_restaurant_id);
            $(".restaurant-table-header").html(selected_restaurant_name);
            $("#guest-count").val("number_of_guest");
            $("#booking_date").datepicker({minDate:new Date()});
            $(".timepicker-time").removeClass("timepicker-time-active");
            $('input[name="allergy_type"]:checked').prop("checked",false);
            $('input[name="guest_need"]:checked').prop("checked",false);
            $(".guest_name").val("");
            $(".guest_email").val("");
            $(".phone_number").val("");
            $(".comments").val("");
            $(".comments").attr("placeholder", "Do you want to say anythings to us...");
            
            window.location = `guest_booking_confirmation.html?id=${restaurant_name}`;
            jQuery('html,body').animate({scrollTop: 0}, 'slow');

        }else if(parsed_data === "false"){

            $(window).scrollTop(0);


            var restaurant_phone_number = this.get_restaurant_phone_numer(restaurant_id);

            
           window.location = `guest_booking_error.html?id=${restaurant_phone_number}`;

           // $(".booking-error-modal").show();


            // $(".booking_message").html("Some Error Occured. Try again later !!!");
            // $(".booking_message").css({"color":"#3240B7"});


        }else if(parsed_data === "table_not_available"){

            var booking_time_class = booking_time.replace(':', '_');

            $(".booking_time_"+booking_time_class).removeClass("is-available");
            $(".booking_time_"+booking_time_class).addClass("is-not-available");
            $(".booking_time_"+booking_time_class).removeClass("timepicker-time-active");

            $(".table_not_available_header").show();
            $(".time_slot_error").show();
            

        }


    }

    this.get_restaurant_phone_numer = function(restaurant_id){

        var restaurant_data = {restaurant_id,"type":"get_phone_number"};

        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 

        var response_data = this.send_data(restaurant_data);
        var parsed_data   = this.process_data(response_data);

        return parsed_data;
    }



    this.get_booking_time_guest_count = function(guest_count,restaurant_id,date){  

        var booking_data = {guest_count,restaurant_id,date,"type":"booking_time_with_guest_count"};

        var response_data = this.send_data(booking_data);
        var parsed_data   = this.process_data(response_data);

        this.generate_booking_time(parsed_data.booking_time);
        this.hide_booked_data(parsed_data);

    }

    this.hide_booked_data = function(booked_data){

        // var table_guest_count  = booked_data.table_guest_count;

        // $(".timepicker").attr("data-table_guest_count",table_guest_count);


        $(".timepicker-body").show();

        $(".time_picker_loader").hide();

        var restaurant_seat_status = booked_data["restaurant_seat_status"];
        var booking_time = booked_data["booked_time"];

        if(restaurant_seat_status != "not_available"){

            if(booking_time !== undefined){
       
                if(Object.keys(booking_time).length != 0){

                    $.each( booking_time, function( key, value ) {

                        var time = value["time"];

                        var booking_time_class = time.replace(':', '_');

                        $(".booking_time_"+booking_time_class).removeClass("is-available");
                        $(".booking_time_"+booking_time_class).addClass("is-not-available");
                        
                      });
                }


                // if(booking_available_time.length != 0){

                //     for(let index = 0; index < booking_available_time.length; index++) {
    
    
                //         var time                  = booking_available_time[index]["time"];
                //         var available_guest_count = booking_available_time[index]["guest_count"];
    
                //         var booking_time_class = time.replace(':', '_');

                //         $(".booking_time_"+booking_time_class).attr( "data-guest_count",available_guest_count);
              
                       
                //     }
                // }
                        
            }

        }else{

                $(".timepicker-time").removeClass("is-available");
                $(".timepicker-time").addClass("is-not-available");
            //    $(".timepicker-time").attr( "disabled", "disabled");

        }

     
    }


    this.get_booking_time = function(restaurant_id){

        var booking_data = {restaurant_id,"type":"booking_time"};

        var response_data = this.send_data(booking_data);
        var parsed_data   = this.process_data(response_data);
       

        this.generate_booking_time(parsed_data.booking_time);
        this.generate_guest_details(parsed_data.guest_count);
    }

    this.set_time_picker = function(changed_date){

        var changed_date = this.get_selected_date();
        var current_date = this.get_current_date();

        if(current_date != changed_date){

            $(".timepicker-time").removeClass("btn-light");
            $(".timepicker-time").addClass("btn-success");
            $(".timepicker-time").removeAttr("disabled");
        
        }else{

      
            var restaurant_id = $("#restaurant-name").val();

            this.get_booking_time(restaurant_id);
        }

    }

    this.find_restaurant_group_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");

        return id;

    }

    this.find_restaurant_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id1");

        return id;

    }

    this.is_valid_email = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    this.disable_all_date = function(){

            $(".timepicker-time").removeClass("btn-success");
            $(".timepicker-time").addClass("btn-light");
            $(".timepicker-time").attr( "disabled", "disabled");

    }
    this.is_valid_phone_number = function(phone_number) {
        var phoneNum = phone_number.replace(/[^\d]/g, '');
        if(phoneNum.length >= 9 && phoneNum.length <= 15) 
        {  
            return true;  
        }else{
            return "failed";
        }

    }
    this.is_valid_name = function(name) {
        var regex = /^[ A-Za-z'\s]*$/;
        return regex.test(name);

    }


}


$(document).ready(function() {

    $(".booking-error-modal").hide();
    $(".booking-success-modal").hide();
    $(".booking_mandatory_header").hide();
    $(".table_not_available_header").hide();

    
    var widget_obj = new widget_details();

    $(".time_picker_loader").hide();

   

    $('#booking_date').datepicker({
        minDate:new Date(),
        onSelect: function (dateText, inst){             
           
            var guest_count         = $("#guest-count").val();
            var restaurant_id       = $("#restaurant-name").val();

            widget_obj.get_booking_time_guest_count(guest_count,restaurant_id,dateText);
               
        },
        onChangeMonthYear: function (year, month) {

            var selected_date = $("#booking_date").datepicker('getDate');
            var todayDate     = new Date(selected_date);
           
            var todayMonth    = (todayDate.getMonth()+1);

            if(month === todayMonth){

                var guest_count   = $("#guest-count").val();
                var restaurant_id = $("#restaurant-name").val();
                
                widget_obj.get_booking_time_guest_count(guest_count,restaurant_id,selected_date);

            }else{

                widget_obj.disable_all_date();
            }

        } 
    });

    var restaurant_group_id = widget_obj.find_restaurant_group_id();
    var restaurant_id = widget_obj.find_restaurant_id();


      
    widget_obj.get_onload_details(restaurant_group_id,restaurant_id);
    //widget_obj.generate_time_picker();

});

$(document).on('click', '.timepicker-time', function(){

    if($(this).hasClass("is-available")){

        $(".timepicker-time").removeClass("timepicker-time-active");
        $(this).addClass("timepicker-time-active");
    
        $(".time_slot_error").hide();
        $(".booking_mandatory_header").hide();
        $(".table_not_available_header").hide();
    

        var new_guest_count = $(this).attr("data-guest_count");
    
        // $(".timepicker").attr("data-table_guest_count",new_guest_count);

    }

   

});


$(document).on('click', '.choose_language', function(){
    
    $(".choose_language").removeClass("active_language");
    $(this).addClass("active_language");
   
});


$(document).on('click', '#make_booking', function(){

    var widget_obj = new widget_details();


    var language            = $(".active_language").attr("data-language");
    var restaurant_id       = $("#restaurant-name").val();
    var restaurant_name     = ($("#restaurant-name option:selected").text()).trim();
    var guest_count         = $("#guest-count").val();
    var booking_date        = $("#booking_date").datepicker('getDate');
    var time_value          = $(".timepicker-time-active").attr("data-time_value");
    var customer_need       = $('input[name="guest_need"]:checked').val();
    var guest_name          = ($(".guest_name").val()).trim();
    var guest_email         = $(".guest_email").val();
    var country_code        = $(".country_code").val();
    var phone_number        = $(".phone_number").val();
    var comments            = $(".comments").val();
    var notification_email  = $("#restaurant-name option:selected").attr("data-email");
    var restaurant_group_id = widget_obj.find_restaurant_group_id();
    var booking_type        = "new_booking";
    //var table_guest_count   = $(".timepicker").attr("data-table_guest_count");
    var table_guest_count   = guest_count;
    
    var alergy_type = [];

    $('input[name="allergy_type"]:checked').each(function() {
        alergy_type .push(this.value);
    });


    alergy_type = alergy_type.join();

    var validation_status = "true";

    var currentDate = new Date();
    var currentHour = currentDate.getHours();
    var currentMinute = currentDate.getMinutes();
    var currentSeconds = currentDate.getSeconds();



    booking_date = new Date(booking_date);

    booking_date.setHours(currentHour,currentMinute,currentSeconds);
    

    if(guest_count == "number_of_guest"){
        $(".guest_count_container").addClass("validation_border_error");
        $(".guest_count_error").show();
        validation_status = "false";

    }


    if(time_value === undefined || time_value === null || time_value === ""){
        $(".time_slot_error").show();
        validation_status = "false";

    }


    if(widget_obj.is_valid_name(guest_name) === false || guest_name === "" || guest_name === " "){

        $(".guest_name").addClass("validation_border_error");

        $(this).addClass("validation_border_error");
        $(".name_validation_error").show();

        validation_status = "false";
    }


    if(widget_obj.is_valid_email(guest_email) === false){
        $(".guest_email").addClass("validation_border_error");
        $(".email_validation_error").show();

        validation_status = "false";

    }

    if((widget_obj.is_valid_phone_number(phone_number)) === "failed"){
        $(".country_code_container").addClass("validation_border_error");
        $(".phno_validation_error").show();
        validation_status = "false";

    }
   


    // var validation_email_status = widget_obj.is_valid_email(guest_email);

    // if(validation_email_status === false){

    //     $(".guest_email").addClass("error");
    //     validation_status = "false";

    // }

  
    alergy_type = (alergy_type === undefined)? "none":alergy_type
    customer_need = (customer_need === undefined)? "none":customer_need

    if(validation_status === "true"){


        widget_obj.add_booking_details(restaurant_group_id,language,restaurant_id,restaurant_name,guest_count,booking_date,time_value,alergy_type,customer_need,guest_name,guest_email,country_code,phone_number,comments,notification_email,booking_type,table_guest_count);

    }else{

        $(".booking_mandatory_header").show();
    }


});




$(document).on('change', '.guest_count_container', function(){

    $(".table_not_available_header").hide();

    if ($(this).hasClass("validation_border_error")) {
        $(".booking_mandatory_header").hide();
        $(".guest_count_error").hide();
        $(this).removeClass("validation_border_error");
        $(this).addClass("input_border");
    }

});




$(document).on('keyup', '.country_code_container', function(){

    if ($(this).hasClass("error")) {
        $(this).removeClass("error");
        $(this).addClass("input_border");
    }

});


$(document).on('keyup', '.guest_email', function(){

    if ($(this).hasClass("error")) {
        $(this).removeClass("error");
        $(this).addClass("input_border");
    }

});



$(document).on('change', '#guest-count', function(){

    $(".guest_count_error").hide();

    var guest_count   = $(this).val();
    var restaurant_id = $("#restaurant-name").find("option:selected").val();
    var date          = $("#booking_date").datepicker('getDate');

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

        date = `${day}-${month}-${year}`;


    var widget_obj = new widget_details();

    $(".timepicker-body").hide();
    $(".time_picker_loader").show();

    widget_obj.get_booking_time_guest_count(guest_count,restaurant_id,date);


});

$(document).on('change', '#restaurant-name', function(){

    var restaurant_id   = $("#restaurant-name option:selected").val();
    var restaurant_name  = $("#restaurant-name option:selected").text();
    var current_country_phone_code = $("#restaurant-name option:selected").attr("data-country_code");
        
    
    var widget_obj = new widget_details();

    $(".time_picker_loader").show();

   
    restaurant_name = $.trim(restaurant_name);


    $(".restaurant-table-header").html(restaurant_name);


    widget_obj.get_booking_time(restaurant_id);
    $(".country_code").val(current_country_phone_code);

});


$(".phone_number").on("keypress keyup",function (event) {    
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });

//  $(document).on('blur','.phone_number',function(evt){
    
//     var phone_number = $(".phone_number").val();
//     var widget_obj   = new widget_details();

//     var phone_number_status = widget_obj.is_valid_phone_number(phone_number);

//     if(phone_number_status === "failed"){

//         $(this).addClass("validation_border_error");
//         $(".phno_validation_error").addClass("validation_error");

//     }else{
//         $(this).removeClass("validation_border_error");
//         $(".phno_validation_error").removeClass("validation_error");
//     }

// });

$(document).on('click', '.booking-close', function(){

    $(".booking-success-modal").hide();
    $(".booking-error-modal").hide();

});

$(document).on('click', '.powered_by_navigation', function(){

    window.location = "https://fullybooked.restaurant/";

});






$(document).on('click', '.choose_language', function(){

    var language = $(this).data("language");
    //alert(language);
    if(language === "English"){

        
        $("#guest-count option").html(function(i,str){
            return str.replace(/gæst|gäst/g,  // here give words to replace
               function(m,n){
                   return "Guest"  // here give replacement words
               });
          });

        $('select[id=guest-count] > option:first-child').text('Number of guests');

        $(".allergy_text").text("Allergies, if any?");
        $(".allergy_1").text("Gluten Allergy");
        $(".allergy_2").text("Nuts Allergy");
        $(".allergy_3").text("Coriander Allergy");
        $(".allergy_4").text("Other Allergy, Check with me, I am allergic");

        $(".help_us_text").text("Help us to serve you better, do you need?");
        $(".help_us_text_1").text("Kids high chair needed");
        $(".help_us_text_2").text("Other");

        $(".guest_name").attr("placeholder","Your name");
        $(".guest_email").attr("placeholder","Your email address");
        $(".phone_number").attr("placeholder","Your phone number");
        $(".comments").attr("placeholder","Do you want to say anything to us...");

        
        $("#make_booking").html("Make a booking");
        $(".powered_by_text").text("Powered by: ");


    } else if(language === "Danish"){

        
        $("#guest-count option").html(function(i,str){
            return str.replace(/gæst|gäst|Guest|guest/g,  // here give words to replace
               function(m,n){
                   return "gæst"  // here give replacement words
               });
          });

        $('select[id=guest-count] > option:first-child').text('Antal gæster');

        $(".allergy_text").text("Allergier, hvis nogen?");
        $(".allergy_1").text("Glutenallergi");
        $(".allergy_2").text("Nødder Allergi");
        $(".allergy_3").text("Koriander Allergi");
        $(".allergy_4").text("Anden allergi, tjek med mig, jeg er allergisk");

        $(".help_us_text").text("Hjælp os med at tjene dig bedre, har du brug for?");
        $(".help_us_text_1").text("Børn høj stol behov");
        $(".help_us_text_2").text("Andet");

        $(".guest_name").attr("placeholder","Dit navn");
        $(".guest_email").attr("placeholder","Din email adresse");
        $(".phone_number").attr("placeholder","Dit telefonnummer");
        $(".comments").attr("placeholder","Vil du sige noget til os?");

        
        $("#make_booking").html("Foretag en reservation");
        $(".powered_by_text").text("Powered by: ");


    } else if(language === "Spanish"){
        
        
        $("#guest-count option").html(function(i,str){
            return str.replace(/gæst|gäst|Guest|guest/g,  // here give words to replace
               function(m,n){
                   return "gäst"  // here give replacement words
               });
          });

        $('select[id=guest-count] > option:first-child').text('Antal gäster');

        $(".allergy_text").text("Allergier, om något?");
        $(".allergy_1").text("Glutenallergi");
        $(".allergy_2").text("Muttrar Allergi");
        $(".allergy_3").text("Korianderallergi");
        $(".allergy_4").text("Annan allergi, kolla med mig, jag är allergisk");

        $(".help_us_text").text("Hjälp oss att tjäna dig bättre, behöver du?");
        $(".help_us_text_1").text("Barnstol behövs");
        $(".help_us_text_2").text("Övrig");

        
        $(".guest_name").attr("placeholder","Ditt namn");
        $(".guest_email").attr("placeholder","din e-postadress");
        $(".phone_number").attr("placeholder","Ditt telefonnummer");
        $(".comments").attr("placeholder","Vill du säga något till oss?");

        
        $("#make_booking").html("Gör en bokning");
        $(".powered_by_text").text("Powered by: ");


    }

});


$(document).on('blur','#guest_email',function(evt){
    var email_address = $("#guest_email").val();
    var widget_obj = new widget_details();
    var email_status = widget_obj.is_valid_email(email_address);

    if(email_status === false){

        $(this).addClass("validation_border_error");
        $(".email_validation_error").show();

    }else{
    $(".booking_mandatory_header").hide();

        $(this).removeClass("validation_border_error");
        $(".email_validation_error").hide();
    }
    

});



$(document).on('keypress','#guest_email',function(evt){
   
        $(".booking_mandatory_header").hide();

        $(this).removeClass("validation_border_error");
        $(".email_validation_error").hide();

});


$(document).on('blur','#guest_name',function(evt){
    var name = ($("#guest_name").val()).trim();
    var widget_obj = new widget_details();
    var name_status = widget_obj.is_valid_name(name);

    if(guest_name === "" || guest_name === " " || name_status === false){

        $(this).addClass("validation_border_error");
        $(".name_validation_error").show();

    }else{
    $(".booking_mandatory_header").hide();

        $(this).removeClass("validation_border_error");
        $(".name_validation_error").hide();
    }
    

});


$(document).on('keypress','#guest_name',function(e){

    $(".booking_mandatory_header").hide();

    $(this).removeClass("validation_border_error");
    $(".name_validation_error").hide();

    var key = e.keyCode;
    if (key >= 48 && key <= 57) {
        e.preventDefault();
        return false;
    }
   
});

$(document).on('keypress','.phone_number',function(evt){

   
    $(".booking_mandatory_header").hide();

    $(this).removeClass("validation_border_error");
    $(".phno_validation_error").hide();

    $(".phno_validation_error").removeClass("validation_error");
 
});

$(document).on('blur','.phone_number',function(evt){

    var phone_number = $(".phone_number").val();
    var widget_obj = new widget_details();
    var phone_number_status = widget_obj.is_valid_phone_number(phone_number);

    if(phone_number_status === "failed"){

        $(this).addClass("validation_border_error");
        $(".phno_validation_error").show();
        $(".phno_validation_error").addClass("validation_error");

    }else{
        $(".booking_mandatory_header").hide();

        $(this).removeClass("validation_border_error");
        $(".phno_validation_error").hide();

        $(".phno_validation_error").removeClass("validation_error");
    }

});



