function user_management(){

            /** User input validation */
            this.user_input_validation = function(){

                var validation_result = "pass";
                $( ".restaurant_textbox" ).each(function( index ) {
                    if(! $( this ).val()){
                        validation_result = "fail";
                    }
                });
                return validation_result;

            }

            this.send_data = function(user_data){

                var response_data='';
                
                    ajaxcall.send_data(user_data, 'manage_users', function(data) {
                        
                    response_data = data;
                    
                    });
                    
                return response_data;
            }


            this.process_data = function(data) {

                var data        = JSON.parse(data);
                return data;
            }


            this.add_new_user = function(){

                var user_details = this.get_new_user_input();
                $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 

                if(user_details !== "failed"){

                    var response_data = this.send_data(user_details);
            
                    var parsed_data   = this.process_data(response_data);
            
                    if(parsed_data == "account_created"){
            
                        this.clear_input();
                        window.location = "my_users.html";
            
                    }else if(parsed_data == "email_exist"){
                        
                        $.unblockUI();
                        $(".email_error").html("Email already exists");
                        $("#user_email").addClass("validation_failed");


                    }else{

                        $.unblockUI();
                        alert("Something went wrong, please try again");
                        
                    }

                }
                $.unblockUI();

                
        
            }


            this.delete_user = function(user_hash){

                var user_details = {user_hash, "type":"delete_user"}
                var response_data = this.send_data(user_details);
            
                var parsed_data   = this.process_data(response_data);
        
                if(parsed_data == "true"){
        
                    $("#delete_account_modal").modal("hide");
                    window.location = "my_users.html";
        
                }else{

                    alert("Something went wrong, please try again");
                    
                }
        
            }


            this.list_all_user = function(){

                var user_hash = gsession.getSession("user_hash");
                var user_details = {user_hash, "type":"list_users"};

                var response_data = this.send_data(user_details);
            
                var parsed_data   = this.process_data(response_data);

                var available_user_count = parsed_data["user_count"];

                if(available_user_count >=1){

                    var user_html_template = '';

                    for(var user_flag=1; user_flag<=available_user_count; user_flag++){
                        var user_hash = parsed_data["user_hash_"+user_flag];
                        var display_name = parsed_data["user_name_"+user_flag];
                        var user_email = parsed_data["user_email_"+user_flag];
                        var user_role = parsed_data["user_role_"+user_flag];
                        

                        var available_restaurant_count = parsed_data["restaurant_info_"+user_hash]["restaurant_count"];


                        //for(var restaurant_flag=1; restaurant_flag<=available_restaurant_count; restaurant_flag++){
                            var restaurant_flag = 1;
                            var restaurant_name = parsed_data["restaurant_info_"+user_hash]["restaurant_name_"+restaurant_flag];
                            var restaurant_id = parsed_data["restaurant_info_"+user_hash]["restaurant_id_"+restaurant_flag];
                            if(!restaurant_name){
                                restaurant_name = "All Restaurants";
                            }
                            if(!restaurant_id){
                                restaurant_id = "";
                            }
                            //var restaurant_id = "";


                            user_html_template += `<tr>
                            <td>`+display_name+`</td>
                            <td>`+user_email+`</td>
                            <td>`+user_role+`</td>
                            <td>`+restaurant_name+`</td>
                            <td><button class="btn btn-success delete_user" data-user_hash="`+user_hash+`" data-user_name = "`+display_name+`" data-user_email = "`+user_email+`" data-user_role = "`+user_role+`" data-restaurant_id="`+restaurant_id+`" data-restaurant_name = "`+restaurant_name+`">Delete</button></td>
                            </tr>`;
                        //}
                        

                    }
        
                    $("#user_list_table").html(user_html_template);
                    
        
                }else{

                    $("#table_data_area").html("<p class='center_align'>Yet to add a new user </p>");

                }
        
            }

            this.load_restaurant_and_roles_dropdown = function(response_data){
                
                var restaurant_count = response_data ["restaurant_count"];
                var roles_count = response_data ["roles_count"];
                var roles_flag = 0;
                var restaurant_flag = 0;
                var roles_html_template ='';
                var restaurants_html_template ='';
                for(roles_flag = 1; roles_flag<=roles_count; roles_flag++){
                    var role_name = response_data ["role_name_"+roles_flag];
                    var role_id = response_data ["role_id_"+roles_flag];
                    roles_html_template += "<option value='"+role_id+"'> "+role_name+"</option>";
                }
                $("#user_role_dropdown").html(roles_html_template);

                for(restaurant_flag = 1; restaurant_flag<=restaurant_count; restaurant_flag++){
                    var restaurant_name = response_data ["restaurant_name_"+restaurant_flag];
                    var restaurant_id = response_data ["restaurant_id_"+restaurant_flag];
                    restaurants_html_template += "<option value='"+restaurant_id+"'> "+restaurant_name+"</option>";
                }
                $("#restaurant_list_dropdown").html(restaurants_html_template);

                var user_role_dropdown = $("#user_role_dropdown").val();
                if(user_role_dropdown==="admin"){
                    $(".restaurant_area").hide();
                }




            }

            this.load_restaurant_roles = function(){
                var user_hash = gsession.getSession("user_hash");
                var user_details = {user_hash, "type":"load_restaurant_roles"};

                var response_data = this.send_data(user_details);
                var parsed_data   = this.process_data(response_data);
            
                this.load_restaurant_and_roles_dropdown(parsed_data);
            }

            


            this.get_new_user_input = function(){

                var validation_check = "pass";
                var display_name = $("#display_name").val();
                if(!display_name){
                    validation_check = "failed";
                    $("#display_name").addClass("validation_failed");
                }
                var user_email = $("#user_email").val();
                if(user_email){
                    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(!regex.test(user_email)) {
                        validation_check = "failed";
                        $("#user_email").addClass("validation_failed");
                    }
                }else{
                    validation_check = "failed";
                    $("#user_email").addClass("validation_failed");
                }
                var password = $("#password").val();
                if(!password){
                    validation_check = "failed";
                    $("#password").addClass("validation_failed");
                }

                if(validation_check === "pass"){
                    var user_role_dropdown = $("#user_role_dropdown").val();
                    if(user_role_dropdown==="admin"){
                        var restaurant_list_dropdown = "";
                    }else{
                        var restaurant_list_dropdown = $("#restaurant_list_dropdown").val();
                    }
                    
                
                    var owner_hash = gsession.getSession("user_hash");

                    var user_details = {display_name, user_email, password, user_role_dropdown, restaurant_list_dropdown,owner_hash, type:"add_new_user"};
                    
                    return user_details;

                }else{

                    return "failed";

                }
                

            }


            this.clear_input = function(){

                $(".restaurant_textbox").val("");
                $(".restaurant_checkbox").prop("checked", false);
                $('.restaurant_dropdown').prop('selectedIndex',0);


            }



}

$(document).on('click','#add_user',function(){
   
   $("#create_account_modal").modal("show");

});



$(document).on('keypress','.new_user_input',function(){
    
    $(this).removeClass("validation_failed");
 
});

$(document).on('change','#user_email',function(){
    //alert("l")
    $(".email_error").html(" ");
    $("#user_email").removeClass("validation_failed");
 
});



$(document).on('change','#user_role_dropdown',function(){

    var current_selection = $("#user_role_dropdown").val();
    if(current_selection==="admin"){
        $(".restaurant_area").hide();
    }else{
        $(".restaurant_area").show();
    }
 
});


$(document).on('click','.delete_user',function(){
   
    var user_name = $(this).attr("data-user_name");
    var user_email = $(this).attr("data-user_email");
    var user_role = $(this).attr("data-user_role");
    var restaurant_name = $(this).attr("data-restaurant_name");
    var user_hash = $(this).attr("data-user_hash");
    
    $("#delete_account_modal").modal("show");

    $("#display_name_show").val(user_name);
    $("#user_email_show").val(user_email);
    $("#display_role_show").val(user_role);
    if(user_role=="admin"){
        $(".delete_user_restaurant_area").hide();
    }else{
        
        $(".delete_user_restaurant_area").show();
        $("#user_restaurant_show").val(restaurant_name);

    }

    $("#delete_user_account").attr("data-user_hash", user_hash);

 });


$(document).on('click','#restaurant_clear_btn',function(){


   var restaurant_obj = new user_management();
   var validation_result = restaurant_obj.clear_input();

});


$(document).on('click','#delete_user_account',function(){


    var restaurant_obj = new user_management();
    var user_hash = $(this).attr("data-user_hash");
    var validation_result = restaurant_obj.delete_user(user_hash);

     
 });



$(document).on('click','#add_new_user',function(){


    var restaurant_obj = new user_management();
    var validation_result = restaurant_obj.add_new_user();
 
 });

$(document).ready(function() {

    var user_management_obj = new user_management();
    user_management_obj.list_all_user();
    user_management_obj.load_restaurant_roles();


});