function myAccount(){


    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'my_account', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.changeUserDetails = function(userName,userHash){

        var userDetails = {userName,userHash,type:"updateUserDetails"};

        var response_data = this.send_data(userDetails);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data["response"] == "true"){

            //alert("User Details Modified");
            var username = parsed_data["user_name"];
            gsession.setSession("display_name",username);
            $(".display_name").html(username);
        }else{

            alert("Something Wrong, try again");
        }


    }


    this.reset_password = function(currentPassword,newPassword,userHash){

        var userDetails = {currentPassword,newPassword,userHash,type:"updatePassword"};

        var response_data = this.send_data(userDetails);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "true"){

            $("#current-password").val("");
            $("#new-password").val("");
            $("#confirm-password").val("");
            alert("Password changed successfully");
            
            
        }else if(parsed_data == "old_pwd_wrong"){

            alert("Old password is wrong");

        }
        else{

            alert("Something Wrong, try again");
        }


    }

    this.change_restaurant_group_name = function(restaurant_group_name,userHash){

        var userDetails = {restaurant_group_name,userHash,type:"change_restaurant_group_name"};

        var response_data = this.send_data(userDetails);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "true"){

            alert("Name changed successfully");
            
        }
        else{

            alert("Something Wrong, try again");

        }


    }



    this.load_profile_details = function(){

        var userHash = gsession.getSession("user_hash");

        var userDetails = {userHash,type:"load_profile_details"};

        var response_data = this.send_data(userDetails);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data["response"] == "true"){

            //alert("User Details Modified");
            var user_name  = parsed_data["user_name"];
            var email  = parsed_data["email"];
            var user_role  = parsed_data["user_role"];

            $("#user-name").val(user_name);
            $("#email-address").val(email);
            $("#user-role").val(user_role);

            if(user_role === "owner" || user_role === "admin"){

                var restaurant_group_name  = parsed_data["restaurant_group_name"];

                $("#restaurant_group_name").val(restaurant_group_name);
                $("#restaurant_settings_area").show();

            }

        }else{

            alert("Something Wrong, try again");
        }

    }




}


$(document).ready(function() {

    $(".my-account-edit").hide();
    $("#restaurant_settings_area").hide();

    var uname = gsession.getSession("display_name");
    $("#user-name").val(uname);
    var MyAccountObj = new myAccount();

    MyAccountObj.load_profile_details();


});


$(document).on('click','.edit-my-profile',function(){
    
    $(".my-account-display").hide();
    $(".my-account-edit").show();

});



$(document).on('click','.save-my-profile',function(){
    
    $(".my-account-display").show();
    $(".my-account-edit").hide();

    var userName       = $("#user-name").val().trim();
    var userHash     = gsession.getSession("user_hash");

    if(userName){

        var MyAccountObj = new myAccount();
        MyAccountObj.changeUserDetails(userName,userHash);

    }

});


$(document).on('click','.change-my-password',function(){

    var currentPassword = $("#current-password").val().trim();
    var newPassword     = $("#new-password").val().trim();
    var confirmPassword = $("#confirm-password").val().trim();

    var userHash        = gsession.getSession("user_hash");

    if(!currentPassword || !newPassword || !confirmPassword){
        alert("Please fill the Password");
    }else{
        if(newPassword === confirmPassword){

            var MyAccountObj = new myAccount();
            MyAccountObj.reset_password(currentPassword,newPassword,userHash);
    
        }else{
    
            alert("Password Doesn't Match");
    
        }
    }

    

});



$(document).on('click','.change_restaurant_group_name',function(){

    var restaurant_group_name = $("#restaurant_group_name").val().trim();
    var userHash        = gsession.getSession("user_hash");

    if(!restaurant_group_name){

        alert("Please fill the Restaurant Group Name");
        
    }
    
    if(restaurant_group_name.length>25){

        alert("Name should be less than 25 characters");

    }
    else{

            var MyAccountObj = new myAccount();
            MyAccountObj.change_restaurant_group_name(restaurant_group_name,userHash);
    
    }

    

});


$(document).on('click','.logout-my-account',function(){
   
    document.location.href = "index.html";
    
});


$(document).on('click','.close-my-profile',function(){
    
    $(".my-account-display").show();
    $(".my-account-edit").hide();

});