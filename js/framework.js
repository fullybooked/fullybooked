var ajaxcall = {};
var response_logs = {};
/**
function to send logs to database
*/

/*   wrapper function to pass the data    */
ajaxcall.send_data = function(json_value, url_id, callback) {
  var json_data = JSON.stringify(json_value);

  var mapping = url_mapper(url_id);
  $.ajax({
    type: "POST",
    async: false,
    data: {
      myData: json_data
    },
    url: mapping
  }).done(function(result) {
    callback(result);
  });
};



response_logs.get_message = function(response_key, callback) {
  var error_log = {
    login_success: "you have logged in successfully!!!",
    login_failed: "login failed"
  };
  return error_log[response_key];
};

function url_mapper(page_id) {
  var pages = {
    user_registration: "model/user_registration.php",
    my_account: "model/my_account.php",
    add_restaurant: "model/add_restaurant.php",
    booking: "model/booking.php",
    dashboard: "model/dashboard.php",
    search_booking: "model/search_booking.php",
    manage_users: "model/manage_users.php",
    widget: "model/widget.php",
    table_allotment:"model/table_allotment.php",
    embed:"model/embed.php",
    sms_settings:"model/sms_settings.php",
  };

  return pages[page_id];
}

var gsession = {};

gsession.setSession = function(key, value) {
  if (value) {
    localStorage.setItem(key, value);
  }
};

gsession.getSession = function(key) {
  return localStorage.getItem(key);
};

gsession.clearSession = function(key) {
  localStorage.removeItem(key);
};

gsession.setTempSession = function(key, value) {
  if (value) {
    sessionStorage.setItem(key, value);
  }
};

gsession.getTempSession = function(key) {
  return sessionStorage.getItem(key);
};

gsession.clearTempSession = function(key) {
  sessionStorage.removeItem(key);
};


$(document).ready(function() {

  var url = window.location.href;
  //var page_name = "widget.html"
  //var non_session_page2 = "guest_booking_confirmation.html";
  //if(url.indexOf(page_name) === -1 || url.indexOf(non_session_page2) === -1 ){
    
    var page_name = $("body").attr("data-page_name");

    if (page_name != "no_session_page") {
      var user_hash = gsession.getSession("user_hash");
      if (!user_hash) {
          document.location.href = "index.html";
      }
    }

    var is_index_page = $("body").attr("data-index_page");
    
    if (is_index_page === "yes") {
      var user_hash = gsession.getSession("user_hash");
      if (user_hash) {
          document.location.href = "dashboard.html";
      }
    }

//}


  

});