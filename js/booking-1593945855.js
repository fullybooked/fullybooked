function booking_details(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'booking', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.add_new_booking = function(restaurant_group_id,booking_id,booking_type,restaurant_id,user_hash,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,language,restaurant_notify,restaurant_email,table_guest_count){


        var booking_data = {restaurant_group_id,booking_id,booking_type,table_guest_count,restaurant_id,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,restaurant_notify,user_hash,language,alergy_type:"none",customer_need:"none",country_code:"-",restaurant_email,type:"new_booking"};

        var response_data = this.send_data(booking_data);
    
        var parsed_data   = this.process_data(response_data);

        if(parsed_data == "true"){

            

            $("#booking_msg_modal").modal("show");
            $("html, body").animate({ scrollTop: 0 }, "slow");
          

            if(booking_type === "new_booking"){
                this.clear_bookign_form();
                $(".booking-msg-title").html("Booking added successfully!");

            }else{
               
                $(".booking-msg-title").html("Booking updated successfully!");

            }

           
        }else{

            alert("Something Wrong, try again");
        }



    }


    this.get_restaurant_link = function(booking_type){

        console.log(booking_type)
        var user_hash = gsession.getSession("user_hash");

        var restaurant_data = {user_hash,type:"get_restaurant"};

        var response_data = this.send_data(restaurant_data);
    
        var parsed_data   = this.process_data(response_data);

        this.add_restaurant_dropdown(parsed_data);
        this.set_restaurant_selected_id(booking_type);
        this.get_sms_settings();

    }

    this.get_sms_settings = function(){
        
        var user_hash = gsession.getSession("user_hash");

        var restaurant_data = {user_hash,type:"get_sms_settings"};

        var response_data = this.send_data(restaurant_data);
    
        var parsed_data   = this.process_data(response_data);

        this.handle_sms_settings(parsed_data["is_sms_enabled"]);

    }

    this.handle_sms_settings = function(is_sms_enabled){
        //console.log(is_sms_enabled)
        if(is_sms_enabled!=="true"){

            var user_role = gsession.getSession("user_role");

            if(user_role === "admin" || user_role === "owner" ){
                
                var label_content = "To Send SMS confirmation now, Please enable SMS in <a href='./sms.html'>Settings</a>";

            }else{
                
                var label_content = "To Send SMS confirmation now, Please ask your admin to enable the SMS in SMS Setup";

            }
            
            $("#send_sms_label").html(label_content);
            $("#send_sms").prop("disabled", true);
            
        }

    }

    this.add_restaurant_dropdown = function(restaurant_data){

        var temp = `<option selected value="" disabled>Select Restaurants</option>`;
       
        var restaurant_count = Object.keys(restaurant_data).length;
        
        for(let index=1; index <=restaurant_count; index++){

            
            var restaurant_id      = restaurant_data[index].restaurant_id;
            var restaurant_name    = restaurant_data[index].restaurant_name;
            var notification_email = restaurant_data[index].notification_email;
            var table_count        = restaurant_data[index].table_count;

            var minimum_group_size    = restaurant_data[index].minimum_group_size;
            var maximum_group_size    = restaurant_data[index].maximum_group_size;
            var restaurant_start_time = restaurant_data[index].restaurant_start_time;
            var restaurant_end_time   = restaurant_data[index].restaurant_end_time;
            var booking_interval      = restaurant_data[index].booking_interval;

            temp += `<option value =${restaurant_id} data-email=${notification_email} data-table_count=${table_count}  data-minimum_group_size=${minimum_group_size} data-maximum_group_size=${maximum_group_size} data-restaurant_start_time=${restaurant_start_time} data-restaurant_end_time=${restaurant_end_time} data-booking_interval=${booking_interval}>${restaurant_name}</option>`;

        }


        $("#restaurant_list_data").html(temp);

        if(restaurant_count === 1){

            $("#restaurant_list_data").val(restaurant_id);
            
        }

    }

    this.set_restaurant_selected_id = function(booking_type){

        if(booking_type === "new_booking"){

            var selected_restaurant_id = this.get_restaurant_id();
            $("#restaurant_list_data").val(selected_restaurant_id);

        }else if(booking_type === "new_booking_select_restaurant"){

            //var selected_restaurant_id = this.get_restaurant_id();
            //$("#restaurant_list_data").val(selected_restaurant_id);

        }else{

            // get restaurant id for current booking
            $("#restaurant_list_data").val(booking_type);

        }
        

        /*if(selected_restaurant_id != null){

            $("#restaurant_list_data").val(selected_restaurant_id);
            console.log("inside")
        }else{
            console.log("outside")
        }*/
        
        //$('#restaurant_list_data').val(selected_restaurant_id);

        var minimum_group_size      = $("#restaurant_list_data option:selected").attr("data-minimum_group_size");
        var maximum_group_size      = $("#restaurant_list_data option:selected").attr("data-maximum_group_size");
        console.log(minimum_group_size)
        console.log(maximum_group_size)
        if(!minimum_group_size){
            minimum_group_size = "-";
        }
        if(!maximum_group_size){
            maximum_group_size = "-";
        }

        $(".guest_count_limit").text("Min Guest Count: "+minimum_group_size+", Max Guest Count: "+maximum_group_size);


        var start_time       = $("#restaurant_list_data option:selected").attr("data-restaurant_start_time");
        var end_time         = $("#restaurant_list_data option:selected").attr("data-restaurant_end_time");
        var booking_interval = $("#restaurant_list_data option:selected").attr("data-booking_interval");

        booking_interval = Number(booking_interval);


        $('#booking_time').timepicker({
            timeFormat: 'HH:mm',
            interval: booking_interval,
            defaultTime: start_time,
            minTime: start_time,
            maxTime: end_time
        });

    }



    this.clear_bookign_form = function(){

        $("#restaurant_list_data").val("");
        $("#booking_date").val("");
        $("#booking_time").val("");
        $("#guest-name").val("");
        $("#guest-count").val("");
        $("#email-address").val("");
        $("#phone-number").val("");
        $("#additional-notes").val("");

        $('#send_email').prop('checked', false); 
        $('#send_sms').prop('checked', false); 
        $('#send_advance_sms').prop('checked', false); 
        $('#restaurant_notify').prop('checked', false); 

    }


    this.find_booking_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");

        return id;

    }

    this.get_booking_data_by_id = function(booking_id){

        var user_hash  = gsession.getSession("user_hash");

        var data = {booking_id,user_hash,type:"get_booking_data_for_id"};

        var response_data = this.send_data(data);
    
        var parsed_data   = this.process_data(response_data);

        //this.process_edit_booking_data(parsed_data);

        return parsed_data;


    }


    this.process_edit_booking_data = function(data){


            var selected_restaurant_id         = data.restaurant_id;
            var booking_date                   = data.booking_date;
            var booking_time                   = data.booking_time;
            var status                         = data.status;
            var assigned_tables                = data.assigned_tables;
            var table_name                     = data.table_name;
            var guest_name                     = data.guest_name;
            var guest_count                    = data.guest_count;
            var email_address                  = data.email_address;
            var phone_number                   = data.phone_number;
            var comments                       = data.comments;
            var email_confirmation_status      = data.email_confirmation_status;
            var sms_confirmation_status        = data.sms_confirmation_status;
            var email_confirmation_status      = data.email_confirmation_status;
            var sms_reminder                   = data.sms_reminder;
            var restaurant_notification_status = data.restaurant_notification_status;
         
           $("#restaurant_list_data").val(selected_restaurant_id);


            $(".booking-date").val(booking_date);
            $("#booking_time").val(booking_time);

            $('.booking-date').datepicker({
                setDate: booking_date,
            // setDate:"03/09/2020"
            });

            var restaurant_start_time = $("#restaurant_list_data option:selected").attr("data-restaurant_start_time");
            var restaurant_end_time   = $("#restaurant_list_data option:selected").attr("data-restaurant_end_time");
            var booking_interval      = $("#restaurant_list_data option:selected").attr("data-booking_interval");

            booking_interval = Number(booking_interval);
            // restaurant_start_time = Number(restaurant_start_time);
            // restaurant_end_time = Number(restaurant_end_time);

            console.log(restaurant_start_time);
            console.log(restaurant_end_time);
    
            
            $('#booking_time').timepicker({
                timeFormat: 'HH:mm',
                interval: booking_interval,
                minTime: restaurant_start_time,
                maxTime: restaurant_end_time,
                defaultTime: booking_time
            });
    

            $("#status").val(status);
            $("#assigned-table").val(assigned_tables);
            $("#guest-name").val(guest_name);
            $("#guest-count").val(guest_count);
            $("#additional-notes").val(comments);
            $("#email-address").val(email_address);
            $("#phone-number").val(phone_number);

            
            email_confirmation_status == 1? $("#send_email").prop("checked"):"";
            sms_confirmation_status == 1? $("#send_sms").prop("checked"):"";
            sms_reminder == 1? $("#send_advance_sms").prop("checked"):"";
            restaurant_notification_status == 1? $("#restaurant_notify").prop("checked"):"";
        
        

    }

   

    this.is_valid_email = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    this.is_valid_phone_number = function(phone_number) {
        var phoneNum = phone_number.replace(/[^\d]/g, '');
        if(phoneNum.length > 6 && phoneNum.length <= 13) 
        {  
            return true;  
        }else{
            return "failed";
        }

    }


    this.check_guest_count_limit = function(guest_count){

        var minimum_group_size      = $("#restaurant_list_data option:selected").attr("data-minimum_group_size");
        var maximum_group_size      = $("#restaurant_list_data option:selected").attr("data-maximum_group_size");
        guest_count = parseInt(guest_count);

        if((guest_count >= minimum_group_size) && (guest_count <= maximum_group_size)){
            return true;
        }else{
            return false;
        }

    }

    

    this.get_restaurant_id = function(){

        var url_string    = window.location.href;
        var url           = new URL(url_string);
        var restaurant_id = url.searchParams.get("resid");
  
        return restaurant_id;
  
    }

    this.check_type_of_page_request = function(){

        var url_string    = window.location.href;
        var url           = new URL(url_string);
        var restaurant_id = url.searchParams.get("resid");
        if(restaurant_id != null){
            return "new_booking";
        }


        var booking_id = url.searchParams.get("id");
        if(booking_id != null){
            return "modify_booking";
        }else{
            return "new_booking_select_restaurant";
        }
  
    }


    this.on_restaurant_change_activities = function(){

        var restaurant_email = $("#restaurant_list_data option:selected").attr("data-email");
        var restaurant_name  = $("#restaurant_list_data option:selected").text();
        var table_count      = $("#restaurant_list_data option:selected").attr("data-table_count");

        var minimum_group_size      = $("#restaurant_list_data option:selected").attr("data-minimum_group_size");
        var maximum_group_size      = $("#restaurant_list_data option:selected").attr("data-maximum_group_size");

        $(".guest_count_limit").text("Min Guest Count: "+minimum_group_size+", Max Guest Count: "+maximum_group_size);

        $(".restaurant_error").html(" ");
        $(".add-new-booking").prop("disabled", false);

        if(table_count == "0"){

            

            $(".restaurant_error").html(`Tables yet to be assigned in ${restaurant_name}.`);

            $(".add-new-booking").prop("disabled", true);
        }

        $(".notification_email").html(restaurant_email);

        var restaurant_id = $("#restaurant_list_data").val();

        
        if(restaurant_id){
            window.location = "./new_booking.html?resid="+restaurant_id;
        }
        //var booking_details_obj = new booking_details();

        //this.get_booking_timing(restaurant_id);

        var start_time      = $("#restaurant_list_data option:selected").attr("data-restaurant_start_time");
        var end_time      = $("#restaurant_list_data option:selected").attr("data-restaurant_end_time");

        var booking_interval = $("#restaurant_list_data option:selected").attr("data-booking_interval");

        booking_interval = Number(booking_interval);
        
        $('#booking_time').timepicker({
            timeFormat: 'HH:mm',
            interval: booking_interval,
            defaultTime: start_time,
            minTime: start_time,
            maxTime: end_time
        });
        

    }


}




$(document).ready(function() {
   
 
    var booking_details_obj = new booking_details();
    
    // check whether it is new booking request or modify booking request
    var booking_type = booking_details_obj.check_type_of_page_request();

    if(booking_type === "new_booking"){ 

        booking_details_obj.get_restaurant_link(booking_type);

    }else if(booking_type === "modify_booking"){

        var booking_id = booking_details_obj.find_booking_id();

        if(booking_id !== null){

            $("#restaurant_list_data").prop("disabled", true); 
            
            /* $("#booking_time").attr("disabled", "disabled"); 
            $("#booking_date").attr("disabled", "disabled"); */

            $("#status").append('<option value="cancelled">Cancelled</option><option value="arrived">Arrived</option><option value="no-show">No-show</option>');
            
            var booking_id_data = booking_details_obj.get_booking_data_by_id(booking_id);
            booking_details_obj.get_restaurant_link(booking_id_data.restaurant_id);
            booking_details_obj.process_edit_booking_data(booking_id_data);
            
        }

    }else{
        booking_details_obj.get_restaurant_link(booking_type);
    }

    

    $(".add-new-booking").prop("disabled", false);



    $('.datepicker').datepicker({
        todayHighlight: true,
        autoclose: true,
        minDate: new Date(), 
        orientation: "bottom" ,
        //selectMultiple:true
    });

    $('.datepicker').datepicker('option', {dateFormat: 'd MM yy'});

    hideOldDays();
    hideNewDays();


    var start_time = "00:00";

    $('#booking_time').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        defaultTime: start_time
    });

        
    

    $("#booking_msg_modal").modal("hide");


});



function hideOldDays(){

    var x = $('.datepicker .datepicker-days table tr td.old');
    if(x.length > 0){
        x.css('visibility', 'hidden');
        if(x.length === 7){
            x.parent().hide();
        }
    }

}



function hideNewDays(){

    var x = $('.datepicker .datepicker-days tr td.new');
    if(x.length > 0){
        x.hide();
    }

}




$(document).on('click','#go_back_btn',function(){
    window.history.back();
});

$(document).on('click','.add-new-booking',function(){
    
    var restaurant_id       = $("#restaurant_list_data option:selected").val();
    var booking_date        = $("#booking_date").val();
    var booking_time        = $("#booking_time").val();
    var status              = $("#status").val();
    var assigned_table      = $("#assigned-table").val();
    var guest_name          = $("#guest-name").val();
    var guest_count         = $("#guest-count").val();
    var email_address       = $("#email-address").val();
    var phone_number        = $("#phone-number").val();
    var additional_notes    = $("#additional-notes").val();
    var send_email          = $("#send_email").is(':checked'); 
     var send_sms         = $("#send_sms").is(':checked'); 
    // var send_advance_sms = $("#send_advance_sms").is(':checked'); 
    var restaurant_notify   = $("#restaurant_notify").is(':checked'); 
    var language            = $("#language").val();
    var user_hash           = gsession.getSession("user_hash");
    var restaurant_group_id = gsession.getSession("restaurant_group_id");
    var restaurant_email    = $("#restaurant_list_data option:selected").attr("data-email");
    var table_guest_count   = guest_count;



    var currentDate = new Date();
    var currentHour = currentDate.getHours();
    var currentMinute = currentDate.getMinutes();
    var currentSeconds = currentDate.getSeconds();


    //booking_date = new Date(booking_date);

    //booking_date.setHours(currentHour,currentMinute,currentSeconds);

    
  
    var booking_details_obj = new booking_details();

    // validation area

    var mandatory_field_status = "passed";
    $( ".mandatory_field" ).each(function() {

        var user_input = $( this ).val();
        if(!user_input){
            mandatory_field_status = "failed";
            $(this).addClass("validation_border_error");
        }
    
    });

    var email_status = booking_details_obj.is_valid_email(email_address);

    if(email_status === false){

        mandatory_field_status === "failed";
    }


    var guest_count_limit = booking_details_obj.check_guest_count_limit(guest_count);

    if(guest_count_limit === false){
        $("#guest-count").addClass("validation_border_error");
        mandatory_field_status === "failed";
    }




    if(mandatory_field_status === "failed"){

        if(email_status === false){

            $(".email_validation_error").show();

        }


        $(".validation_error").html("Please fill mandatory fields");
        $(".booking_completed").html("");

    }else{
        
        $(".validation_error").html("");
       // $(".booking_completed").html("New booking added");
        var booking_id = booking_details_obj.find_booking_id();
        var booking_type = "update_booking";

        if(booking_id === null){

            var booking_id  = "yet_to_generate";
            var booking_type = "new_booking";
        }

        send_email        = send_email == true ? 'true' : 'false';
        send_sms          = send_sms == true ? 'true' : 'false';
        // send_advance_sms  = send_advance_sms == true ? 'true' : 'false';
        restaurant_notify = restaurant_notify == true ? 'true' : 'false';

        //var send_sms = 'false';
        var send_advance_sms = 'false';

        booking_details_obj.add_new_booking(restaurant_group_id,booking_id,booking_type,restaurant_id,user_hash,booking_date,booking_time,status,assigned_table,guest_name,guest_count,email_address,phone_number,additional_notes,send_email,send_sms,send_advance_sms,language,restaurant_notify,restaurant_email,table_guest_count);


    }


    
});



$(document).on('change','#restaurant_list_data',function(){
    
    var booking_details_obj = new booking_details();
    booking_details_obj.on_restaurant_change_activities();

});



$(document).on('change','.mandatory_field',function(){
    
    $(this).removeClass("validation_border_error");
    //$(".validation_error").html("");

});

$(document).on('change','.booking-input',function(){
    
    $(".booking_completed").html("");

});

$(document).on('keypress','#email-address',function(){
    
    $(".email_validation_error").hide();

});



$(document).on('keypress','#phone-number',function(evt){
    
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

});



$(document).on('keypress','#guest-name',function(evt){
    
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode >= 48 && charCode <= 57) {
        evt.preventDefault();
    }

});



$(document).on('blur','#email-address',function(evt){
    var email_address = $("#email-address").val();
    var booking_details_obj = new booking_details();
    var email_status = booking_details_obj.is_valid_email(email_address);

    if(email_status === false){

        $(this).addClass("validation_border_error");
        $(".email_validation_error").show();

    }else{
        $(this).removeClass("validation_border_error");
        $(".email_validation_error").hide();
    }
    

});


$(document).on('blur','#phone-number',function(evt){
    
    var phone_number = $("#phone-number").val();
    var booking_details_obj = new booking_details();
    var phone_number_status = booking_details_obj.is_valid_phone_number(phone_number);

    if(phone_number_status === "failed"){

        $(this).addClass("validation_border_error");
        $(".phno_validation_error").addClass("validation_error");

    }else{
        $(this).removeClass("validation_border_error");
        $(".phno_validation_error").removeClass("validation_error");
    }

});




$(document).on('blur','#guest-count',function(evt){
    
    var guest_count = parseInt($("#guest-count").val()) ;
    
    var minimum_group_size      = parseInt( $("#restaurant_list_data option:selected").attr("data-minimum_group_size") );
    var maximum_group_size      = parseInt( $("#restaurant_list_data option:selected").attr("data-maximum_group_size") );

    if(guest_count<minimum_group_size || guest_count>maximum_group_size){
        $(this).addClass("validation_border_error");
        $(".guest_count_limit").addClass("validation_error");
    }else{
        $(this).removeClass("validation_border_error");
        $(".guest_count_limit").removeClass("validation_error");
    }


});


$(document).on('blur','#guest-name',function(evt){
    
    var guest_name = $("#guest-name").val().trim();
    var name_length = guest_name.length;

    if(name_length<3 || name_length>50){
        $(this).addClass("validation_border_error");
        $(".name_validation_error").show();
    }else{
        $(this).removeClass("validation_border_error");
        $(".name_validation_error").hide();
    }


});