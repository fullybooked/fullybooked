
function embed_details(){


    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'embed', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }

    this.onload = function(){

        var user_hash = gsession.getSession("user_hash");

        var embed_data = {user_hash,type:"get_group_data"};

        var response_data = this.send_data(embed_data);
    
        var parsed_data   = this.process_data(response_data);

        
        
        
        console.log(parsed_data["restaurant_count"]);
        var restaurants_count = parsed_data["restaurant_count"];
        var dropdown_html = "<option >Select Restaurant</option>";

        for(var flag=1; flag<= restaurants_count; flag++){
            
            var restaurant_id = parsed_data[flag]["restaurant_id"];
            var restaurant_name = parsed_data[flag]["restaurant_name"];
            var restaurant_widget_status = parsed_data[flag]["booking_widget_status"];
            var restaurant_group_id = parsed_data["restaurant_group_id"];
            
            dropdown_html += "<option value="+restaurant_id+" data-widget_status="+restaurant_widget_status +" data-restaurant_group_id="+restaurant_group_id +" > "+restaurant_name+" </option>";

        }

        $("#restaurant_list_data").html(dropdown_html);
        
        

    }



}




$(document).ready(function() {


    var embed_obj = new embed_details();

    embed_obj.onload();

});



$(document).on('click', '.copy-iframe-button', function (event) {
    
    var range = document.createRange();
    range.selectNode(document.getElementById("iframe-link")); //changed here
    window.getSelection().removeAllRanges(); 
    window.getSelection().addRange(range); 
    document.execCommand("copy");
    window.getSelection().removeAllRanges();
    alert("URL copied to clipboard");
   
});

$(document).on('click', '.copy-link-button', function (event) {
    
    var range = document.createRange();
    range.selectNode(document.getElementById("widget-link")); //changed here
    window.getSelection().removeAllRanges(); 
    window.getSelection().addRange(range); 
    document.execCommand("copy");
    window.getSelection().removeAllRanges();
    alert("URL copied to clipboard");
   
});


$(document).on('click', '.copy-html-button', function (event) {
    
    var range = document.createRange();
    range.selectNode(document.getElementById("html-link")); //changed here
    window.getSelection().removeAllRanges(); 
    window.getSelection().addRange(range); 
    document.execCommand("copy");
    window.getSelection().removeAllRanges();
    alert("URL copied to clipboard");
   
});





$(document).on('change','#restaurant_list_data',function(){
    
    //var booking_details_obj = new booking_details();
    //booking_details_obj.on_restaurant_change_activities();
    var restaurant_id = $("#restaurant_list_data").val();
    var widget_status = $("#restaurant_list_data option:selected").attr("data-widget_status");
    var restaurant_group_id = $("#restaurant_list_data option:selected").attr("data-restaurant_group_id");

    var restaurant_url_params = restaurant_group_id + "&id1="+restaurant_id;

    var url      = window.location.href;    

    if(widget_status === "true" ){ 

        
        var widget_file = "widget.html";        
        url = url.replace(url.substring(url.lastIndexOf('/')+1), widget_file);


        $("#html-link").html(`${url}?id=${restaurant_url_params}`);

        $("#widget-link").html(`&lt;a href="${url}?id=${restaurant_url_params}" target="_blank"&gt;Book a table&lt;/a&gt;`);

        $("#iframe-link").html(`&lt;iframe src="${url}?id=${restaurant_url_params}" width="400" height="1400" frameborder="0"&gt;&lt;/iframe&gt;`);

            
    }else{
        var error_msg = "You haven't enabled online booking for this restaurant. Please go to restaurant setting to enable.";

        $("#html-link").html(error_msg);

        $("#widget-link").html(error_msg);

        $("#iframe-link").html(error_msg);

    }

});
