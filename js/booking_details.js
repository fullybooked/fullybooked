function booking_information(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'booking', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.find_booking_id = function(){

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");

        return id;

    }

    this.get_booking_data_by_id = function(booking_id){

        var user_hash  = gsession.getSession("user_hash");

        var data = {booking_id,user_hash,type:"get_booking_data_for_id"};

        var response_data = this.send_data(data);
    
        var parsed_data   = this.process_data(response_data);

        this.display_booking_data(parsed_data);


    }

    this.display_booking_data = function(data){

        var booking_date                   = data.booking_date;
        var booking_time                   = data.booking_time;
        var status                         = data.status;
        var assigned_tables                = data.assigned_tables;
        var guest_name                     = data.guest_name;
        var guest_count                    = data.guest_count;
        var restaurant_name                = data.restaurant_name;
        var user_name                      = data.user_name;
        var alergy_type                    = data.alergy_type;
        var customer_need                  = data.customer_need;
        var added_at                       = data.added_at;
        var language                       = data.language;
   
        var guest_count =  guest_count == 1? "1 guest":guest_count+" guests";
        // alergy_type =  alergy_type == ""? "None":alergy_type;
        // customer_need =  customer_need == ""? "None":customer_need;

        $(".guest_name").text(guest_name);
        $(".guest_count").text(guest_count);
        $(".restaurant_name").text(restaurant_name);
        $(".bookign_date").text(booking_date +" "+ booking_time);
        $(".booking_status").text(status);
        $(".booking_type").text(assigned_tables);
        //$(".created_at").text(added_at);
        $(".created_by").text("Booking created at "+added_at);
        $(".alergy_type").text(alergy_type);
        $(".other_need").text(customer_need);
        $(".booking_language").text(language);

    }


}



$(document).ready(function() {

    var booking_info_obj = new booking_information();
    
    var booking_id = booking_info_obj.find_booking_id();

    if(booking_id){

        booking_info_obj.get_booking_data_by_id(booking_id);

        $(".edit-booking").attr("data-booking_id",booking_id);
    
    }


});


$(document).on('click','.edit-booking',function(){
    
    var booking_id = $(this).attr("data-booking_id");

    window.location.href = "new_booking.html?id="+booking_id;


});