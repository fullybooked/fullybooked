function dashboard_data_summary(){

    this.send_data = function(user_data){

        var response_data='';
        
            ajaxcall.send_data(user_data, 'dashboard', function(data) {
                
            response_data = data;
            
            });
            
        return response_data;
    }


    this.process_data = function(data) {

        var data        = JSON.parse(data);
        return data;
    }


    this.load_summary_data = function(){

        var user_hash = gsession.getSession("user_hash");
        var user_details = {user_hash, "type":"load_dashboard_summary"}

        var response_data = this.send_data(user_details);
    
        var parsed_data   = this.process_data(response_data);

        this.generate_restaurant_summary(parsed_data);

        return parsed_data;

    }

    this.load_graph_data = function(processed_data, status){

        var data = [];
        var this_week_data = parseInt(processed_data["this_week"][status]);
        if(!this_week_data){
            this_week_data = 0;
        }
        var next_week_data = parseInt(processed_data["next_week"][status]);
        if(!next_week_data){
            next_week_data = 0;
        }
        var last_week_data = parseInt(processed_data["last_week"][status]);
        if(!last_week_data){
            last_week_data = 0;
        }
        data = [last_week_data, this_week_data, next_week_data];

        return data;

    }

    this.load_latest_bookings = function(processed_data){

        var bookings_count = processed_data["latest_bookings"]["bookings_count"];
        var latest_bookings_html = "";
        if(bookings_count===0){
            latest_bookings_html = "<br><br><br><p style='text-align:center'>No recent bookings</p>";
        }
        for(let flag = 1; flag<=bookings_count; flag++){
            var booking_date = processed_data["latest_bookings"]["booking_date_"+flag];
            var booking_time = processed_data["latest_bookings"]["booking_time_"+flag];
            var booking_id = processed_data["latest_bookings"]["booking_id_"+flag];
            var guest_count = processed_data["latest_bookings"]["guest_count_"+flag];
            var guest_name = processed_data["latest_bookings"]["guest_name_"+flag];
            var restaurant_name = processed_data["latest_bookings"]["restaurant_name_"+flag];
            var date_difference = processed_data["latest_bookings"]["date_difference_"+flag];
            var date_diff_text = "";
            if(date_difference === 0 ){
                var time_difference = processed_data["latest_bookings"]["time_difference_"+flag];
                date_diff_text = time_difference;
            }else{
                date_diff_text = date_difference+' days ago';
            }

            latest_bookings_html += '<div class="booking_list">';
            latest_bookings_html += '<p class="created_text">Received booking '+date_diff_text+'</p>';
            latest_bookings_html += '<a href="./new_booking.html?id='+booking_id+'" class="booking_link">'+booking_date+' '+booking_time+'(slot) - '+guest_name+'</a>';
            latest_bookings_html += '<p class="guests_text">for '+guest_count+' guests at '+restaurant_name+'</p>';
            latest_bookings_html += '</div>';
        }

        $(".latest_booking_area").append(latest_bookings_html);
    }

    this.get_restaurant_group_name = function(response_data){
        var restaurant_group_name = response_data["restaurant_group_name"];
        return restaurant_group_name;
    }

    this.generate_restaurant_summary = function(summary_data){

    var available_restaurant_count = summary_data["restaurant_count"];
    
    if(available_restaurant_count>=1){

            var restaurant_ui_template= "";
    
            for (var flag=1;flag<=available_restaurant_count;flag++){

                restaurant_name_label = summary_data["restaurant_"+flag];
                restaurant_ui_template += `<div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2 go_to_booking_report" >
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800 ">`+restaurant_name_label+`</div>
                        <div class="row no-gutters align-items-center restaurant_bookings_count">
                        <div class="col-auto"> `;

                        restaurant_ui_template += `  <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> OPEN - ACCEPTING BOOKINGS </div> `;
                        

                        var restaurant_bookings_count = parseInt(summary_data["restaurant_booking_"+flag]);
                        var restaurant_guests_count = parseInt(summary_data["restaurant_guests_"+flag]);

                        var restaurant_table_status = summary_data["restaurant_table_"+flag];

                        var restaurant_guests_count   = parseInt(summary_data["restaurant_guests_"+flag]);
                        var restaurant_id             = summary_data["restaurant_id_"+flag];
                        if(!restaurant_guests_count){
                            restaurant_guests_count = 0;
                        }
                        restaurant_ui_template += ` </div>
                        
                        <div class="col-auto">
                    
                            
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">`+restaurant_bookings_count+` Bookings (`+(restaurant_guests_count)+` guests) </div>  </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                    </div>


                    <div style="text-align:center">`;
                        if(restaurant_table_status === "true"){

                            restaurant_ui_template += `<button class="btn btn-primary btn-icon-split walkin_btn" data-toggle="modal"  data-restaurant_name="`+restaurant_name_label+`" data-restaurant_id="`+restaurant_id+`">
                            <span class="icon text-white-50">
                            <i class="fas fa-plus-circle"></i>
                            </span>
                            <span class="text">Walk In</span>
                            </button>`;

                        }else{

                            restaurant_ui_template += `<a href="./table_allotment.html?id=`+restaurant_id+`" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                            <i class="fas fa-plus-circle"></i>
                            </span>
                            <span class="text">Allot Table</span>
                            </a>`;

                        }
                    

                    restaurant_ui_template += `</div>

                    
                </div>
                </div>
            </div> `;
                
            }
            $("#restaurant_booking_info").html(restaurant_ui_template);

            
    }

    }

    this.manage_quick_links = function(){

        var user_role = gsession.getSession("user_role");
        var quick_text = "";

        if(user_role === "admin" || user_role === "owner"){
            
            quick_text += '<i class="fas fa-fw fa-hotel"></i> <a href="./add_restaurant.html"> Add restaurant </a> <br>';
           
        }

        quick_text += '<i class="fas fa-fw fa-plus"></i> <a href="./new_booking.html"> Add booking to restaurant</a> <br>';

        quick_text += '<i class="fas fa-fw fa-search"></i> <a href="./search.html"> Search bookings in restaurant</a> <br>';


        $("#quick_links_area").html(quick_text);

    }


    this.walkin_booking_call = function(walkin_booking_details){

        var response_data = this.send_data(walkin_booking_details);
    
        var parsed_data   = this.process_data(response_data);

        //this.generate_restaurant_summary(parsed_data);
        if(parsed_data === "success"){

            $.unblockUI();
            location.reload()


        }else{

            $.unblockUI();
            alert("Error occured.");
            
        }

    }

    
}

$(document).ready(function() {

    var dashboard_data_summary_obj = new dashboard_data_summary();

    var result_data = dashboard_data_summary_obj.load_summary_data();

    var received_data = dashboard_data_summary_obj.load_graph_data(result_data, "received");
    
    

    var confirmed_data = dashboard_data_summary_obj.load_graph_data(result_data, "confirmed");

    
    var arrived_data = dashboard_data_summary_obj.load_graph_data(result_data, "arrived");

    var no_show_data = dashboard_data_summary_obj.load_graph_data(result_data, "no-show");

    var cancelled_data = dashboard_data_summary_obj.load_graph_data(result_data, "cancelled");

    dashboard_data_summary_obj.load_latest_bookings(result_data);

    dashboard_data_summary_obj.manage_quick_links();


    var restaurant_group_name = dashboard_data_summary_obj.get_restaurant_group_name(result_data);


    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: restaurant_group_name
        },
        subtitle: {
            text: 'All Restaurants (Guest Count)'
        },
        xAxis: {
            categories: [
                
                'Last week',
                'This week',
                'Next week'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bookings'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Received',
            data: received_data
    
        }, {
            name: 'Confirmed',
            data: confirmed_data
    
        }, {
            name: 'Arrived',
            data: arrived_data
    
        }, {
            name: 'No-Show',
            data: no_show_data
    
        },
        {
            name: 'Cancelled',
            data: cancelled_data
    
        }]
    });

});




$(document).on('click','.go_to_booking_report',function(){
    
    //window.location = "bookings_report.html";

    //window.location = "new_booking.html";

});





$(document).on('click','.walkin_btn',function(){
    
    $("#walkin_booking_modal").modal("show");
    var restaurant_name = $(this).attr("data-restaurant_name");
    var restaurant_id = $(this).attr("data-restaurant_id");
    $("#walkin_booking_modal_title").text("Walk In Booking - "+restaurant_name);
    $("#walkin_booking_confirm").attr("data-restaurant_id",restaurant_id);

});


$(document).on('click','#walkin_booking_confirm',function(){
    
    //$("#walkin_booking_modal").modal("hide");
    var walkin_guest_count = $("#walkin_guest_count").val();
    var restaurant_id = $(this).attr("data-restaurant_id");

    //alert(walkin_guest_count + " - " + restaurant_id);

        var user_hash = gsession.getSession("user_hash");
        var walkin_booking_details = { walkin_guest_count, restaurant_id, user_hash,  "type":"walkin_booking" }
        $.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 

        var dashboard_data_summary_obj = new dashboard_data_summary();

        dashboard_data_summary_obj.walkin_booking_call(walkin_booking_details);

        
    

});

