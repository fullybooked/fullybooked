function table_allotment(){

            /** User input validation */
            this.user_input_validation = function(){

                var validation_result = "pass";
                $( ".restaurant_textbox" ).each(function( index ) {
                    if(! $( this ).val()){
                        validation_result = "fail";
                        $(this).addClass("validation_border_error");
                    }
                });
                return validation_result;

            }

            this.send_data = function(user_data){

                var response_data='';
                
                    ajaxcall.send_data(user_data, 'table_allotment', function(data) {
                        
                    response_data = data;
                    
                    });
                    
                return response_data;
            }


            this.process_data = function(data) {

                var data        = JSON.parse(data);
                return data;
            }


            this.list_tables = function(){

                var restaurant_id = this.get_restaurant_id();
                var user_hash = gsession.getSession("user_hash");

                var table_details = {restaurant_id, user_hash, type:"load_tables"}

                var response_data = this.send_data(table_details);
            
                var parsed_data   = this.process_data(response_data);

                var tables_count = parsed_data["total_tables"];

                var html_text = "";

                for(var flag=1; flag<=tables_count; flag++){

                    var table_id = parsed_data["table_id_"+flag];
                    var guest_count = parsed_data["guest_count_"+flag];

                    html_text+= '<div class="table_list" id="table_no_'+flag+'">';
                    html_text+= '<div class="col-sm-6 mb-6 mb-sm-0">';
                    html_text+= '<label for="restaurant_name" class="restaurant_label">Table '+flag+'</label>';
                    html_text+= '<a href="#" data-table_no="'+flag+'" class="delete_table">Delete</a>';
                    html_text+= '</div>';
      
                    html_text+= '<div class="input-group mb-3 col-sm-6 mb-6 mb-sm-0">';
                    html_text+= '<input type="number" min="1" max="20" class="form-control form-control-user restaurant_table_entry" data-table_id="'+table_id+'" data-table_name="table_'+flag+'" id="table_guest_count_'+flag+'" value="'+guest_count+'" aria-describedby="basic-addon2">';
                    html_text+= '<div class="input-group-append">';
                    html_text+= '<span class="input-group-text" id="basic-addon2">seats</span>';
                    html_text+= '</div>';
                    html_text+= '</div>';
                    html_text+= '</div><br>';


                }

                $('#add_new_table').data('total_tables',tables_count); 

                
                $("#tables_area").html(html_text)
        
        
            }

            this.add_new_table = function(new_table_count){

                    var html_text="";
                    html_text+= '<div class="table_list" id="table_no_'+new_table_count+'">';
                    html_text+= '<div class="col-sm-6 mb-6 mb-sm-0">';
                    html_text+= '<label for="restaurant_name" class="restaurant_label">Table '+new_table_count+'</label>';
                    html_text+= '<a href="#" data-table_id="'+new_table_count+'" class="delete_table">Delete</a>';
                    html_text+= '</div>';
      
                    html_text+= '<div class="input-group mb-3 col-sm-6 mb-6 mb-sm-0">';
                    html_text+= '<input type="number" min="1" max="20" class="form-control form-control-user restaurant_table_entry"  data-table_id="new_table" data-table_name="table_'+new_table_count+'" id="table_guest_count_'+new_table_count+'" aria-describedby="basic-addon2">';
                    html_text+= '<div class="input-group-append">';
                    html_text+= '<span class="input-group-text" id="basic-addon2">seats</span>';
                    html_text+= '</div>';
                    html_text+= '</div>';
                    html_text+= '</div><br>';

                    $("#tables_area").append(html_text);

            }
            


            this.get_restaurant_id = function(){
                var url_string = window.location.href;
                var url = new URL(url_string);
                var restaurant_id = url.searchParams.get("id");
                return restaurant_id;
            }


            this.get_table_data = function(){

                
                var table_data_array = {};
                
                var table_count = 0;
                $(".restaurant_table_entry").each(function(){
                    table_count = table_count+1;
                    var table_id = $(this).data('table_id');
                    var guest_count = $(this).val();
                    var table_name = $(this).data('table_name');

                    table_data_array["table_id_"+table_count] = table_id;
                    table_data_array["guest_count_"+table_count] = guest_count;
                    table_data_array["table_name_"+table_count] = table_name;
                });

                table_data_array["total_tables"] = table_count;
                var user_hash = gsession.getSession("user_hash");
                var restaurant_id = this.get_restaurant_id();
                
                
                var table_details = {};
                table_details["table_data_array"] = table_data_array;
                table_details["user_hash"] = user_hash;
                table_details["restaurant_id"] = restaurant_id;
                table_details["type"] = "update_table";
               
                var response_data = this.send_data(table_details);
            
                var parsed_data   = this.process_data(response_data);

                var response = parsed_data["response"];
                
                if(response === "inserted" || response === "updated" ){
                    $(".seat_allocation_response").html("Seats modified successfully!").addClass("success_response").removeClass("fail_response");
                    var table_count = parsed_data["numbers"]["table_count"];
                    var seat_count = parsed_data["numbers"]["seat_count"];
                    $("#table_count").text(table_count);
                    $("#seat_count").text(seat_count);
                    $("#table_allotment_modal").modal("hide");
                }else{
                    $(".seat_allocation_response").html("Sorry error occured, Try again.").addClass("fail_response").removeClass("success_response");;
                }
            }

            this.load_table_summary = function(){

                var table_details = {};
                var user_hash = gsession.getSession("user_hash");
                var restaurant_id = this.get_restaurant_id();

                table_details["user_hash"] = user_hash;
                table_details["restaurant_id"] = restaurant_id;
                table_details["type"] = "load_tables_summary";
               
                var response_data = this.send_data(table_details);
            
                var parsed_data   = this.process_data(response_data);

                var table_count = parsed_data["table_count"];
                var seat_count = parsed_data["seat_count"];

                $("#seat_count").text(seat_count);
                $("#table_count").text(table_count);
                

            }



}

$(document).on('click','#edit_table_allotment',function(){
   
    var table_allotment_obj = new table_allotment();
    table_allotment_obj.list_tables();
    $(".seat_allocation_response").html("");
    $("#table_allotment_modal").modal("show");


});

$(document).on('click','#add_new_table',function(){

    var new_table_count = parseInt($('#add_new_table').data('total_tables'));
    new_table_count = new_table_count+1;
    var table_allotment_obj = new table_allotment();
    $('#add_new_table').data('total_tables',new_table_count); 
    table_allotment_obj.add_new_table(new_table_count);

});


$(document).on('click','#save_table',function(){

    var table_allotment_obj = new table_allotment();
    var table_data = table_allotment_obj.get_table_data();

});


$(document).on('click','.delete_table',function(){

    var delete_position = $(this).data('table_no');
    $("#table_no_"+delete_position).remove();

});

$(document).on('click','#get_widget_link',function(){
    
    //window.history.back();
    window.location = "./embed.html";

});


$(document).ready(function() {


    var user_role = gsession.getSession("user_role");
    if(user_role === "owner" || user_role === "admin" || user_role === "manager"){
        var table_allotment_obj = new table_allotment();
        var restaurant_id = table_allotment_obj.get_restaurant_id();
        if(!restaurant_id){
            alert("invalid");
            window.location = "./dashboard.html";
        }else{
            table_allotment_obj.load_table_summary();
        }
    }
    else{
        alert("Permission Denied for "+user_role);
        window.location = "./dashboard.html";
    }
    

});



$(document).on('keypress','.restaurant_table_entry',function(evt){
    
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

});
